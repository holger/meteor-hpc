#include <functional>

double gaussLegendre(int order, std::function<double(double)> f, double x0 = -1, double x1 = 1);
