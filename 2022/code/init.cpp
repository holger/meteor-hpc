// g++ -o init init.cpp -D_GLIBCXX_DEBUG

#include <iostream>
#include <functional>
#include <vector>
#include <cmath>

#include "gauss_legendre.hpp"

double trafo(double x, double x0 = -1, double x1 = 1)
{
  return 2*x/(x1-x0)+1-2*x1/(x1-x0);
}

double P0(double x, double x0 = -1, double x1 = 1)
{
  return 1;
}

double P1(double x, double x0 = -1, double x1 = 1)
{
  double xt = trafo(x,x0,x1);
  return xt;
}

double P2(double x, double x0 = -1, double x1 = 1)
{
  double xt = trafo(x,x0,x1);
  return 0.5*(3*xt*xt-1);
}

double P3(double x, double x0 = -1, double x1 = 1)
{
  double xt = trafo(x,x0,x1);
  return 0.5*(5*xt*xt*xt-3*xt);
}

std::vector<std::function<double(double x, double x0, double x1)>> legendrePoly = {&P0, &P1, &P2, &P3};

double u0(double x)
{
  return sin(x);
}

std::function<double(double x)> product(std::function<double(double x)> func, std::function<double(double x, double x0, double x1)> leg, double x0, double x1)
{
  return [func,leg,x0,x1](double x){return func(x)*leg(x,x0,x1);};
}

int main()
{
  // auto f = product(u0, legendrePoly[0], x0, x1);
  // std::cout << gaussLegendre(2,f,x0,x1) << std::endl;

  double globalX0 = 0;
  double globalX1 = 6.28;
  int n = 20;
  double dx = (globalX1-globalX0)/n;
  std::vector<double> c0(n);
  std::vector<double> c1(n);
  std::vector<double> c2(n);

  for(int i=0; i<n;i++) {
    double x0 = i*dx;
    double x1 = (i+1)*dx;
    
    double order = 0;
    auto f = product(u0, legendrePoly[order], x0, x1);
    c0[i] = (2*order+1)/dx*gaussLegendre(2,f,x0,x1);

    order = 1;
    f = product(u0, legendrePoly[order], x0, x1);
    c1[i] = (2*order+1)/dx*gaussLegendre(2,f,x0,x1);
}

  for(int i=0; i<n;i++) 
    std::cout << i << "\t" << c0[i] << "\t" << c1[i] << std::endl;
    
}
