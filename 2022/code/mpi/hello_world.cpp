// mpicxx -o course course.cpp -std=c++11
// mpirun -np 4 course <n> <loop>

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>

// Each processor has its own memory.
// Each processor reads each line of the code for its own.
// Communication between processes by MPI functions

int main(int argc, char** argv)
{
  // initialize MPI with command line arguments
  MPI_Init(&argc, &argv);
  
  int commSize; // number of processes
  int commRank; // id of process

  MPI_Comm_size(MPI_COMM_WORLD, &commSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &commRank);

  std::cout << "hello world from rank " << commRank << " of " << commSize << " ranks" << std::endl;

  MPI_Finalize();
}
