// g++ -o main_threads main_threads.cpp --std=c++17 -pthread -O3 -fno-tree-vectorize

#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <chrono>
#include <mpi.h>

#include "legendre.hpp"

void advance(std::vector<double>& c0New, std::vector<double>& c1New, std::vector<double> const& c0, std::vector<double> const& c1, std::vector<double> const& c0Tmp, std::vector<double> const& c1Tmp,
	     double a, double dt, double dx, int commRank, int commSize) {

  int nx = c0.size();

  for(int i=1;i<nx;++i) {
    c0New[i] = c0[i] - a*dt*(c0Tmp[i]+c1Tmp[i] - c0Tmp[i-1] - c1Tmp[i-1])/dx;
    c1New[i] = c1[i] + 3.*a*dt/dx*(c0Tmp[i] - c1Tmp[i] - c0Tmp[i-1] - c1Tmp[i-1]);
  }

  double neighboorValue0;
  double neighboorValue1;
  int tag0 = 0;
  int tag1 = 1;
  MPI_Status status;

  // interprocess exchange:
  if(commRank != commSize-1) {
    MPI_Send(&c0Tmp[nx-1], 1, MPI_DOUBLE, commRank+1, tag0, MPI_COMM_WORLD);
    MPI_Send(&c1Tmp[nx-1], 1, MPI_DOUBLE, commRank+1, tag1, MPI_COMM_WORLD);
    //    std::cout << commRank << " sends " << c0Tmp[nx-1] << "\t" << c1Tmp[nx-1] << " to " << commRank+1 << std::endl;
  }
  if(commRank != 0) {
    MPI_Recv(&neighboorValue0, 1, MPI_DOUBLE, commRank-1, tag0, MPI_COMM_WORLD, &status);
    MPI_Recv(&neighboorValue1, 1, MPI_DOUBLE, commRank-1, tag1, MPI_COMM_WORLD, &status);
    //    std::cout << commRank << " receives " << neighboorValue0 << "\t" <<   neighboorValue1 << " from " << commRank-1 << std::endl;
  }

  int i = 0;
  c0New[i] = c0[i] - a*dt*(c0Tmp[i]+c1Tmp[i] - neighboorValue0 - neighboorValue1)/dx;
  c1New[i] = c1[i] + 3.*a*dt/dx*(c0Tmp[i] - c1Tmp[i] - neighboorValue0 - neighboorValue1);

  // periodic boundary conditions:
  if(commRank == commSize -1) {
    MPI_Send(&c0New[nx-2], 1, MPI_DOUBLE, 0, tag0, MPI_COMM_WORLD);
    MPI_Send(&c1New[nx-2], 1, MPI_DOUBLE, 0, tag1, MPI_COMM_WORLD);
    //    std::cout << commRank << " sends " << c0New[nx-2] << "\t" << c1New[nx-2] << " to " << 0 << std::endl;
  }

  if(commRank == 0) {
    MPI_Recv(&c0New[0], 1, MPI_DOUBLE, commSize-1, tag0, MPI_COMM_WORLD, &status);
    MPI_Recv(&c1New[0], 1, MPI_DOUBLE, commSize-1, tag1, MPI_COMM_WORLD, &status);
    //    std::cout << 0 << " receives " << c0New[0] << "\t" <<   c1New[0] << " from " << commSize-1 << std::endl;    
  }


  if(commRank == 0) {
    MPI_Send(&c0New[1], 1, MPI_DOUBLE, commSize-1, tag0, MPI_COMM_WORLD);
    MPI_Send(&c1New[1], 1, MPI_DOUBLE, commSize-1, tag1, MPI_COMM_WORLD);
    //    std::cout << 0 << " sends " << c0New[1] << "\t" << c1New[1] << " to " << commSize-1 << std::endl;
  }

  if(commRank == commSize-1) {
    MPI_Recv(&c0New[nx-1], 1, MPI_DOUBLE, 0, tag0, MPI_COMM_WORLD, &status);
    MPI_Recv(&c1New[nx-1], 1, MPI_DOUBLE, 0, tag1, MPI_COMM_WORLD, &status);
    //    std::cout << commSize-1 << " receives " << c0New[nx-1] << "\t" <<   c1New[nx-1] << " from " << 0 << std::endl;    
  }
}

int main(int argc, char** argv)
{
  // initialize MPI with command line arguments
  MPI_Init(&argc, &argv);

  std::cerr << "Test of the discontinious Galerkin method for 1d advection starts ...\n";

  if(argc < 2) {
    std::cerr << "ERROR: argc = " << argc << " < 2 !\n";
    std::cerr << "usage: mpirun -np <nProc> ./cours <nGlobal> \n";
    exit(1);
  }

  int commSize; // id of process
  int commRank; // number of processes
  
  MPI_Comm_size(MPI_COMM_WORLD, &commSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &commRank);

  // global size of the grid
  int nGlobal = atoi(argv[1]);
  // local size of vector
  int nLocal = nGlobal/commSize;
  
  double lx = 2*M_PI;
  double dx = lx/(nGlobal-2);
  int nInt = 100;
  double dInt = 2./(nInt-1);
  double ddx = dx/(nInt-1);

  double cfl = 0.1;  // cfl = a*dt/dx
  double a = 1;
  double dt = cfl*dx/a;
  double T = lx/a;
  
  std::cerr << "nGlobal = " << nGlobal << std::endl;
  std::cerr << "lx = " << lx << std::endl;
  std::cerr << "dx = " << dx << std::endl;
  std::cerr << "cfl = " << cfl << std::endl;
  std::cerr << "a = " << a << std::endl;
  std::cerr << "dt = " << dt << std::endl;
  std::cerr << "T = " << T << std::endl;
  
  std::vector<double> c0(nLocal);
  std::vector<double> c1(nLocal);
  std::vector<double> c0New(nLocal);
  std::vector<double> c1New(nLocal);
  std::vector<double> c0Tmp(nLocal);
  std::vector<double> c1Tmp(nLocal);
  std::vector<double> uNew(nLocal);
  std::vector<double> u(nLocal);
  std::vector<double> uFine(nLocal*nInt);
  std::vector<double> uFineNew(nLocal*nInt);

  std::vector<double> uRef(nLocal);
  std::vector<double> uRefFine(nLocal*nInt);

  int localStart = nLocal*commRank;
  std::cerr << commRank << ": localStart = " << localStart << std::endl;
  
  for(int i=0;i<nLocal*nInt;i++) {
    uRefFine[i] = sin(dx/nInt*(i + localStart + 0.5));
  }

  std::vector<double> xi={-sqrt(3./5), 0 ,sqrt(3./5)};
  std::vector<double> wi={5./9, 8./9, 5./9};
    
  for(int i=0;i<nLocal;i++){
    double integ=0;
    for(size_t j=0;j<xi.size();j++){
      double x=(i+localStart)*dx+0.5*dx+xi[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi[j]*Legendre::Pn(0, xi[j]);
    }
    c0[i]=((2*0+1)/2.)*integ;

    integ=0;
    for(size_t j=0;j<xi.size();j++){
      double x=(i+localStart)*dx+0.5*dx+xi[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi[j]*Legendre::Pn(1, xi[j]);
    }
    c1[i]=((2*1+1)/2.)*integ;
  }
 
  for(int i=0;i<nLocal;i++) {
    uRef[i] = sin(dx*((i+localStart)+0.5));
  }
  
  for(int i=0;i<nLocal;i++) {
    u[i] = c0[i];
    for(int j=0;j<nInt;j++)
      uFine[i*nInt+j] = c0[i]+c1[i]*(-1+j*dInt);
  }
  
  
  // std::cout << "u = \n";
  // for(int i=0;i<nx;i++) 
  //   std::cout << i << "\t" << u[i] << std::endl;

  // exit(0);

  std::string filename = "u.txt";
  std::ofstream out(filename.c_str(), std::ios::app);
  for(int i=0;i<nLocal;i++) 
    out << (i+localStart + 0.5)*dx << "\t" << u[i] << "\t" << uRef[i] << std::endl;
  
  out.close();
  
  filename = "uFine.txt";
  out.open(filename.c_str(), std::ios::app);
  for(int i=0;i<nLocal*nInt;i++) 
    out << (i+localStart*nInt)*ddx << "\t" << uFine[i] << "\t" << uRefFine[i] << std::endl;
  
  out.close();

  int nIter = round(T/dt);
  std::cerr << "nIter = " << nIter << "\t" << T/dt << std::endl;

  auto start = std::chrono::system_clock::now();
  
  for(int iter = 0;iter<nIter;iter++) {
    
    advance(c0Tmp, c1Tmp, c0, c1, c0, c1, a, dt/2, dx, commRank, commSize);
    advance(c0New, c1New, c0, c1, c0Tmp, c1Tmp, a, dt, dx, commRank, commSize);

    c0 = c0New;
    c1 = c1New;
  }

  auto end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "finished computation at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n";

  
  for(int i=0;i<nLocal;i++) {
    uNew[i] = c0New[i];
    for(int j=0;j<nInt;j++)
      uFineNew[i*nInt+j] = c0New[i] + c1New[i]*Legendre::Pn(1, -1+j*dInt);
  }

  double error = 0;
  for(int i=0;i<nLocal;i++) {
    error += (uNew[i]-uRef[i])*(uNew[i]-uRef[i])*dx;
  }

  double globalError;
  MPI_Reduce(&error, &globalError, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  globalError = sqrt(globalError);

  if(commRank == 0) {
    std::cerr << commRank << ": global error = " << globalError << std::endl;
    filename = "error.txt";
    out.open(filename.c_str(), std::ios::app);
    out << nGlobal << "\t" << globalError << std::endl;
    out.close();
  }


  filename = "uNew.txt";
  out.open(filename.c_str(), std::ios::app);
  for(int i=0;i<nLocal;i++) 
    out << (i+localStart + 0.5)*dx << "\t" << uNew[i] << "\t" << uRef[i] << std::endl;
  
  out.close();

  filename = "uFineNew.txt";
  out.open(filename.c_str(), std::ios::app);
  for(int i=0;i<nLocal*nInt;i++) 
    out << (i+localStart*nInt)*ddx << "\t" << uFineNew[i] << "\t" << uRefFine[i] << std::endl;
  
  out.close();

  MPI_Finalize();
}
