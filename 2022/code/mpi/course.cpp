// mpicxx -o course course.cpp -std=c++11 -O3
// mpirun -np 4 course <n> <loop>

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <chrono>

// MPI implementation of the derivative of a function
// 
// Each processor has its own memory.
// Each processor reads each line of the code for its own.
// Communication between processes by MPI functions

int main(int argc, char** argv)
{
  // initialize MPI with command line arguments
  MPI_Init(&argc, &argv);
  
  std::cerr << "starting mpi test ...\n";

  if(argc < 3) {
    std::cerr << "ERROR : argc = " << argc << " < 3\n";
    std::cerr << "usage : course <n> <loop>\n";
    exit(0);
  }

  int commSize; // id of process
  int commRank; // number of processes

  MPI_Comm_size(MPI_COMM_WORLD, &commSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &commRank);

  std::cerr << "hello world from rank " << commRank << " of " << commSize << " ranks" << std::endl;

  // global size of vector
  int n = atoi(argv[1]);
  // number of loops
  int loop = atoi(argv[2]);
  // local size of vector
  int nLocal = n/commSize;
  // cell size
  double dx = 2*M_PI/n;
    
  std::vector<float> vec(nLocal);
  std::vector<float> vecDiff(nLocal);
  
  int localStart = nLocal*commRank;
  
  for(int i=0;i<nLocal;i++)
    vec[i] = sin(2*M_PI/n*(i + localStart+1));

  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();

  for(int i=0;i<loop;i++) {
    for(int j=0;j<nLocal-1;j++) 
      vecDiff[j] = (vec[j+1] - vec[j])/dx;

    // exchange of boundary values:
    float neighboorValue;
    int tag = 1;
    MPI_Status status;

    if(commRank == 0) {
      MPI_Send(&vec[0], 1, MPI_FLOAT, commSize-1, tag, MPI_COMM_WORLD);
      //      std::cout << commRank << " sends " << vec[0] << " to " << commSize-1 << std::endl;
    }
    else {
      MPI_Send(&vec[0], 1, MPI_FLOAT, commRank-1, tag, MPI_COMM_WORLD);
      //      std::cout << commRank << " sends " << vec[0] << " to " << commRank-1 << std::endl;
    }
    
    if(commRank == commSize - 1) {
      MPI_Recv(&neighboorValue, 1, MPI_FLOAT, 0, tag, MPI_COMM_WORLD, &status);
      //      std::cout << commRank << " receives " << neighboorValue << " from " << 0 << std::endl;
    }
    else {
      MPI_Recv(&neighboorValue, 1, MPI_FLOAT, commRank+1, tag, MPI_COMM_WORLD, &status);
      //      std::cout << commRank << " receives " << neighboorValue << " from " << commRank+1 << std::endl;
    }
	     
    vecDiff[nLocal-1] = neighboorValue - vec[nLocal-1];
  }

  end = std::chrono::system_clock::now();
  
  std::chrono::duration<double> elapsed_seconds = end-start;
  
  std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";


  // for(int i=0; i<nLocal;i++)
  //   std::cout << i+localStart << "\t" << vec[i] << "\t" << vecDiff[i] << std::endl;

  MPI_Finalize();
}
