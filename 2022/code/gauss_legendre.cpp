// g++ -o gauss_legendre gauss_legendre.cpp -D_GLIBCXX_DEBUG

#include <iostream>
#include <vector>
#include <functional>

double f1(double x)
{
  return x;
}

double f2(double x)
{
  return x*x;
}

double f3(double x)
{
  return x*x*x;
}

double f4(double x)
{
  return x*x*x*x;
}

std::vector<std::vector<double>> dat_order1={{0},
					 {2}};
std::vector<std::vector<double>> dat_order2={{-0.5773502691896257645091488,0.5773502691896257645091488},
					 {1,1}};
std::vector<std::vector<double>> dat_order3={{-0.7745966692414833770358531,0,0.7745966692414833770358531},
					 {0.555555555555555555555555,0.8888888888888888888888889,0.555555555555555555555555}};

std::vector<std::vector<std::vector<double>>> dat = {dat_order1, dat_order2, dat_order3};

double gaussLegendre(int order, std::function<double(double)> f, double x0 = -1, double x1 = 1)
{
  double result = 0;
  int number_nodes = dat[order][0].size();
  for(int i=0; i<number_nodes; i++) {
    double xi = dat[order][0][i];
    double wi = dat[order][1][i];
    double xit = (x1-x0)/2*xi-(x1-x0)/2+x1;
    result += f(xit)*wi;
  }

  return result*(x1-x0)/2;
}

// int main()
// {
//   std::cout << "Integration with Gauss-Legendre quadrature starts\n";

//   std::cout << dat[0][1][0] << std::endl;

//   std::cout << gaussLegendre(1, &f2) << std::endl;
//   std::cout << gaussLegendre(2, &f3) << std::endl;
//   std::cout << gaussLegendre(2, &f3, 0, 0.1) << std::endl;
// }
