#include <thread>
#include <iostream>

int a = -1;

void initialization()
{
  a = 1;
}

int main()
{
  std::thread simple(initialization);
  simple.join();
  std::cout << "a = " << a << std::endl;
}
