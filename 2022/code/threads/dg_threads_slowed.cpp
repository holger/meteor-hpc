// g++ -o main_threads main_threads.cpp --std=c++17 -pthread -O3 -fno-tree-vectorize

#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <chrono>

#include "legendre.hpp"
#include "threadPool.hpp"

void advance(std::vector<float>& c0New, std::vector<float>& c1New, std::vector<float> const& c0, std::vector<float> const& c1, std::vector<float> const& c0Tmp, std::vector<float> const& c1Tmp,
	     double a, double dt, double dx, int nThreads, int threadId) {

  int nx = c0.size()-2;
  int localSize = nx/nThreads;
  int from = threadId*localSize+1;
  int to = (threadId+1)*localSize+1;

  //  std::cout << "localSize = " << localSize << "\t" << from << "\t" << to << std::endl;

  for(int i=from;i!=to;++i) {
    c0New[i] = c0[i] - a*dt*(c0Tmp[i]/fabs(c0Tmp[i])*sqrt(c0Tmp[i]*c0Tmp[i])+c1Tmp[i] - c0Tmp[i-1] - c1Tmp[i-1])/dx;
    c1New[i] = c1[i] + 3.*a*dt/dx*(c0Tmp[i] - c1Tmp[i] - c0Tmp[i-1] - c1Tmp[i-1]);
  }
}

void copy(std::vector<float>& c0, std::vector<float> const& c0Old, std::vector<float>& c1, std::vector<float> const& c1Old, int nThreads, int threadId) {
  
  int nx = c0.size()-2;
  int localSize = nx/nThreads;
  int from = threadId*localSize+1;
  int to = (threadId+1)*localSize+1;
  
  for(int i=from;i!=to;i++) {
    c0[i] = c0Old[i];
    c1[i] = c1Old[i];
  }
}

int main(int argc, char** argv)
{
  std::cerr << "Test of the discontinious Galerkin method for 1d advection starts ...\n";

  if(argc < 3) {
    std::cerr << "ERROR: argc = " << argc << " < 3 !\n";
    std::cerr << "usage: ./cours <nx> <nThreads> \n";
    exit(1);
  }
    
  int nx = atoi(argv[1]);
  int nThreads = atoi(argv[2]);
  float lx = 2*M_PI;
  float dx = lx/(nx-2);
  int nInt = 100;
  float dInt = 2./(nInt-1);
  float ddx = dx/(nInt-1);

  float cfl = 0.1;  // cfl = a*dt/dx
  float a = 1;
  float dt = cfl*dx/a;
  float T = lx/a;
  
  std::cerr << "nx = " << nx << std::endl;
  std::cerr << "lx = " << lx << std::endl;
  std::cerr << "dx = " << dx << std::endl;
  std::cerr << "cfl = " << cfl << std::endl;
  std::cerr << "a = " << a << std::endl;
  std::cerr << "dt = " << dt << std::endl;
  std::cerr << "T = " << T << std::endl;
  std::cerr << "nThreads = " << nThreads << std::endl;
  
  std::vector<float> c0(nx);
  std::vector<float> c1(nx);
  std::vector<float> c0New(nx);
  std::vector<float> c1New(nx);
  std::vector<float> c0Tmp(nx);
  std::vector<float> c1Tmp(nx);
  std::vector<float> uNew(nx);
  std::vector<float> u(nx);
  std::vector<float> uFine(nx*nInt);
  std::vector<float> uFineNew(nx*nInt);

  std::vector<float> uRef(nx);
  std::vector<float> uRefFine(nx*nInt);
  
  for(int i=0;i<nx*nInt;i++) {
    uRefFine[i] = sin(2*M_PI/((nx-2)*nInt)*(i+0.5));
  }

  std::vector<float> xi={-sqrt(3./5), 0 ,sqrt(3./5)};
  std::vector<float> wi={5./9, 8./9, 5./9};
    
  for(int i=0;i<nx;i++){
    float integ=0;
    for(size_t j=0;j<xi.size();j++){
      float x=i*dx+0.5*dx+xi[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi[j]*Legendre::Pn(0, xi[j]);
    }
    c0[i]=((2*0+1)/2.)*integ;

    integ=0;
    for(size_t j=0;j<xi.size();j++){
      float x=i*dx+0.5*dx+xi[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi[j]*Legendre::Pn(1, xi[j]);
    }
    c1[i]=((2*1+1)/2.)*integ;
  }
 
  for(int i=0;i<nx;i++) {
    uRef[i] = sin(2*M_PI/(nx-2)*(i+0.5));
  }
  
  for(int i=0;i<nx;i++) {
    u[i] = c0[i];
    for(int j=0;j<nInt;j++)
      uFine[i*nInt+j] = c0[i]+c1[i]*(-1+j*dInt);
  }
  
  
  // std::cout << "u = \n";
  // for(int i=0;i<nx;i++) 
  //   std::cout << i << "\t" << u[i] << std::endl;

  // exit(0);

  // std::string filename = "u.txt";
  // std::ofstream out(filename.c_str());
  // for(int i=0;i<nx;i++) 
  //   out << (i+0.5)*dx << "\t" << u[i] << "\t" << uRef[i] << std::endl;
  
  // out.close();
  
  // filename = "uFine.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx*nInt;i++) 
  //   out << i*ddx << "\t" << uFine[i] << "\t" << uRefFine[i] << std::endl;
  
  // out.close();

  int nIter = round(T/dt);
  nIter=100;
  std::cerr << "nIter = " << nIter << "\t" << T/dt << std::endl;

  ThreadPool threadPool(nThreads);
  
  auto start = std::chrono::system_clock::now();
  
  for(int iter = 0;iter<nIter;iter++) {
    
    std::vector<std::future<void>> threadVec(nThreads);
    
    // creation of threads and execution of function diff
    for(int threadId = 0; threadId < nThreads; threadId++) {
      threadVec[threadId] = threadPool.Push(advance, std::ref(c0Tmp), std::ref(c1Tmp), std::cref(c0), std::cref(c1), std::cref(c0), std::cref(c1), a, dt/2, dx, nThreads, threadId);
    }
    
    for(int threadId = 0; threadId < nThreads; threadId++) {
      threadVec[threadId].get();
    }
    
    c0Tmp[0] = c0Tmp[nx-2];
    c0Tmp[nx-1] = c0Tmp[1];
    c1Tmp[0] = c1Tmp[nx-2];
    c1Tmp[nx-1] = c1Tmp[1];

    for(int threadId = 0; threadId < nThreads; threadId++) {
      threadVec[threadId] = threadPool.Push(advance, std::ref(c0New), std::ref(c1New), std::cref(c0), std::cref(c1), std::cref(c0Tmp), std::cref(c1Tmp), a, dt, dx, nThreads, threadId);
    }
    
    for(int threadId = 0; threadId < nThreads; threadId++) {
      threadVec[threadId].get();
    }

    c0New[0] = c0New[nx-2];
    c0New[nx-1] = c0New[1];
    c1New[0] = c1New[nx-2];
    c1New[nx-1] = c1New[1];

    for(int threadId = 0; threadId < nThreads; threadId++) {
      threadVec[threadId] = threadPool.Push(copy, std::ref(c0), std::cref(c0New), std::ref(c1), std::cref(c1New), nThreads, threadId);
    }
    
    for(int threadId = 0; threadId < nThreads; threadId++) {
      threadVec[threadId].get();
    }
  }

  auto end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "finished computation at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n";

  
  for(int i=0;i<nx;i++) {
    uNew[i] = c0New[i];
    for(int j=0;j<nInt;j++)
      uFineNew[i*nInt+j] = c0New[i] + c1New[i]*Legendre::Pn(1, -1+j*dInt);
  }

  float error = 0;
  for(int i=0;i<nx;i++) {
    error += (uNew[i]-uRef[i])*(uNew[i]-uRef[i])*dx;
  }
  error = sqrt(error);
  std::cerr << "error = " << error << std::endl;

  // filename = "error.txt";
  // out.open(filename.c_str(), std::ios::app);
  // out << nx << "\t" << error << std::endl;
  // out.close();
  
  // filename = "uNew.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx;i++) 
  //   out << (i+0.5)*dx << "\t" << uNew[i] << "\t" << uRef[i] << std::endl;
  
  // out.close();

  // filename = "uFineNew.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx*nInt;i++) 
  //   out << i*ddx << "\t" << uFineNew[i] << "\t" << uRefFine[i] << std::endl;
  
  // out.close();
}
