// g++ -std=gnu++11 -o cours -pthread cours.cc

#include <iostream>      
#include <thread>        
#include <vector> 
#include <cmath>
#include <cstdlib>

#include "threadPool.hpp"



// Thread implementation of the derivative of a function
//
// All threads (processors) share the same memory
// Threads are created for a specific task (function)

// Threaded differentiation: nThreads = number of threads, threadId = id of thread
void diff(const std::vector<double>& vec, std::vector<double>& result, int nThreads, int threadId)
{
  // global size of the vector
  int size = vec.size() - 2;
  // part computed by the thread
  int localSize = size/nThreads;
  
  // start index for the thread in the vector
  int from = threadId*localSize+1;
  // end index
  int to = (threadId+1)*localSize+1;

  double dx = 2*M_PI/vec.size();

  int loops = 1;
  for(int loop=0;loop<loops;loop++) 
    for(int i=from; i != to; i++)
      result[i] = (vec[i+1] - vec[i-1])/(2*dx);
  
}

int main(int argc, char** argv) 
{

  if(argc < 4) {
    std::cerr << "ERROR: argc = " << argc << " < 4 !\n";
    std::cerr << "usage: ./cours <n> <nThreads> <loops>\n";
    exit(1);
  }
    
  
  // number of threads
  int nThreads = atoi(argv[2]);
  // number of grid points 
  int n=atoi(argv[1]);
  // number of loops 
  int loops=atoi(argv[3]);
  

  ThreadPool threadPool(nThreads);
  
  // allocation of vectors of global size
  std::vector<double> vec(n);
  std::vector<double> vecDiff(n);
  
  // initialization with a sin
  for(int i=0;i<n;i++) {
    vec[i] = sin(2*M_PI*i/n);
  }

  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();

  for(int i=0; i<loops; i++) {
    std::vector<std::future<void>> threadVec(nThreads);
    
    // creation of threads and execution of function diff
    for(int threadId = 0; threadId < nThreads; threadId++) {
     threadVec[threadId] = threadPool.Push(diff,std::cref(vec), std::ref(vecDiff),nThreads,threadId);
    }
    
    for(int threadId = 0; threadId < nThreads; threadId++) {
      threadVec[threadId].get();
    }
      
    // // wait until the threads completed the task
    // for(auto thread : threadVec)
    //   thread->join();
    
    // // erase the threads
    // for(auto thread : threadVec) 
    //   delete thread;
  }

  end = std::chrono::system_clock::now();
 
  std::chrono::duration<double> elapsed_seconds = end-start;

  std::cout << "elapsed time with " << nThreads << " threads : " << elapsed_seconds.count() << "s\n";

  
  // output
  // for(int i=0;i<n;i++)
  //   std::cout << i << "\t" << vec[i] << "\t" << vecDiff[i] << std::endl;

  return 0;
}

// CAUTION!!! : Thread safety !

// imagine that you have an int x = 0; and now you use threads to
// increment its value (++x) (a function like diff could just do this);
// the problem: ++x consists of three operations:

// 1) fetch the variable from the memory
// 2) Increment the variable inside the arithmetic logic unit (ALU)
// 3) write the variable back to the memory

// with in a multi threaded implementation another thread (T2) could
// read the variable x while the first thread (T1) uses the ALU in the
// following sequence:

// T1 fetches x (x=0 in memory)
// T1 increases x (x=1 in ALU)
// T2 fetches x (x=0 in memory)
// T1 writes x (x=1 in memory)
// T2 increases x (x=1 in ALU)
// T2 writes x (x=1 in memory)

// The result is: x=1 in memory. We expected x=2.  For this one has to
// guarante that the read-modify-write operation is ATOMIC.
// -> introduction of locks by Mutex = Mutual Exclusives

// exlusive access to a variable by acquiring a mutex associated to
// the variable. Other thread have to wait until the first thread
// releases the mutex.

// no problem in our example above: each thread accesses its own
// portion of the memory.

// Task: write multi-threaded program to make the threads write "hi, I
// am thread 0 (... n) of n threads".
