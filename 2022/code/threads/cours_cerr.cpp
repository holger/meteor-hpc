// g++ -std=c++11 -o cours_cerr -pthread cours_cerr.cc

#include <iostream>      
#include <thread>
#include <vector>     

void print(int nThreads, int threadId)
{
  std::cout << "hi, I am " << threadId << " of " << nThreads << std::endl;
}

int main(int argc, char** argv) 
{
  // number of threads
  int nThreads = 4;
 
  // pointers to threads are stored in a vector
  std::vector<std::thread*> threadVec(nThreads);
 
  // creation of threads and execution of function diff
  for(int threadId = 0; threadId < nThreads; threadId++) 
    threadVec[threadId] = new std::thread(print,nThreads,threadId);
 
  // wait until the threads completed the task
  for(auto thread = threadVec.begin(); thread != threadVec.end(); thread++) {
    (*thread)->join();
  } 
  
  // erase the threads
  for(auto thread = threadVec.begin(); thread != threadVec.end(); thread++) {
    delete *thread;
  } 
}
