// Discontinuous-Galerkin_multi_thread

#include <iostream>      
#include <thread>        
#include <vector> 
#include <cmath>
#include <cstdlib>
#include <mutex>
#include <atomic>
#include <fstream>
#include <iomanip>




//--------------------------------------------------------------------------------------------------------------------------------------------
//// Object Class Array defined during the lecture 
class Array  // 1D Array
{
public:

  // constructor = function that does not return anything
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }
  
  
  Array(Array const& a) { // when Array const& a copy is made
    n_ = a.n_;
    dataP_ = new float[n_];
    for(int i=0; i<n_; i++)
      dataP_[i] = a.dataP_[i];
 
  }
  
  ~Array(){  // destructor allows the code to destroy the memories when a scope (function) is left
  	delete  [] dataP_;
  }
  
  // operator overloading

    float& operator[](int i) {
    // if(i < 0 || i >= n_) {
    //   std::cout << "ERROR in Array::write: i=" << i << "not in range n_=" << n_ << std::endl;
    //   exit(1);
    // }
    return dataP_[i];
    }
    
    float operator[](int i) const {
    // if(i < 0 || i >= n_) {
    //   std::cout << "ERROR in Array::write: i=" << i << "not in range n_=" << n_ << std::endl;
    //   exit(1);
    // }
    return dataP_[i];
    }

  void operator=(Array array)
  {
    // if(n_ != array.n_) {
    //   std::cout << "ERROR : size of operators don't match";
    //   exit(1);
    // }
    for(int i=0; i<n_; i++)
      dataP_[i] = array.dataP_[i];
  }

  int size() {
    return n_;
  }

private:
  int n_;
  float* dataP_;
};
//---------------------------------------------------------------------------------------------------------------------------------------------




//=============================================================== FUNCTIONS =================================================================


//-------------------------------------------------------------------------------------------------------------------------------------------
//Initial Profile
double Profile(double x) {
  
  return sin(x);

}
//-------------------------------------------------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------------------------------------------------
//Second Legendre polynomial
double P1(double x, double dx){

	return x * (2./dx);

}
//-------------------------------------------------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------------------------------------------------
// Boundary_conditions
void boundary_conditions(Array& u){

  u[0]=u[u.size()-2];
  
  u[u.size()-1] = u[1];

}
//-------------------------------------------------------------------------------------------------------------------------------------------




//-------------------------------------------------------------------------------------------------------------------------------------------
// ki_0
void ki_0(double a, double dx, Array const & ctemp0, Array const & ctemp1, Array& ki0, int nx){

  for(int ix=1; ix<nx-1; ix++) {
    
      ki0[ix] = -(a/dx) * (ctemp0[ix] + ctemp1[ix] - ctemp0[ix-1] - ctemp1[ix-1]);
       
    }
	
}
//-------------------------------------------------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------------------------------------------------
// ki_1
void ki_1(double a, double dx, Array const& ctemp0, Array const& ctemp1, Array& ki0, int nx){

  for(int ix=1; ix<nx-1; ix++) {
    
      ki0[ix] = 3 * (a/dx) * (ctemp0[ix] - ctemp1[ix] - ctemp0[ix-1] - ctemp1[ix-1]);
       
    }

}
//-------------------------------------------------------------------------------------------------------------------------------------------


 

//-------------------------------------------------------------------------------------------------------------------------------------------
//Runge-Kutta Order 4
void Runge_kutta_order_4(Array& C0_i0, Array& C0_i1, int nt, int nx , double dt, double dx, double a, int it, Array& cf0, Array& cf1 ){

Array u1(nx); // will contain the values of u at time step t+dt for the whole grid

Array k10(nx); //will contain the ki for c0 for the whole grid

Array k11(nx);

Array k20(nx);

Array k21(nx);

Array k30(nx);

Array k31(nx);

Array k40(nx);

Array k41(nx);

Array ctemp0(nx);

Array ctemp1(nx);

  

    // preparing the ctemp for the k1
    for(int ix=0; ix<nx; ix++) {
      
      ctemp0[ix] = C0_i0[ix];
      
      ctemp1[ix] = C0_i1[ix];
    
    }

    // calculating the k1
    
    ki_0( a, dx, ctemp0, ctemp1, k10, nx);
      
    ki_1( a, dx, ctemp0, ctemp1, k11, nx);
   

    boundary_conditions(k10); // mettre les joins !!!!!!!
    
    boundary_conditions(k11);
    
    //calculating the ctemp to be inputed for the k2
    for(int ix=0; ix<nx; ix++) {
    
      ctemp0[ix] = C0_i0[ix] + (dt/2) * k10[ix];
      
      ctemp1[ix] = C0_i1[ix] + (dt/2) * k11[ix];
      
    }
      
    //calculating the k2
 
    ki_0(a, dx, ctemp0, ctemp1, k20, nx);
    
    ki_1( a, dx, ctemp0, ctemp1, k21, nx);
    
    //std::thread* th_0 = new std::thread(ki_0, a, dx, std::cref(ctemp0), std::cref(ctemp1), std::ref(k20), nx); 
      
    //std::thread* th_1 = new std::thread(ki_1, a, dx, std::cref(ctemp0), std::cref(ctemp1), std::ref(k21), nx);
    
    
    
    boundary_conditions(k20);
    
    boundary_conditions(k21);
    
    //th_0 -> join();
    
    //th_1 -> join();
    
    //delete th_0;
    
    //delete th_1;

    //calculating the ctemp to be inputed for the k3
    for(int ix=0; ix<nx; ix++) {
    
      ctemp0[ix] = C0_i0[ix] + (dt/2) * k20[ix];
      
      ctemp1[ix] = C0_i1[ix] + (dt/2) * k21[ix];
    
    }

    //calculating the k3

    
    ki_0( a, dx, ctemp0, ctemp1, k30, nx);
      
    ki_1( a, dx, ctemp0, ctemp1, k31, nx);
    
    
    boundary_conditions(k30);
    
    boundary_conditions(k31);

    //calculating the ctemp to be inputed for the k4
    for(int ix=0; ix<nx; ix++) {
    
      ctemp0[ix] = C0_i0[ix] + dt * k30[ix];
      
      ctemp1[ix] = C0_i1[ix] + dt * k31[ix];
    
    }

    //calculating the k4

    
    ki_0( a, dx, ctemp0, ctemp1, k40, nx);
      
    ki_1( a, dx, ctemp0, ctemp1, k41, nx);
    
    
    boundary_conditions(k40);
    
    boundary_conditions(k41);

    //calculating the new c at time t+dt
    for(int ix=0; ix<nx; ix++) {
    
      cf0[ix] = C0_i0[ix] + 1./6. * (k10[ix] + 2*k20[ix] + 2*k30[ix] + k40[ix]) * dt;
      
      cf1[ix] = C0_i1[ix] + 1./6. * (k11[ix] + 2*k21[ix] + 2*k31[ix] + k41[ix]) * dt;
    
    }


    //Writing the results
    // std::string newFilename = "result/"+std::to_string(it)+"Discontinuous_Galerkine_rk.txt";
    
    // std::ofstream out(newFilename);
    

    // out << "CF0" <<"\t"<< "CF1" <<std::endl;
    
    
    // for(int ix=0; ix<nx; ix++) {
    
    //   out << std::setprecision(16) << cf0[ix] << "\t" << cf1[ix] << std::endl;
    
    // }

}
//-------------------------------------------------------------------------------------------------------------------------------------------

//===========================================================================================================================================












//============================================================= MAIN ========================================================================
int main(int argc, char *argv[]){

int n_arg  = 0;

int nx = 0; // Number of cells (spatial discretization)

int nt = 0; // Number of time step (time discretization)

for (char **pargv = &argv[0]; *pargv != argv[argc]; pargv++){ //retrieve the arguments
 
	//std::cout <<"argument #"<< n_arg <<" : " << *pargv <<" typeof argument : "<< typeid(*pargv).name()  << " sizeof : "<< sizeof(*pargv) << std::endl;
	
	if(n_arg == 1){
	
	nx = atoi(*pargv);

	}
	
	if(n_arg == 2){
	
	nt = atoi(*pargv);

	}
	n_arg+=1;



}


clock_t start, end;




std::string path = "."; // where the output will be placed


std::ofstream out( "Discontinuous_Galerkine_rk4_.txt" );
  
out << "CF0" <<"\t"<< "CF1" <<std::endl;

start = clock();

//Initial conditions------------------------

double xmin = 0.; // Lower bound

double xmax = 2.* M_PI; // Upper bound 

double dx = (xmax-xmin)/(nx-2); // Spatial step 

double a = 1.; // Characteristic speed of the fluid

//Initialization-----------------------------------------------------------------------------------------------------------------------------

//Gauss quadrature, order 2

//Weight
double omega1 = 1. * (dx/2.); //First weight here not 1 because we place the polynomial within the cell between [ (-dx/2), (dx/2) ]

double omega2 = 1. * (dx/2.);

//Points
double x1 = -(1./std::sqrt(3.)) * (dx/2.);//here we added (dx/2), because  we place the polynomial within the cell between [ (-dx/2), (dx/2) ]

double x2 = (1./std::sqrt(3.)) * (dx/2.);


Array u0(nx); //Initial profile here it is a sine function

Array C0_i0(nx); //Array containing all the c0 of the grid at time 0

Array C0_i1(nx); //Array containing all the c1 of the grid at time 0


  for(int i=0; i<nx; i++) {
    
    C0_i0[i] = (1/dx) * ( omega1 * Profile( (i+0.5)*dx + x1 ) + ( omega2 * Profile( (i+0.5)*dx + x2 ) ) );

    C0_i1[i] = (3/dx) * ( omega1 * Profile( ( (i+0.5)*dx + x1 ) )* P1(x1, dx)+   omega2 * Profile( ((i+0.5)*dx + x2 ) ) * P1(x2, dx) ); 

    out << std::setprecision(16) <<C0_i0[i] <<"\t"<< C0_i1[i] <<std::endl;

  }



//Runge-kutta order 4 -----------------------------------------------------------------------------------------------------------------------
  
double dt = ((0.1)* dx )/a;

Array cf0(nx); //will contain the c0 for the whole grid at t+dt

Array cf1(nx); //will contain the c1 for the whole grid at t+dt

for(int it=0; it<nt; it++) {

    Runge_kutta_order_4(C0_i0, C0_i1, nt, nx , dt, dx, a , it, cf0, cf1);

    C0_i0 = cf0;
    
    C0_i1 = cf1;
    
  }



//-------------------------------------------------------------------------------------------------------------------------------------------
// check the time taken by the code and create a log where its save the time taken.
end = clock();

double time_taken = double(end - start) / double(CLOCKS_PER_SEC);

std::cout << "time taken by the code :  "<< time_taken << " s"<< std::endl;

std::ofstream log;

log.open("log/log_time_nx_"+std::to_string(nx)+"_nt_"+std::to_string(nt)+".txt", std::fstream::in | std::fstream::out | std::fstream::app);

std::ifstream read("log/log_time_nx_"+std::to_string(nx)+"_nt_"+std::to_string(nt)+".txt");

std::cout << "is the file empty ? "<< read.peek() << std::endl;

if(read.peek() == -1){ // If the file is empty then write the header, nx and nt

log <<  "#"<< nx << std::endl;
log << "#"<< nt << std::endl;
log << "time" << std::endl;
//log << "#Time" << std::endl;

}

log << time_taken << std::endl;

std::cout << "nt = " << nt << " and nt type : "<< typeid(nt).name() << std::endl;
std::cout << "nx = " << nx << " and nx type : "<< typeid(nx).name() << std::endl;

//-------------------------------------------------------------------------------------------------------------------------------------------
return 0;
}
//===========================================================================================================================================
