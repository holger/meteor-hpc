#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 13:44:15 2022

@author: lmarques
"""

import numpy as np
import matplotlib.pyplot as plt

n = 6
nstr = str(n)

nx = 1

resdirec = "/home/lmarques/Downloads/METEORs/M5-Fluids/project/advection/results/"
direc = "/home/lmarques/Downloads/METEORs/M5-Fluids/project/advection/c_vector/gauss+c-vectors/test_vectorise_ctemp/test-getU/test-vector-array/test_error/"
filename = "adv_gauss_dg" + nstr +"_rk4_nx" + str(nx)+"_simu_"


# dt = 0.02
# nt = int((tmax - tmin)/dt)

xmax = 3.14 # + 2*2*3.14/nx
xmin = -3.14
dx = (xmax-xmin)/nx
xtheo = np.linspace(xmin-dx,xmax+dx,3000)

tmax = 1
tmin = 0
a = 1.
# dt = (tmax - tmin)/nt
CFL = 15
dt = dx/a/CFL
revolutions = 5
nt = revolutions*int(CFL*nx)

nb = 50

labelsize = 12
ticksize = 10
titlesize = 18

for i in range(0,nt,10): #plotted every 10 timesteps
    plt.figure(dpi=180)
    # u = np.loadtxt(direc+filename+str(i*CFL*nx)+".txt", skiprows=1)
    # u0 = np.loadtxt(direc+filename+str(0*CFL*nx)+".txt", skiprows=1)
    
    u = np.loadtxt(direc+filename+str(i)+".txt", skiprows=1)
    u0 = np.loadtxt(direc+filename+str(0)+".txt", skiprows=1)
    
    for icell in range(0,(nb+2)*(nx+2),nb+2):
        # plt.plot(u0[icell:icell+nb+2,0],u0[icell:icell+nb+2,1],color='tab:blue', alpha=0.6,linewidth=2)#,s=2, marker='x')        
        plt.plot(xtheo, np.sin(xtheo-a*i*dt), color='red',linewidth=1, linestyle='dashed')#, alpha=0.2)
        plt.scatter(u[icell:icell+nb+2,0],u[icell:icell+nb+2,1],color='tab:blue',s=2, marker='x')
        # plt.scatter(u[icell,0],u[icell,1],color='blue',s= 5, marker='x')
    # plt.scatter(u[:,0], u[:,1], s=5, color='blue', label=r'$u$')
    # plt.scatter(u[:,0]*dx, u[:,2], s=5, color='blue', label=r'$c_{0}$')
    # plt.scatter(u[:,0]*dx, u[:,3], s=5, color='red', label=r'$c_{1}$')
    plt.plot(xtheo, np.sin(xtheo), color='red',linewidth=1, alpha=0.7)
    plt.plot(xtheo, np.sin(xtheo-a*i*dt), color='red',linewidth=1, linestyle='dashed')#, alpha=0.2)
    for x in range(nx+3):
        plt.vlines(x=xmin-dx+x*dx, ymin=-1, ymax=1, color='tab:blue',linewidth=0.5,alpha=0.6,linestyle='dashed')
    plt.xlabel(r"$x$", fontsize=labelsize)
    plt.ylabel(r'$u$', fontsize=labelsize)
    plt.xticks(fontsize=ticksize)
    plt.yticks(fontsize=ticksize)
    plt.title("t = " + str(round(i*dt,1)),fontsize=titlesize)
    # plt.legend()
    plt.xlim(-4,4)
    plt.ylim(-1.1,1.1)
    plt.grid()
    plt.savefig(resdirec+"dg"+nstr+"_nx"+str(nx)+"_nb"+str(nb)+"_t"+str(round(i*dt,1))+".png")
    plt.show()


