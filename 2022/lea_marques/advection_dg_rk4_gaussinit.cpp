#include <iostream> // header file
#include <string>
#include <cmath>
#include <fstream>
#include <vector>
#include <array>
#include </home/lmarques/Downloads/METEORs/M5-Fluids/project/legendre.hpp>

//initial condition defined over whole physical domain
//argument is the physical position on the domain of definition
double init_cond(double x) {
  return sin(x);
}

//Order of DG
const int n = 6;
const std::string nstr = std::to_string(n);
//Order of Gauss quadrature (for advection, must be the same as n for quadrature to be exact)
const int m = 6;


// Defining the spatial grid
const double xmin = -3.14;
const double xmax = 3.14;
const int nx = 1;
const std::string nxstr = std::to_string(nx);
const double dx = (xmax- xmin)/nx;

double integral(double cell_center, int idg, std::vector<double>  omega, std::vector<double> xgauss) {
  
  double integr = 0;
  
  for(int igauss=0; igauss<m; igauss++){
    
    double term = omega.at(igauss) * init_cond(cell_center+xgauss.at(igauss)) * Legendre::Pn(idg,xgauss.at(igauss)*2/dx);
    
    integr += term;
  }
  
  return integr;
}

//advection flux
double flux(double u, double char_speed) {
  // u should be calculated at the right position !
  return char_speed*u;
}


double gauss_scheme_cell(std::vector<double> omega, std::vector<double> xgauss, int idg, std::array<double,n> ctemp_icell, double a) {
  
  double integr = 0;
  
  for(int igauss=0; igauss<m; igauss++) {

    //calculating u at xgauss
    double u = 0;
    
    for(int idg_u=0; idg_u<n; idg_u++) {
      u += ctemp_icell[idg_u]*Legendre::Pn(idg_u,xgauss.at(igauss)*2./dx);
    }

    // calculating the term associated to the first step of the Gauss sum
    double term = 2/dx * omega.at(igauss) * flux(u,a) * Legendre::DPn(idg,xgauss.at(igauss)*2./dx);

    //incrementing the integral by this term
    integr += term;

    //std::cout << "u(x_" << igauss+1 << "))" << "\t" << "omega_" << igauss+1 << "\t" << "flux(u(x_" << igauss+1 << ")" << "\t" << "DP_" << idg << "(x_" << igauss+1 << ")" << "\t" << "term" << "\t" << "integral" << std::endl;
    //std::cout << u << "\t" << omega.at(igauss) << "\t" << flux(u,a) << "\t" << Legendre::DPn(idg,xgauss.at(igauss)*2./dx) << "\t" << term << "\t" << integr << std::endl;
  }
  //exit(0);
  return integr;
}
      


double getU(std::vector<std::array<double, n>> const& c, int cell, double x) {
  int nx = c.size()-2;
  double u = 0;
  for(int idg_u=0; idg_u<n; idg_u++){
    u += c.at(cell)[idg_u]*Legendre::Pn(idg_u,x);
  }
  return u;
}



void boundaryCondition(std::vector<std::array<double,n>>& k) {
  //boundary conditions
  int nx=k.size()-2;
  for(int idg=0; idg<n; idg++){
    k.at(0)[idg] = k.at(nx)[idg];
    k.at(nx+1)[idg] = k.at(1)[idg];
    //std::cout << "ok" << std::endl;
  }
}



int main(int argc, char** argv)
{
  std::cout << "dx = " << dx << std::endl;
  
  //Characteristic velocity (at least for advection)
  double a = 1;

  //Defining the temporal grid
  double tmax = 1;
  double tmin = 0;
  double CFL = 15;
  double dt = dx/a/CFL;
  int nt = 3.5*CFL*nx;
  //1 revolution takes CFL*nx=(xmax-xmin)/(adt) time steps so here simulation will last 5 revolutions

  //Declaring the u vector and initializing each cell to zero
  int nb = 50;
  std::vector<double> ui((nb+2)*(nx+2), 0); //i for initial

  

  //INITIALISATION

  //Declaring the array for c initial (maybe better with vectors if n or nx very large) and initialising each cell to zero
  //std::array<std::array<double, n>, nx+2> ci;
  //std::vector<std::array<double, n>> ci(nx+2, {0});
  std::vector<std::array<double, n>> ci(nx+2);
  
  //Gauss
  //Defining the order of the quadrature (maybe do it as a user input with std::cin)

  double omega1 = 1*dx/2; //*dx/2 bc we must adapt it to the width of the cell
  double omega2 = 1*dx/2;
  double x1 = -1./sqrt(3)*dx/2; //in the right domain depending on how P is defined
  double x2 = 1./sqrt(3)*dx/2;

  std::vector<double> omega = {0.171324492*dx/2,
  			       0.360761573*dx/2,
                         0.467913935*dx/2,
                         0.467913935*dx/2,
                         0.360761573*dx/2,
                         0.171324492*dx/2};
  std::vector<double> xgauss = {-0.932469514*dx/2,
  			        -0.661209386*dx/2,
  			        -0.2386191860*dx/2,
  			        0.2386191860*dx/2,
  			        0.661209386*dx/2,
  			        0.932469514*dx/2};

  
  //std::vector<double> omega={omega1,omega2};
  //std::vector<double> xgauss={x1, x2};

 
  std::ofstream out("adv_gauss_dg" + nstr +"_rk4_nx"+nxstr+"_simu_0.txt");
  out << "x" << "\t" << "u" << std::endl;

  //Calculating the c
  for(int icell=0; icell<nx+2; icell++){   
    for(int idg=0; idg<n; idg++){
      ci.at(icell)[idg] = (2*idg+1)/dx * integral(xmin-dx+icell*dx+dx/2,idg,omega,xgauss);
    }
  }

  //Calculating the u
  for(int icell=0; icell<nx+2; icell++){    
    for(int isub=0; isub<nb+2; isub++){
      int ipt = (nb+2)*icell + isub;
      ui.at(ipt) += getU(ci, icell, -1 + isub*2./(nb+1));
      out << xmin-dx + icell*dx + isub*dx/(nb+1) << "\t" << ui[ipt]  << std::endl;
    }
  }
  
  // IMPLEMENTING THE NUMERICAL SCHEME

  std::vector<double> uf((nb+2)*(nx+2), 0);
  std::vector<std::array<double, n>> ctemp(nx+2);
  std::vector<std::array<double, n>> cf(nx+2);
  std::vector<std::array<double, n>> k1(nx+2, {0});
  std::vector<std::array<double, n>> k2(nx+2, {0});
  std::vector<std::array<double, n>> k3(nx+2, {0});
  std::vector<std::array<double, n>> k4(nx+2, {0});

  std::string filename;
  filename = "adv_gauss_dg" +nstr+"_rk4_nx"+nxstr+"_simu_";
  
  for(int it=1; it<nt; it++){

    std::cout << "t = " << it << std::endl;
    
    ctemp = ci;
    
    for(int icell=1; icell<nx+1; icell++) {
      
      //for the numerical flux
      double uhere;
      double ubehind;
      uhere = getU(ctemp, icell, 1);
      ubehind = getU(ctemp, icell-1, 1);
      
      for(int idg=0; idg<n; idg++){
	k1.at(icell)[idg] = (2.*idg+1)/dx * (gauss_scheme_cell(omega, xgauss, idg, ctemp.at(icell),a) - (flux(uhere,a)*Legendre::Pn(idg,1) - flux(ubehind,a)*Legendre::Pn(idg,-1)));
      }
    }

    boundaryCondition(k1);

    //updating the ctemp for k2
    for(int icell=0; icell<nx+2; icell++){
      for(int idg=0; idg<n; idg++){
	ctemp.at(icell).at(idg) = ci.at(icell).at(idg) + dt/2 * k1.at(icell)[idg];
      }
    }
  
    for(int icell=1; icell<nx+1; icell++){
      double uhere;
      double ubehind;
      uhere = getU(ctemp, icell, 1);
      ubehind = getU(ctemp, icell-1, 1);

      for(int idg=0; idg<n; idg++){
	k2.at(icell)[idg] = (2.*idg+1)/dx * (gauss_scheme_cell(omega, xgauss, idg, ctemp.at(icell),a) - (flux(uhere,a)*Legendre::Pn(idg,1) - flux(ubehind,a)*Legendre::Pn(idg,-1)));
      }
    }

    boundaryCondition(k2);


    //updating the ctemp for k3
    for(int icell=0; icell<nx+2; icell++){
      for(int idg=0; idg<n; idg++){
	ctemp.at(icell).at(idg) = ci.at(icell).at(idg) + dt/2 * k2.at(icell)[idg];
      }
    }

    for(int icell=1; icell<nx+1; icell++){
      double uhere;
      double ubehind;
      uhere = getU(ctemp, icell, 1);
      ubehind = getU(ctemp, icell-1, 1);

      for(int idg=0; idg<n; idg++){
	k3.at(icell)[idg] = (2.*idg+1)/dx * (gauss_scheme_cell(omega, xgauss, idg, ctemp.at(icell),a) - (flux(uhere,a)*Legendre::Pn(idg,1) - flux(ubehind,a)*Legendre::Pn(idg,-1)));
      }
    }

    boundaryCondition(k3);

 

    //updating the ctemp for k4
    for(int icell=0; icell<nx+2; icell++){
      for(int idg=0; idg<n; idg++){
	ctemp.at(icell).at(idg) = ci.at(icell).at(idg) + dt * k3.at(icell)[idg];
      }
    }

    for(int icell=1; icell<nx+1; icell++){
      double uhere;
      double ubehind;
      uhere = getU(ctemp, icell, 1);
      ubehind = getU(ctemp, icell-1, 1);

      for(int idg=0; idg<n; idg++){
	k4.at(icell)[idg] = (2.*idg+1)/dx * (gauss_scheme_cell(omega, xgauss, idg, ctemp.at(icell),a) - (flux(uhere,a)*Legendre::Pn(idg,1) - flux(ubehind,a)*Legendre::Pn(idg,-1)));
      }
    }

    boundaryCondition(k4);

    for(int icell=0; icell<nx+2; icell++){
      for(int idg=0; idg<n; idg++){
	cf.at(icell)[idg] = ci.at(icell).at(idg) + dt/6 * (k1.at(icell)[idg] + 2*k2.at(icell)[idg] + 2*k3.at(icell)[idg] + k4.at(icell)[idg]);
      }
    }
  
    std::string newFilename = filename + std::to_string(it) + ".txt";
    std::ofstream out(newFilename);
    out << "physical position" << "\t" << "ui" << std::endl;
    
    //Calculating uf
    for(int icell=0; icell<nx+2; icell++){
      for(int isub=0; isub<nb+2; isub++){
	int ipt = (nb+2)*icell + isub;
	uf.at(ipt) = getU(cf,icell,-1+isub*2./(nb+1));
	
      out << xmin-dx + icell*dx + isub*dx/(nb+1) << "\t" << uf.at(ipt)  << std::endl;
      //std::cout << "uf = " << uf.at(ipt) << std::endl;
      }
    }

    ci = cf;   
  
  }   
}
