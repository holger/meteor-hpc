#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 14:26:47 2022

@author: lmarques
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

direc = "/home/lmarques/Downloads/METEORs/M5-Fluids/project/advection/c_vector/gauss+c-vectors/test_vectorise_ctemp/test-getU/test-vector-array/test_error/"

n = 
nstr = str(n)

a = 1.

nb = 50

nx = np.array([3,4])#2,4,6,8,16,20,32,64,128,200,256,512,1024,2000])#,20,200,2000])
errL2 = np.zeros(np.shape(nx))



 
#initial condition
def initcond(x):
    return np.sin(x)

# time at which I will measure the error:
errstep = 2

for i in range(len(nx)):
    xmax = 2*3.14 + 2*2*3.14/nx[i]
    xmin = 0
    dx = (xmax-xmin)/nx[i]
    dt = dx/a/15
    filename = "adv_gauss_dg" + nstr +"_rk4_nx" + str(nx[i]) + "_simu_"
    u = np.loadtxt(direc+filename+str(errstep)+'.txt', skiprows=1)
    x = u[:,0]
    unum = u[:,1]
    uref = initcond(x-a*errstep*dt)
    
    for j in range(0,len(x),nb+2):
        errL2[i] += (unum[j] - uref[j])**2
    errL2 *= dx


plt.figure()
plt.scatter(np.log10(nx),np.log10(errL2))
plt.grid()


arr = np.array([nx,errL2])



df = pd.DataFrame({'nx' : nx,
                   'error' : errL2
                   })



direc_res = "/home/lmarques/Downloads/METEORs/M5-Fluids/project/advection/results/"
# df.to_csv(direc_res+"errors_order"+nstr+".csv")


# plt.figure()
# plt.scatter(np.log10(df.nx),np.log10(df.error))