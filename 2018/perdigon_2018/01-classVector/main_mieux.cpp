// g++ -o main main.cpp

#include <iostream>

class Vector
{
public:

  Vector(int length) {
    data_ = new double[length];
    length_ = length;
  }

  double& operator[](int i) {
    if(i < 0 || i > length_-1) {
      std::cerr << "ERROR: i = " << i
		<< " length = " << length_ << std::endl;
      exit(1);
    }
      
    return data_[i];
  }

  int get_length() {
    return length_;
  }
  
private:

  double* data_;
  int length_;  
};

void output(Vector vec)
{
  for(int i=0; i<vec.get_length(); i++)
    std::cout << i << "\t" << vec.value(i) << std::endl;
}

int main()
{
  double value = 5;
  double* valueP = &value;

  std::cout << "value = " << value << std::endl;
  std::cout << "&value = " << &value << std::endl;

  std::cout << "valueP = " << valueP << std::endl;
  std::cout << "&valueP = " << &valueP << std::endl;
  std::cout << "*valueP = " << *valueP << std::endl;

  int length = 12;
  
  double* valueVec = new double[length];
  for(int i=0; i<length; i++) {
    valueVec[i] = i;
  }
  
  for(int i=0; i<length; i++) {
    std::cout << i << "\t" << valueVec[i] << std::endl;
  }

  Vector vec(length);
  for(int i=0; i<vec.get_length(); i++) {
    vec[i] = i;
    
  output(vec);
}
