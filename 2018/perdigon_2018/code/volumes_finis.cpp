#include <iostream>
#include <math.h>  // Fonctions de base maths
#include <cstdlib> // pour le exit
#include <fstream> // Ouverture/fermeture/lecture/ecriture fichiers
#include <time.h>

#include "array.hpp"


// #######################  FONCTIONS #############################


double sqr(double value) // Calcul du carré d'un nombre
{
  return value*value;
}


double err_norm_L2(Array const& u1, Array const& u2, double dx){ // Calcul la norme L2 de l'erreur

  int nx1 = u1.get_length();
  int nx2 = u2.get_length();

  if(nx1 != nx2){
     std::cerr << "lengths do not match ! " << std::endl;
     exit(1);
    }

  double sum = 0;

  for( int i=0; i < nx1; i++){
    sum = sum + sqr(u1[i]-u2[i]);
  }
    
    return sqrt(dx*sum);	 
  }

void write_erreur(char * filename, int nx, double err){ // Ecriture de l'erreur dans le fichier filename
  
  std::ofstream out(filename ,std::ios::app);
  out << nx << " " << err << std::endl;
  out.close();
}

void write_data(char * filename, Array const& x, Array const& u){ // Ecriture de x et u pour un t donné

  int nx1 = x.get_length();
  int nx2 = u.get_length();

  if(nx1 != nx2){
    std::cerr << "lengths do not matches" <<std::endl;
    exit(1);
  }
  
  std::ofstream out(filename ,std::ios::app);
  for(int i=0; i<nx1; i++){
    out << x[i] << " " << u[i] << std::endl;
  }  
  out.close();
}

void bound_periodic_conditions(Array& un){
  
  int nx = un.get_length();
  
  un[0]=un[nx-4]; 
  un[1]=un[nx-3];
  un[nx-2]=un[2];
  un[nx-1]=un[3];
}

inline double flux_right(int i, double dx, double a, double pos, Array const& u){
  return a*(u[i] + (((0.5*dx-pos)/dx)*((u[i+1]-u[i-1])/2.0)) );
    
}

inline double rhs(int i, double dx, double a, double pos, Array const& u){
  return (1.0/dx)*(flux_right(i-1,dx,a,pos,u)-flux_right(i,dx,a,pos,u));
}

void euler(Array& un, Array const& u, double dt, double dx, double a, double pos){ 

  int nx = un.get_length();
 
  for(int i=2; i<nx-2; i++){
    un[i]= u[i] + dt*rhs(i,dx,a,pos,u);
  }
  bound_periodic_conditions(un);
}

void rk2(Array& un , Array const& u , double dt, double dx, double a, double pos){

  int nx = un.get_length();
  
  Array unhalf(nx);

  euler(unhalf, u, 0.5*dt, dx, a, pos);
  
  for(int i=2; i<nx-2; i++){
    un[i]= u[i] + dt*rhs(i,dx,a,pos,unhalf);
  }
  
  bound_periodic_conditions(un);
}


// ######################## MAIN ################################################

int main(int argc, char ** argv){ // argc: nombre d'arguments à passer, char ** la liste de strings d'arguments
  
  if(argc<2){ 
    std::cerr << "Erreur: need parameter ( int nx, char * dataname, char * err_name)" << std::endl;
    exit(1);
  }
  
  int nx = atoi(argv[1]); // atoi: transforme un character en un integer
  double xmin = 0.;
  double xmax = 5.;
  double dx = (xmax-xmin)/(nx-1);
  double L = (nx-4)*dx; // Longueur du domaine -1 incrément
  double  a = 1; // Vitesse
  double cfl = 0.1; // A prendre entre 0 et 1 pour être stable (CFL)
  double dt = cfl*dx/a;
  int  nT = round((nx-4)*dx/(a*dt)); // nombre d'itérations correspondant à une période de la simulation
  int nt=nT; // nbre d'itérations de la simulation.
  int n =1;
  double k = 2*M_PI*n/L; // vecteur d'onde

  // Création des vecteurs x et u:

  Array x(nx);
  Array u(nx);
  Array un(nx); 

  Array u0(nx); 
  Array uT(nx); 

  // Remplissage des vecteurs x et u:
  
  for(int i=0;i<nx;i++){

    x[i]=xmin+i*dx;
    u0[i]=sin(k*x[i]);
    u[i]=u0[i];
  }
  
  int flag1 = atoi(argv[2]);
  double pos;

  if(flag1 == 0){ //upwind
    pos = 0.5*dx;
  }

  else if(flag1 == 1){ //fromm
    pos = 0.5*a*dt;
  }

  else if(flag1 == 2){ // bord
    pos = 0;
  }

  else{
    std::cerr << "Wrong pos flag" << std::endl;
    exit(1);
  }
  
  int flag2 = atoi(argv[3]);

  double elapsed = 0;
  double start = clock();

  if(flag2==1){
    for(int it=0; it<nt;it++){
     euler(un, u, dt, dx, a, pos);
     u=un;
    }
  }

  else if(flag2==2){
    for(int it=0; it<nt;it++){ 
     rk2(un,u, dt, dx, a, pos);
     u=un;
    }
  }

  else{
    std::cerr << "Wrong time flag" << std::endl;
    exit(1);
  }

  double end = clock();
  elapsed = (end - start)/CLOCKS_PER_SEC;
  write_erreur(argv[4],nx,elapsed);
  
  
  uT=un;
  /*
  char filename1[7] = {'u','0','.','d','a','t'};
  char filename2[7] = {'u','f','.','d','a','t'};
  write_data(filename1,x,u0);
  write_data(filename2,x,uT);
  */
  
  double err = err_norm_L2(u0,uT,dx);
  write_erreur(argv[5],nx,err); 
  
}
