#include <iostream>
#include <math.h>  // Fonctions de base maths
#include <cstdlib> // pour le exit
#include <fstream> // Ouverture/fermeture/lecture/ecriture fichiers
#include <time.h> // Pour la mesure du temps d'exécution

// importation des objets: vecteurs et matrices

#include "array.hpp"
#include "matrix.hpp"


// ############################ FONCTIONS ##############################
template<class TYPE>
inline double intPow(TYPE x, int potenz)
{
  if(potenz == 0)
    return 1.;
  double tmp = x;
  if(potenz < 0){
    for(int i=0;i<-potenz-1;i++){
      tmp *= x;
    }
    return 1/tmp;
  }
  else
    for(int i=0;i<potenz-1;i++){
      tmp *= x;
    }
  return tmp;
}

inline int factorial(int n)
{
	int facto, i;
	facto=1;
	i=1;

	while (i<=n)
	{
		facto=facto*i;
		i=i+1;
	}
	return facto;
}

// polynôme legendre ( -1 <= x <= 1 )

inline double poly_leg( int n, double x){
  
  double sum=0;
  
  for(int k=0; k<=n; k++){
    sum=sum+1.0*intPow(factorial(n)/(factorial(n-k)*factorial(k)),2)*intPow(x-1,n-k)*intPow(x+1,k);
  }
  
  return sum/intPow(2,n);
}

// Dérivée polynôme de legendre ( -1 <= x <= 1 )

inline double ddx_poly_leg(int n, double x){
  
  double sum=0;
  
  for(int k=0; k<=n; k++){
    sum+=intPow(factorial(n)/(factorial(n-k)*factorial(k)),2)*intPow(x-1,n-k)*intPow(x+1,k)*((n-k)/(x-1)+(k/(x+1)));
  }


  return 1.0*sum/intPow(2,n);
}

double f_init(int i, int j, double k, double dx, double x){
  return sin(k*dx*(0.5*x+i))*poly_leg(j,x);
}

inline void gauss_quadrature(double k, double dx, Matrix& C0){

  double w1 = 5.0/9;
  double w2 = 8.0/9;
  double w3 = 5.0/9;
  
  double x1 = -1.0*sqrt(3.0/5);
  double x2 = 0;
  double x3 = sqrt(3.0/5);

  for(int i=0; i<C0.get_line(); i++){
    for(int j=0; j<C0.get_col(); j++){
      C0(i,j)=((2*j+1)/2.0)*(w1*f_init(i,j,k,dx,x1) + w2*f_init(i,j,k,dx,x2) + w3*f_init(i,j,k,dx,x3));
    }
  }
}

void write_data(char * filename, int i, double dx, Matrix const& C, int npoints){
  int P = C.get_col();
  std::ofstream out(filename ,std::ios::app);
  double pas = dx/(npoints-1);
    for(int l=0; l<npoints; l++){
      double x = (i-0.5)*dx+l*pas;
      double sum = 0;
      for(int j=0; j<P; j++){
	sum+=C(i,j)*poly_leg(j,(2/dx)*(x-i*dx));
      }
      out << x << " " << sum  << std::endl;
    }
  out.close();
}

inline double flux_right(int i, double a, double dx, Matrix const& C){
  double sum = 0;
  int P = C.get_col();
  for(int j=0; j<P; j++){
    sum+=C(i,j);
  }
  return a*sum; 
}

inline double flux_poly(int i, int j, double x, double a, double dx, Matrix const& C){
  double sum = 0;
  int P = C.get_col();
  for(int l=0; l<P; l++){
    sum+=C(i,l)*poly_leg(l,x);
  }
  sum=sum*a;
  return sum*ddx_poly_leg(j,x);
}

inline double flux_integral(int i,int j, double a, double dx, Matrix const& C){

  double w1 = 5.0/9;
  double w2 = 8.0/9;
  double w3 = 5.0/9;

  double x1 = -sqrt(3.0/5);
  double x2 = 0;
  double x3 = sqrt(3.0/5);

  return (w1*flux_poly(i,j,x1,a,dx,C) + w2*flux_poly(i,j,x2,a,dx,C) + w3*flux_poly(i,j,x3,a,dx,C));
  
}

inline void periodic_conditions(Matrix& C){
  
  int nx = C.get_line();
  int P = C.get_col();
  
  for(int j=0; j<P; j++){
     C(0,j)=C(nx-4,j);
     C(1,j)=C(nx-3,j);
     C(nx-2,j)=C(2,j);
     C(nx-1,j)=C(3,j);
  }
}


inline double right_hand_side(int i, int j, double a, double dx, Matrix const& C){
 
  return ((2*j+1)/dx)*(flux_integral(i,j,a,dx,C) - flux_right(i,a,dx,C) + intPow((-1),j)*flux_right(i-1,a,dx,C));
}

void euler(double dt, double a, double dx, Matrix& Cn, Matrix const& C){
  int nx = Cn.get_line();
  int P = Cn.get_col();
  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
      Cn(i,j)=C(i,j)+dt*right_hand_side(i,j,a,dx,C);
    }
  }
  periodic_conditions(Cn);
}

void rk2(double dt, double a, double dx, Matrix& Cn, Matrix const& C){

  int nx = Cn.get_line();
  int P = Cn.get_col();
  
  Matrix Cint(nx,P);
  euler(0.5*dt,a,dx,Cint,C);
 
  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
       Cn(i,j)=C(i,j)+dt*right_hand_side(i,j,a,dx,Cint);
    }
  }
  periodic_conditions(Cn);
}

void rk4(double dt, double a, double dx, Matrix& Cn, Matrix const& C){

  int nx = Cn.get_line();
  int P = Cn.get_col();
  
  Matrix Cint(nx,P);

  Matrix k1(nx,P);
  Matrix k2(nx,P);
  Matrix k3(nx,P);
  Matrix k4(nx,P);

  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
      k1(i,j) = right_hand_side(i,j,a,dx,C);
    }
  }
  periodic_conditions(k1);

  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
      Cint(i,j)=C(i,j)+0.5*dt*k1(i,j);
    }
  }
  periodic_conditions(Cint);


  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
      k2(i,j)=right_hand_side(i,j,a,dx,Cint);
    }
  }
  periodic_conditions(k2);

  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
      Cint(i,j)=C(i,j)+0.5*dt*k2(i,j);
    }
  }
  periodic_conditions(Cint);

  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
      k3(i,j)=right_hand_side(i,j,a,dx,Cint);
    }
  }
  periodic_conditions(k3);

  for(int i=2; i<nx-2; i++){
    for(int j=0; j<P; j++){
      Cint(i,j)=C(i,j)+dt*k3(i,j);
    }
  }
  periodic_conditions(Cint);

  for(int i=2; i<nx-2; i++){
   for(int j=0; j<P; j++){
     k4(i,j)=right_hand_side(i,j,a,dx,Cint);
    }
  }
  periodic_conditions(k4);

  for(int i=2; i<nx-2; i++){
   for(int j=0; j<P; j++){
     Cn(i,j)=C(i,j)+(dt/6)*(k1(i,j)+2*(k2(i,j)+k3(i,j))+k4(i,j));
    }
  }
  periodic_conditions(Cn); 
  
}

double err_norm_L2(Array u1, Array u2, double dx){ // Calcul la norme L2 de l'erreur

  int nx1 = u1.get_length();
  int nx2 = u2.get_length();
  
  if(nx1 != nx2){
    std::cerr << "lengths do not match ! " << std::endl;
    exit(1);
  }

  double sum = 0;
  for(int i=0; i < nx1; i++){
    sum = sum + (u1[i]-u2[i])*(u1[i]-u2[i]);
    }

  return sqrt(dx*sum);
}

Array build_array(double dx, Matrix const& C, int npoints){
  int nx = C.get_line();
  int P = C.get_col();
  Array u(nx*npoints);
  for(int i=0; i<nx; i++){
    for(int l=0; l<npoints; l++){
      double x = -1+l*(2.0/(npoints-1));
      double sum = 0;
      for(int j=0; j<P; j++){
	sum+=C(i,j)*poly_leg(j,x);
      }
      u[i*npoints+l]=sum;
    }    
  }
  return u;
}

void write_erreur(char * filename, int nx, double err){
  std::ofstream out(filename ,std::ios::app);
  out << nx << " " << err  << std::endl;
  out.close();
}



// ########################### MAIN ####################################
int main(int argc, char ** argv){ // argc: nombre d'arguments à passer, char ** la liste de strings d'arguments
  
  if(argc<2){ 
    std::cerr << "Erreur: need parameter" << std::endl;
    exit(1);
  }
  
  int nx = atoi(argv[1]); // atoi: transforme un character en un integerint main(){
  int P = atoi(argv[2]); // ordre du schéma
  double xmin = 0.;
  double xmax = 5.;
  double dx = (xmax-xmin)/(nx-1);
  double L = (nx-4)*dx; // Longueur du domaine -1 incrément
  double  a = 1.0; // Vitesse
  double sigma = 0.1; // A prendre entre 0 et 1 pour être stable (CFL)
  double dt = sigma*dx/a;
  int  nT = round((nx-4)/sigma); // nombre d'itérations correspondant à une période de la simulation
  int nt=nT; // nbre d'itérations de la simulation.
  int n =1;
  double k = 2*M_PI*n/L; // vecteur d'onde

  Matrix C0(nx,P);
  Matrix C(nx,P);
  Matrix Cn(nx,P);

  gauss_quadrature(k,dx,C0);
  C=C0;
  
  int flag1 = atoi(argv[3]);
  
  double elapsed = 0;
  double start = clock();

  if(flag1 == 1){
    for(int it=0; it<nt ; it++){
      euler(dt,a,dx,Cn,C);
      C=Cn;
    }
  }

  else if(flag1 == 2){
    for(int it=0; it<nt ; it++){
      rk2(dt,a,dx,Cn,C);
      C=Cn;
    }
  }

  else if(flag1 == 4){
    for(int it=0; it<nt ; it++){
      rk4(dt,a,dx,Cn,C);
      C=Cn;
    }
  }

  else{
    std::cerr << "Wrong time flag" <<std::endl;
    exit(1);
  }
  
  double end = clock();
  elapsed =(end - start)/CLOCKS_PER_SEC;
  write_erreur(argv[4],nx,elapsed);
  
  /*
  char filename1[7] = {'u','0','.','d','a','t'};
  char filename2[7] = {'u','f','.','d','a','t'};
   for(int i=0; i<nx; i++){
     writedata(filename2,i,dx,Cn,10);
     writedata(filename1,i,dx,C0,10);
  }
  */
  
  Array u0 = build_array(dx,C0,10);
  Array uT = build_array(dx,Cn,10);
  double erreur = err_norm_L2(u0,uT,dx);
  write_erreur(argv[5],nx,erreur);
  
}
  

