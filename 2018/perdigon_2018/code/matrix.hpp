#ifndef matrix_hpp
#define matrix_hpp

#include "array.hpp"
#include <iostream>

class Matrix{
public:

  // Constructeurs:
  
  Matrix(int N, int M){
    data_ = new double*[N];
    for(int i=0; i<N; i++){
      data_[i] = new double[M];
    }
    N_ = N;
    M_ = M;
  }

  Matrix(Matrix const& mat){
    N_ = mat.N_;
    M_ = mat.M_;
    data_ = new double*[N_];
    for(int i=0; i<N_; i++){
      data_[i] = new double[M_];
    }
    for(int i=0; i<N_; i++){
      for(int j=0; j<M_; j++){
	data_[i][j] = mat.data_[i][j];
      }
    }
  }

  // Destructeurs:

  ~Matrix(){
    for(int i=0; i<N_; i++){
      delete [] data_[i];
    }
    delete [] data_;
  }

  // Opérateurs:

  double& operator()(int i, int j){ // Pour l'écriture des valeurs
    // if (i<0 || i >= N_ || j<0 || j>=M_){
    //   std::cerr << "ERROR: (i,j) = (" << i << "," << j 
    // 		<< ") and dim = (" << N_ << "," << M_ << ")" << std::endl;
    //    exit(1);
    // }

    return data_[i][j];
  }

  double operator()(int i, int j) const { // Pour la lecture des valeurs
    // if (i<0 || i >= N_ || j<0 || j>=M_){
    //   std::cerr << "ERROR: (i,j) = (" << i << " ; " << j 
    // 		<< ") and dim = ( " << N_ << ";" << M_ << ")" << std::endl;
    //    exit(1);
    // }

    return data_[i][j];
  }

  Matrix& operator=(Matrix const& mat){ // Egaliser 2 matrices

    if(N_ != mat.N_ || M_ != mat.M_){
      std::cerr << "ERROR: N_ or M_ != mat.N_ or mat.M_" << std::endl;
       exit(1);
    }
    for(int i=0; i<N_; i++){
      for(int j=0; j<M_; j++){
	data_[i][j] = mat.data_[i][j];
      } 
    }
  }

  Matrix operator*(Matrix const& mat){ // Produit matriciel
    
    if(M_ != mat.N_){
      std::cerr << "ERROR: M_ != mat.N_" << std::endl;
       exit(1);
    }
    Matrix R(N_,mat.M_);
  
    
    for(int k=0; k<N_; k++){
      for(int l=0; l<mat.M_; l++){
	R(k,l)=0;
	for(int i=0; i<M_; i++ ){
	  R(k,l)=R(k,l)+data_[k][i]*mat.data_[i][l];
	}
      } 
    }
    return R;
  }

  Matrix operator+(Matrix const& mat){ // somme de deux matrices
    
    if(M_ != mat.M_ || N_ != mat.N_){
      std::cerr << "ERROR: dimension does not match !" << std::endl;
       exit(1);
    }
    Matrix R(N_,M_);
  
    for(int i=0; i<N_; i++){
      for(int j=0; j<M_; j++){
	R(i,j)=data_[i][j]+mat.data_[i][j];
      } 
    }
    return R;
  }

   Matrix operator*(double k){ // multiplication avec un scalaire
    
    Matrix R(N_,M_);
  
    for(int i=0; i<N_; i++){
      for(int j=0; j<M_; j++){
	R(i,j)=k*data_[i][j];
      } 
    }
    return R;
  }

  Array operator*(Array vec){ // Produit matrice vecteur
    
    if(M_ != vec.get_length()){
      std::cerr << "ERROR: M_ != vec.get_length()" << std::endl;
       exit(1);
    }
    Array u(N_);
  
    for(int i=0; i<N_; i++){
      u[i]=0;
      for(int k=0; k<M_; k++ ){
	u[i]=u[i]+data_[i][k]*vec[k];
      }
    }
    return u;
  }

  // Fonction de classe:

  int get_line() const { // retourner la dimension de la matrice
    return N_;
  }
   int get_col() const { // retourner la dimension de la matrice
    return M_;
  }

private:
  int N_;
  int M_;
  double** data_;
  
};

#endif
