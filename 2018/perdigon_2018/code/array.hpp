#ifndef array_hpp
#define array_hpp

#include <iostream>

class Array{
  
public:

  // Constructeurs:

  Array(int length){
    data_ = new double[length];
    length_ = length;
  }

  Array(Array const& vector){
    length_ = vector.length_;
    data_ = new double[vector.length_];

    for(int i=0; i<length_; i++){
      data_[i] = vector.data_[i];
    }
  }

  // Destructeurs:

  ~Array(){
    delete [] data_;
  }

  // Opérateurs:

  inline double& operator[](int i){ // Pour l'écriture des valeurs
    /*
    if (i<0 || i > length_-1){
      std::cerr << "ERROR: i " << i
		<< "length = " << length_ << std::endl;
       exit(1);
    }
    */

    return data_[i];
  }

  inline double operator[](int i) const { // Pour la lecture des valeurs
    /*
    if (i<0 || i > length_-1){
      std::cerr << "ERROR: i " << i
		<< "length = " << length_ << std::endl;
      exit(1);
    }
    */
    return data_[i];
  }

  Array& operator=(Array const& vector){ // Egaliser 2 vecteurs
    if(length_ != vector.length_){
      std::cerr << "ERROR: length_ != vector.length_" << std::endl;
       exit(1);
    }
    for(int i=0; i<length_; i++){
      data_[i] = vector.data_[i];
    }
    return *this;
  }

  
  // Fonction de classe:

  int get_length() const{ // retourner la longueur du vecteur
    return length_;
  }

private:

  int length_;
  double* data_;
  
};

#endif
