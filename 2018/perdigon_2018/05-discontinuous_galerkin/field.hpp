#ifndef field_hpp
#define field_hpp

#include "matrix.hpp"

// polynôme legendre ( -1 <= x <= 1 )

double poly_leg( int n, double x){
  
  double sum=0;
  
  for(int k=0; k<=n; k++){
    sum=sum+1.0*pow(factorial(n)/(factorial(n-k)*factorial(k)),2)*pow(x-1,n-k)*pow(x+1,k);
  }
  
  return sum=sum*1.0/pow(2,n);
}

class Field
{
public:

  Field(int nx, int order, double dx) : coeff_(nx, order) {
    dx_ = dx;
  }

  int get_nx() const {
    return coeff_.get_line();
  }

  int get_order() const {
    return coeff_.get_col();
  }

  double get_dx() const {
    return dx_;
  }

  double value(int cell, double x) const {
    double sum = 0;
    for(int j=0; j<get_order(); j++){
      sum+=coeff_(cell,j)*poly_leg(j,x); 
    }
  }

  double operator()(int i, int j) const {
    return coeff_(i,j);
  }

  double& operator()(int i, int j) {
    return coeff_(i,j);
  }

  double position(int cell, double x) const {
    return cell*dx_ + x*dx_;
  }
  
private:
  
  double dx_;
  Matrix coeff_;
};

#endif
