#include <iostream>
#include <math.h>  // Fonctions de base maths
#include <cstdlib> // pour le exit
#include <fstream> // Ouverture/fermeture/lecture/ecriture fichiers

// importation des objets: vecteurs et matrices

#include "array.hpp"
#include "matrix.hpp"



// ############################ FONCTIONS ##############################

int factorial(int n)
{
	int facto, i;
	facto=1;
	i=1;

	while (i<=n)
	{
		facto=facto*i;
		i=i+1;
	}
	return facto;
}

// Dérivée polynôme de legendre ( -1 <= x <= 1 )

double ddx_poly_leg(int n, double x){
  
  double sum=0;
  
  for(int k=0; k<=n; k++){
    sum+=pow(factorial(n)/(factorial(n-k)*factorial(k)),2)*pow(x-1,n-k)*pow(x+1,k)*((n-k)/(x-1)+k/(x+1));
  }
  return sum=sum*1.0/pow(2,n);
}

double f_init(double k, double dx, double x, int i, int j){
  return sin(k*dx*(0.5*x+i))*poly_leg(j,x);
}

/*
double gauss_quadrature(double k, double i, double j, double dx, double nx, double P){

  double w1 = 5.0/9;
  double w2 = 8.0/9;
  double w3 = 5.0/9;

  //Matrix R(nx,P);

  double x1 = -1.0*sqrt(3.0/5);
  double x2 = 0;
  double x3 = sqrt(3.0/5);

  return (2*j+1)/2.0*(w1*f_init(k,dx,x1,i,j) + w2*f_init(k,dx,x2,i,j) + w3*f_init(k,dx,x3,i,j));
}
*/

void gauss_quadrature(double k, Field& C0){

  double w1 = 5.0/9;
  double w2 = 8.0/9;
  double w3 = 5.0/9;
  
  double x1 = -1.0*sqrt(3.0/5);
  double x2 = 0;
  double x3 = sqrt(3.0/5);

  double dx = CO.get_dx();

  for(int i=0; i<C0.get_line(); i++){
    for(int j=0; j<C0.get_col(); j++){
      C0(i,j)=(2*j+1)/2.0*(w1*f_init(k,dx,x1,i,j) + w2*f_init(k,dx,x2,i,j) + w3*f_init(k,dx,x3,i,j));
      std::cout << C0(i,j) << "   "  << (2*j+1)/2.0*(w1*f_init(k,dx,x1,i,j) + w2*f_init(k,dx,x2,i,j) + w3*f_init(k,dx,x3,i,j)) << std::endl;
    }
  }
}

void writedata(char * filename,int i,  double dx, Matrix C, int npoints){
  std::ofstream out(filename ,std::ios::app);
  double pas = dx/(npoints-1);
    for(int l=0; l<npoints; l++){
      double x = (i-0.5)*dx+l*pas;
      double sum = 0;
      for(int j=0; j < C.get_col(); j++){
	sum+=C(i,j)*poly_leg(j,(2/dx)*(x-i*dx));
      }
      out << x << " " << sum  << std::endl;
    }
  out.close();
}

double a = 1;

double getFlux(Field const& u, int cell, double x)
{
  return a*u.value(cell, x);
}

double flux_right(int i, Matrix const& C){
  return getFlux(C, i, 1);
}

double flux_poly(int i, int k, double x, Field const& C){
  double flux = getFlux(C, i, x)
  return flux*ddx_poly_leg(k,x);
}

double flux_integral(int i,int j, Matrix const& C){

  double w1 = 5.0/9;
  double w2 = 8.0/9;
  double w3 = 5.0/9;

  double x1 = -sqrt(3.0/5);
  double x2 = 0;
  double x3 = sqrt(3.0/5);

  double dx = C.get_dx();

  return dx/2*(w1*flux_poly(i,j,x1,C) + w2*flux_poly(i,j,x2,C) + w3*flux_poly(i,j,x3,C));  
}

void periodic_cond(Field& RHS){
  for(int j=0; j<RHS.get_order(); j++){
    RHS(0,j)=RHS(RHS.get_nx()-4,j);
    RHS(1,j)=RHS(RHS.get_nx()-3,j);
    RHS(RHS.get_order()-2,j)=RHS(2,j);
    RHS(RHS.get_order()-1,j)=RHS(3,j);
  }
}
double  right_hand_side(double i, double j, Matrix const& C)
{
  double dx = C.get_dx();
  return ((2*j+1)/dx)*(flux_integral(i,j,C)-flux_right(i,C)*poly_leg(j,1)+flux_right(i-1,C)*poly_leg(j,-1));
}

void rk2(double dt, Field& Cn, Field const& C){

  Field Cmid(C.get_nx(),C.get_order());
  for(int i=2; i<C.get_nx()-2; i++){
    for(int j=0; j<C.get_order(); j++){
      Cmid(i,j)=C(i,j)+right_hand_side(i,j,C)*dt*0.5;
    }
  }
  periodic_cond(Cmid);

  for(int i=2; i<C.get_line()-2; i++){
    for(int j=0; j<C.get_col(); j++){
      Cn(i,j)=C(i,j)+right_hand_side(i,j,a,dx,Cmid)*dt;
    }
  }
  periodic_cond(Cn);
}

// ########################### MAIN ####################################
int main(){

  // GALERKIN DISCONTINU
  
  int nx = 10; //nombre de points
  int P = 2; // ordre du schéma
  double xmin = 0.;
  double xmax = 5.;
  double dx = (xmax-xmin)/(nx-1);
  double L = (nx-4)*dx; // Longueur du domaine -1 incrément
  double  a = 3.; // Vitesse
  double cfl = 0.2; // A prendre entre 0 et 1 pour être stable (CFL)
  double dt = cfl*dx/a;
  int  nT = round((nx-4)*dx/(a*dt)); // nombre d'itérations correspondant à une période de la simulation
  int nt=nT+5; // nbre d'itérations de la simulation.
  double t=0;
  double sigma= a*dt/(1.0*dx);
  int n =1;
  double k = 2*M_PI*n/L; // vecteur d'onde

  //std::cout << "nT = " << nT << std::endl;

  // Création des matrices

  // Matrix C0(nx,P);
  // Matrix C(nx,P);
  // Matrix Cn(nx,P);
  Field C0(nx,P,dx);
  Field C(nx,P,dx);
  Field Cn(nx,P,dx);

  char filename1[7] = {'u','0','.','d','a','t'};
  
  // Remplissage de la matrice C0

  gauss_quadrature(k,C0);

  for(int i=0; i<C0.get_line(); i++){
    for(int j=0; j<C0.get_col(); j++){
      //C0(i,j) = gauss_quadrature(k,i,j,dx,nx,P);
      //C(i,j) = C0(i,j); // initialisation de C
      //std::cout << C0(i,j) << " ";
    }
    std::cout << std::endl << std::endl;
    writedata(filename1,i,dx,C0,10);
  }

  for(int it=0; it<nT ; it++){
    
    rk2(dt,a,dx,Cn,C);
    
    for(int i=0; i<C0.get_line(); i++){
      for(int j=0; j<C0.get_col(); j++){
      C(i,j) = Cn(i,j); 
      }
    }
    
    t+=dt;
  }
  char filename2[7] = {'u','f','.','d','a','t'};

   for(int i=0; i<C0.get_line(); i++){
    writedata(filename2,i,dx,Cn,2);
  }

  
}
  

