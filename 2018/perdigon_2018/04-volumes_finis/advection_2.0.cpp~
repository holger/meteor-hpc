#include <iostream>
#include <math.h>  // Fonctions de base maths
#include <cstdlib> // pour le exit
#include <fstream> // Ouverture/fermeture/lecture/ecriture fichiers



// ###################### CLASSES ####################

template<typename Type>
class Array{
  
public:

  // Constructeurs:

  Array(int length){
    data_ = new Type[length];
    length_ = length;
  }

  Array(Array const& vector){
    length_ = vector.length_;
    data_ = new Type[vector.length_];

    for(int i=0; i<length_; i++){
      data_[i] = vector.data_[i];
    }
  }

  // Destructeurs:

  ~Array(){
    delete [] data_;
  }

  // Opérateurs:

  Type& operator[](int i){ // Pour l'écriture des valeurs
    if (i<0 || i > length_-1){
      std::cerr << "ERROR: i " << i
		<< "length = " << length_ << std::endl;
       exit(1);
    }

    return data_[i];
  }

  Type operator[](int i) const { // Pour la lecture des valeurs
    if (i<0 || i > length_-1){
      std::cerr << "ERROR: i " << i
		<< "length = " << length_ << std::endl;
      exit(1);
    }
    
    return data_[i];
  }

  Array& operator=(Array const& vector){ // Egaliser 2 vecteurs

    if(length_ != vector.length_){
      std::cerr << "ERROR: length_ != vector.length_" << std::endl;
       exit(1);
    }
    for(int i=0; i<length_; i++){
      data_[i] = vector.data_[i];
    }
    return *this;
  }

  
  // Fonction de classe:

  int get_length(){ // retourner la longueur du vecteur
    return length_;
  }

private:

  int length_;
  Type* data_;
};



class Matrix{
public:

  // Constructeurs:
  
  Matrix(int N, int M){
    data_ = new Type[N,M];
    N_ = N;
    M_ = M;
  }

  Matrix(Matrix const& mat){
    N_ = mat.N_;
    M_ = mat.M_;
    data_ = new Type[N_,M_];

    for(int i=0; i<N_; i++){
      for(int j=0; j<M_; j++){
	data_[i,j] = mat.data_[i,j];
      }
    }
  }

  // Destructeurs:

  ~Matrix(){
    delete [] data_;
  }


   // Opérateurs:

  double& operator()(int i, int j){ // Pour l'écriture des valeurs
    if (i<0 || i > N_-1 || j<0 || j>M_-1 ){
      std::cerr << "ERROR: i,j " << i << " ; " << j
		<< "length = " << N_ << " ; " << M_ << std::endl;
       exit(1);
    }

    return data_[i,j];
  }

  double operator()(int i, int j) const { // Pour la lecture des valeurs
    if (i<0 || i > N_-1 || j<0 || j>M_-1 ){
      std::cerr << "ERROR: i,j " << i << " ; " << j
		<< "length = " << N_ << ";" << M_ << std::endl;
       exit(1);
    }

    return data_[i,j];
  }

  Matrix& operator=(Matrix const& mat){ // Egaliser 2 matrices

    if(N_ != mat.N_ || M_ != mat.M_){
      std::cerr << "ERROR: N_ or M_ != mat.N_ or mat.M_" << std::endl;
       exit(1);
    }
    for(int i=0; i<N_; i++){
      for(int j=0; j<M_; j++){
	data_[i,j] = mat.data_[i,j];
      } 
    }
    return *this;
  }

  Matrix operator*(Matrix const& mat){ // Produit matriciel
    
    if(M_ != mat.N_){
      std::cerr << "ERROR: M_ != mat.N_" << std::endl;
       exit(1);
    }
    Matrix<double> R(N_,mat.M_);
  
    
    for(int k=0; k<N_; k++){
      for(int l=0; l<mat.M_; l++){
	R(k,l)=0;
	for(int i=0; i<M_; i++ ){
	  R(k,l)=R(k,l)+data_[k,i]*mat.data_[i,l];
	}
      } 
    }
    return R;
  }

  Array operator*(Array vec){ // Produit matrice vecteur
    
    if(M_ != vec.get_length()){
      std::cerr << "ERROR: M_ != vec.get_length()" << std::endl;
       exit(1);
    }
  
    Array<double> u(N_);
  
    for(int i=0; i<N_; i++){
      u[i]=0;
      for(int k=0; k<M_; k++ ){
	u[i]=u[i]+data_[i,k]*vec[k];
      }
    }
    return u;
  }

  // Fonction de classe:

  int get_nline(){ // retourner la dimension de la matrice
    return N_;
  }
   int get_ncol(){ // retourner la dimension de la matrice
    return M_;
  }

private:
  int N_;
  int M_;
  Type* data_;
  
};

// #######################  FONCTIONS #############################

template<typename Type>
Type sqr(Type value) // Calcul du carré d'un nombre
{
  return value*value;
}

template<typename Type>
double err_norm_L2(Array<Type> u1, Array<Type> u2, double dx){ // Calcul la norme L2 de l'erreur
    
    if(u1.get_length() != u2.get_length()){
      std::cerr << "lengths do not match ! " << std::endl;
      exit(1);
    }

    double sum = 0;
    for( int i=0; i < u1.get_length(); i++){
      sum = sum + sqr(u1[i]-u2[i]);
    }
    
    return sqrt(dx*sum);	 
  }

void Writedata_err(char * filename, int nx, double err){ // Ecriture de l'erreur dans le fichier filename
  
  std::ofstream out(filename ,std::ios::app);
  out << nx << " " << err << std::endl;
  out.close();
}

template<typename Type>
void Writedata(char *  filename, double t, Array<Type> x, Array<Type> y){ // Ecriture de x et u pour un t donné
  
  std::ofstream out(filename ,std::ios::app);
  out  << "#time =" << t << std::endl;
  for(int i=0; i<x.get_length()-1;i++){
    out << x[i] << " " << y[i] << std::endl;
  }
  out << std::endl;
  out << std::endl;
  
  out.close();
}

template<typename Type>
inline double U(Array<Type> u, int i,  double pos, int flag){ // Interpolation de u dans la cellule.

  if(pos < 0.5 || pos > 0.5){
    std:cerr << "Wrong cell domain (-1/2 <= pos <= 1/2)" <<std::endl;
    exit(1);
  }
    
  if(flag == 0){
    return u[i];
  }

  
  if(flag == 1){
    return u[i] + pos/2*(u[i+1]-u[i-1]);
  }

  if{flag == 2){
    
    double Delta_u;
    Type phi = (u[i+1]-u[i])*(u[i]-u[i-1]);
    
    if(phi > 0){
      Delta_u = ((u[i+1]-u[i-1])/sqrt((u[i+1]-u[i-1])*(u[i+1]-u[i-1])))*std::min(2.0*fabs(u[i+1]-u[i]),std::min(2.0*fabs(u[i]-u[i-1]), 0.5*fabs(u[i+1]-u[i-1])));
    }
    
    else{
      Delta_u = 0;
    }
    
    return u[i]+0.5*(1-sigma)*Delta_u;
  }

  else{
    std::cerr << "Incorrect flag value (0 = constant ; 1= linear ; 2=linear+limitors)" << std::endl;
    exit(1);
  }
}

template<typename Type>
void bound_periodic_conditions(Array<Type>& un){
  
  int nx = un.get_length();
  
  un[0]=un[nx-4]; 
  un[1]=un[nx-3];
  un[nx-2]=un[2];
  un[nx-1]=un[3];
}


template<typename Type>
void Euler(Array<Type>& un, Array<Type>& u, double sigma){ 

  int nx = un.get_length();
 
  for(int i=2; i<nx-2; i++){

    un[i]=u[i]+sigma*(U(u, i-1, sigma,2)-U(u,i,sigma,2)); // Limiteur géométrique
    //un[i]=u[i]+sigma*(U(u, i-1, (1-sigma)/2,1)-U(u, i, (1-sigma)/2),1); // Fromm en jdx/2-adt/2
    //un[i]=u[i]+sigma*(U(u, i-1, 1.0/2,1)-U(u, i, 1.0/2,1)); // flux en bord de cellule
  }

  bound_periodic_conditions(un);
}

template<typename Type>
void RK2(Array<Type>& un , Array<Type>& u , double sigma){

  int nx = un.get_length();
  Array<Type> unhalf(nx);

  Euler(unhalf, u, sigma/2); // Première itération n+1/2
  
  for(int i=2; i<nx-2; i++){
   
    un[i]=u[i]+sigma*(U(unhalf, i-1, (1-sigma)/2,1)-U(unhalf, i, (1-sigma)/2,1)); // Fromm en jdx/2-adt/2
    //un[i]=u[i]+sigma*(U(unhalf, i-1, 0.5,1)-U(unhalf, i, 0.5,1)); // flux en bord de cellule
  }

  bound_periodic_conditions(un);
}


// ######################## MAIN ################################################

int main(int argc, char ** argv){ // argc: nombre d'arguments à passer, char ** la liste de strings d'arguments
  
  if(argc<2){ 
    std::cerr << "Erreur: need parameter ( int nx, char * dataname, char * err_name)" << std::endl;
    exit(1);
  }
  
  int nx = atoi(argv[1]); // atoi: transforme un character en un integer
  
  double xmin = 0.;
  double xmax = 5.;
  double dx = (xmax-xmin)/(nx-1);
  double L = (nx-4)*dx; // Longueur du domaine -1 incrément
  double  a = 3.; // Vitesse
  double cfl = 0.2; // A prendre entre 0 et 1 pour être stable (CFL)
  double dt = cfl*dx/a;
  int  nT = round((nx-4)*dx/(a*dt)); // nombre d'itérations correspondant à une période de la simulation
  int nt=nT+5; // nbre d'itérations de la simulation.
  double t=0;
  double sigma= a*dt/(1.0*dx);
  int n =1;
  double k = 2*M_PI*n/L; // vecteur d'onde

  // Création des vecteurs x et u:

  typedef double TYPE;
  Array<TYPE> x(nx);
  Array<TYPE> u(nx);
  Array<TYPE> un(nx); // Pour l'itération n+1

  Array<TYPE> u0(nx); // Pour le calcul de l'erreur
  Array<TYPE> uT(nx); 

  // Remplissage des vecteurs x et u:
  
  for(int i=0;i<nx;i++){

    x[i]=xmin+i*dx;
    
    if(i>=nx/4 && i<=3*nx/4){
      u0[i]=1;
    }
    else{
      u0[i]=0;
    }
    //u0[i]=sin(k*x[i]);
    u[i]=u0[i];
    uT[i]=0;
  }

  Writedata(argv[2],t,x,u0); // Ecriture de la solution initiale pour makemovie

  for(int it=0; it<nt;it++){ // Boucle temporelle

    Euler(un, u, sigma);
    //RK2(un,u,sigma);

    // Mise à jour de la variable u et t pour la prochaine itération temporelle
    u=un; 
    t+=dt;
    
    Writedata(argv[2],t,x,un); // Ecriture des valeurs pour makemovie
  
    if(it+1==nT){ // récupération de la solution après une période
      uT=un;
    } 
  }

  double err = err_norm_L2(u0,uT,dx); // Calcul de l'erreur
  Writedata_err(argv[3],nx,err); // Ecriture de l'erreur dans un fichier
  
}
