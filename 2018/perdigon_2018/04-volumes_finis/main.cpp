// g++ -o main main.cpp -std=c++11

#include <iostream>
#include <functional>

double flux(double u, double a, double dx){
  return u*a/dx;
}

double euler(double u, std::function<double(double)> fluxFunction)
{
  std::cout << fluxFunction(u) << std::endl;
}

int main()
{

  auto fluxFunction = std::bind(flux,std::placeholders::_1, 0.5, 0.25);
  
  euler(1,fluxFunction);
  
}
