#include <fstream>

void Writedata_err(char * filename, int nx, double err)
{
  std::ofstream out(filename ,std::ios::app);
  out << nx << " " << err << std::endl;
  out.close();
}
