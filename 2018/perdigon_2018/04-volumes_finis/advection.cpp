//g++ -o advection advection.cpp write.cpp -std=c++11 -O3

#include <iostream>
#include <math.h>  // Fonctions de base maths
#include <cstdlib> // pour le exit
#include <fstream> // Ouverture/fermeture/lecture/ecriture fichiers
#include <vector>
#include <algorithm>

#include "array.hpp"
#include "write.hpp"

// #######################  FONCTIONS #############################

template<typename Type>
Type sqr(Type value)
{
  return value*value;
}

template<typename Type>
double err_norm_L2(Array<Type> const& u1, Array<Type> const& u2, double dx){ // Calcul la norme L2 de l'erreur
    
    if(u1.get_length() != u2.get_length()){
      std::cerr << "lengths do not match ! " << std::endl;
      exit(1);
    }

    double sum = 0;
    for( int i=0; i < u1.get_length(); i++){
      sum = sum + sqr(u1[i]-u2[i]);
      //      sum=sum+(u1[i]-u2[i])*(u1[i]-u2[i]);
    }
    
    return sqrt(dx*sum);	 
}

template<typename Type>
void Writedata(char *  filename, double t, Array<Type> const& x, Array<Type> const& y){
  
  std::ofstream out(filename ,std::ios::app);
  out  << "#time =" << t << std::endl;
  for(int i=0; i<x.get_length()-1;i++){
    out << x[i] << " " << y[i] << std::endl;
  }
  out << std::endl;
  out << std::endl;
  
  out.close();
}


template <typename T> int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

// template<typename Type>
// inline double U(Array<Type> u, int i,  bool right, int flag, double sigma){ // Interpolation de u dans la cellule.

//   if(pos < 0.5 || pos > 0.5){
//     std::cerr << "Wrong cell domain (-1/2 <= pos <= 1/2)" <<std::endl;
//     exit(1);
//   }

//   if(flag == 0){
//     return u[i];
//   }
  
  
//   if(flag == 1){
//     return u[i] + pos/2*(u[i+1]-u[i-1]);
//   }

//   if(flag == 2){
//     double duj = u[i+1]-u[i-1];
//     return u[i] + 0.5*(1-sigma)*duj;
//   }
  
//   if(flag == 3){
    
//     double duj;
//     if((u[i+1]-u[i])*(u[i]-u[i-1])>0){
//       std::vector<Type> v = {2.0*fabs(u[i+1]-u[i]), 2.0*fabs(u[i]-u[i-1]), 0.5*fabs(u[i+1]-u[i-1])};
//       duj = sgn(u[i+1]-u[i-1])*(*std::min_element(v.begin(),v.end()));
//     }
//     else{
//       duj=0;
//     }
    
//     return u[i]+0.5*(1-sigma)*duj;
//   }

//   else{
//     std::cerr << "Incorrect flag value (0 = constant ; 1= linear ; 2=linear+limitors)" << std::endl;
//     exit(1);
//   }
// }

/*
template<typename Type>
inline double U(Array<Type> u, int i,  double pos){ // Calcul de l'interpolation de u: Constant par morceaux 
  return u[i];
}
*/

// flux for advection equation
template<typename Type>
inline double flux(Array<Type> u, int i, int flag, double a, double dx, double dt){ // Interpolation de u dans la cellule.

  if(flag == 0){
    return a*u[i]/dx;
  }
   if(flag == 1){
     return (u[i] + 0.25*(u[i+1]-u[i-1]))*a/dx;
  }
   if(flag == 2){
     double duj = 0.5*(u[i+1]-u[i-1]);
     return (u[i] + 0.5*(1-a*dt/dx)*duj)*a/dx;
   }
   if(flag == 3){
     double duj;
     if((u[i+1]-u[i])*(u[i]-u[i-1])>0){
       std::vector<Type> v = {2.0*fabs(u[i+1]-u[i]), 2.0*fabs(u[i]-u[i-1]), 0.5*fabs(u[i+1]-u[i-1])};
       duj = sgn(u[i+1]-u[i-1])*(*std::min_element(v.begin(),v.end()));
     }
     else{
       duj=0;
     }
     return (u[i] + 0.5*(1-a*dt/dx)*duj)*a/dx;
   }  
}

template<typename Type>
void Euler(Array<Type>& un, Array<Type> const& u, double dt, double a, double dx){ 

  int nx = un.get_length();

  int flag = 2;
  
  for(int i=2; i<nx-2; i++){
    un[i]=u[i]+dt*(flux(u, i-1, flag, a, dx, dt) - flux(u,i, flag, a, dx, dt)); // Limiteur géométrique
  }

  // Conditions de bords périodiques (2 cellules par bords)
    
  un[0]=un[nx-4]; 
  un[1]=un[nx-3];
  un[nx-2]=un[2];
  un[nx-1]=un[3];
}

// template<typename Type>
// void RK2(Array<Type>& un , Array<Type> const& u , double dt, double a, double dx){

//   int nx = un.get_length();
//   Array<Type> un1demi(nx);

//   // Calcul de u(n+1/2):
  
//   Euler(un1demi, u, sigma/2, dt, a, dx); 
 
//   // Calcul de u(n):

//   for(int i=2; i<nx-2; i++){
   
//     un[i]=u[i]+sigma*(U(u, i-1, (1-sigma)/2)-U(u, i, (1-sigma)/2)); // Fromm en jdx/2-adt/2
//     //un[i]=u[i]+sigma*(U(un1demi, i-1, 1.0/2)-U(un1demi, i, 1.0/2)); // flux en bord de cellule
//   }

//   un[0]=un[nx-4]; 
//   un[1]=un[nx-3];
//   un[nx-2]=un[2];
//   un[nx-1]=un[3];
// }


// ######################## MAIN ################################################

int main(int argc, char ** argv){ // argc: nombre d'arguments à passer, char ** la liste de strings d'arguments

  if(argc<2){ 
    std::cerr << "Erreur: need parameter ( int nx, char * dataname, char * err_name)" << std::endl;
    exit(1);
  }
  
  int nx = atoi(argv[1]); // atoi: transforme un character en un integer
  
  double xmin = 0.;
  double xmax = 5.;
  double dx = (xmax-xmin)/(nx-1);
  double L = (nx-4)*dx; // Longueur du domaine -1 incrément
  double  a = 3.; // Vitesse
  double cfl = 0.2; // A prendre entre 0 et 1 pour être stable (CFL)
  double dt = cfl*dx/a;
  int  nT = round((nx-4)*dx/(a*dt)); // nombre d'itérations correspondant à une période de la simulation
  int nt=nT+5; // nbre d'itérations de la simulation.
  double t=0;
  double sigma= a*dt/(1.0*dx);
  int n =1;
  double k = 2*M_PI*n/L; // vecteur d'onde

  //std::cout << "nT = " << nT << std::endl;

  // Création des vecteurs x et u:

  typedef double TYPE;
  Array<TYPE> x(nx);
  Array<TYPE> u(nx);
  Array<TYPE> un(nx); // Pour l'itération n+1

  Array<TYPE> u0(nx); // Pour le calcul de l'erreur
  Array<TYPE> uT(nx); 

  // Remplissage des vecteurs x et u:
  
  for(int i=0;i<nx;i++){

    x[i]=xmin+i*dx;
    
    if(i>=nx/4 && i<=3*nx/4){
      u0[i]=1;
    }
    else{
      u0[i]=0;
    }
    //u0[i]=sin(k*x[i]);
    u[i]=u0[i];
    uT[i]=0.;
  }

  Writedata(argv[2],t,x,u0); // Ecriture de la solution initiale pour makemovie

  for(int it=0; it<nt;it++){ // Boucle temporelle

    Euler(un, u, dt, a, dx);
    //RK2(un,u,sigma);

    // Mise à jour de la variable u et t pour la prochaine itération temporelle
    u=un; 
    t+=dt;
    
    Writedata(argv[2],t,x,un); // Ecriture des valeurs pour makemovie
  
    if(it+1==nT){ // récupération de la solution après une période
      uT=un;
    } 
  }

  double err = err_norm_L2(u0,uT,dx); // Calcul de l'erreur
  Writedata_err(argv[3],nx,err); // Ecriture de l'erreur dans un fichier
  
}
