// g++ -o main main.cpp

#include <iostream>

class Vector
{
public:

  void setLength(int length) {
    data_ = new double[length];
    length_ = length;
  }
  
  double& value(int i) {
    if(i < 0 || i > length_-1) {
      std::cerr << "ERROR: i = " << i
		<< " length = " << length_
		<< std::endl;
      exit(1);
    }
      
    return data_[i];
  }
    
private:

  double* data_;
  int length_;
  
};

int main()
{
  double value = 5;
  double* valueP = &value;

  std::cout << "value = " << value << std::endl;
  std::cout << "&value = " << &value << std::endl;

  std::cout << "valueP = " << valueP << std::endl;
  std::cout << "&valueP = " << &valueP << std::endl;
  std::cout << "*valueP = " << *valueP << std::endl;

  int length = 12;
  double* valueVec = new double[length];
  for(int i=0; i<length; i++) {
    valueVec[i] = i;
  }
  
  for(int i=0; i<length; i++) {
    std::cout << i << "\t" << valueVec[i] << std::endl;
  }

  Vector vec;
  vec.setLength(length);
  vec.value(0) = 11;
  vec.setLength(2*length);
}
