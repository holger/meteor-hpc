#include <iostream> 
#include<cmath>
#include <fstream>
#include <functional>
#include<boost/multi_array.hpp>

using array_1d = boost::multi_array<double,1>;
using array_2d = boost::multi_array<double,2>;


inline void write2d(std::ofstream& file, double t,array_1d& x, array_1d& y,array_2d& u){
  file << "#time = "<< t <<std::endl;
  int nx =  u.shape()[0];
  int ny =  u.shape()[1];
  for(int ix = 0 ; ix < nx ; ++ix)
    {
      for(int iy =0; iy<ny;++iy)
	{
	  file << x[ix] << " " << y[iy] << " " << u[ix][iy]<<std::endl;
	}
      file << std::endl<<std::endl;
    }
  file << std::endl<<std::endl;
}

void condition_bord(array_2d& u,int N){
  for (int i =1; i<N-1;i++){
    u[i][0]=u[i][N-2];
    u[i][N-1] = u[i][1];
      
  }
  for(int j =1; j<N-1;j++){
      u[0][j] = u[N-2][j];
      u[N-1][j] = u[1][j];
  }
 
}

double Flux_LF(double Fj,double Fj1,double Fj2,double uj,double uj1,double uj2,double u,double a)
  
{
  double F1 = 1./2*(Fj1 + Fj)-1./2*fmax(abs(u-a),abs(u+a))*(uj-uj1);
  double F2 = 1./2*(Fj + Fj2)-1./2*fmax(abs(u-a),abs(u+a))*(uj2-uj);
  return F2-F1;

}




int main()
{
int const Nc = 70;
  double xmax = 2*3.14;
  double xmin =0.;
  double ax =1;

  double ymax =2*3.14;
  double ymin = 0;
  double ay =1;

  double dx = (xmax-xmin)/(Nc-2);
  double dy = (ymax-ymin)/(Nc-2);

  array_1d x((boost::extents[Nc]));
  array_1d y((boost::extents[Nc]));
  
  for (int i =0; i <Nc;i++){
    x[i] = xmin + i*dx;
    y[i] = ymin + i*dy;
  }

  
  array_2d ux((boost::extents[Nc][Nc]));
  array_2d uy((boost::extents[Nc][Nc]));
  
  array_2d Rho0((boost::extents[Nc][Nc]));
  
  array_2d mx0((boost::extents[Nc][Nc]));
  array_2d my0 ((boost::extents[Nc][Nc]));

  for(int i =0; i<Nc;i++){
    for(int j=0; j<Nc;j++){
      ux[i][j] = 1/dx*1/dy*(cos(i*dx+dx)-cos(i*dx))*(cos(j*dy+dy)-cos(j*dy));
    }
  }

    for(int i =0; i<Nc;i++){
    for(int j=0; j<Nc;j++){
      uy[i][j] = 1/dx*1/dy*(cos(i*dx+dx)-cos(i*dx))*(cos(j*dy+dy)-cos(j*dy));
    }
  }

    for(int i =0; i<Nc;i++){
      for(int j=0;j<Nc;j++){
	Rho0[i][j] = 2 + exp(-(i*dx-xmax/2)*(i*dx-xmax/2)/2)*exp(-(j*dx-ymax/2)*(j*dx-ymax/2)/2);
      }
    }
    
  std::ofstream file("density2dt0.txt");
  file << "#title = Gas_iso density2d " << std::endl;
  file << "#xmin = " << -1 << std::endl;
  file << "#xmax = " << xmax+1 << std::endl;
  write2d(file,0.,x,y,Rho0);

  std::ofstream file2("density2dt1.txt");
  file2 << "#title = Gas_iso density2d " << std::endl;
  file2 << "#xmin = " << -1 << std::endl;
  file2 << "#xmax = " << xmax+1 << std::endl;
  // write2d(file,0.,x,y,Rho0);

 



    for(int i =0; i<Nc;i++){
      for(int j=0;j<Nc;j++){
	mx0[i][j] =Rho0[i][j]*ux[i][j];
      }
    }

    /*    
  std::ofstream file("mx2dt0.txt");
  file << "#title = Gas_iso mx2d" << std::endl;
  file << "#xmin = " << -1 << std::endl;
  file << "#xmax = " << xmax+1 << std::endl;
  write2d(file,0.,x,y,mx0);



  std::ofstream file2("mx2dt1.txt");
  file2 << "#title = Gas_iso density2d " << std::endl;
  file2 << "#xmin = " << -1 << std::endl;
  file2 << "#xmax = " << xmax+1 << std::endl;*/
  
  
    for(int i =0; i<Nc;i++){
      for(int j=0;j<Nc;j++){
	my0[i][j] =Rho0[i][j]*uy[i][j];
      }
    }

    /*  
  std::ofstream file3("my2d.txt");
  file3 << "#title = Gas_iso my2d " << std::endl;
  file3 << "#xmin = " << -1 << std::endl;
  file3 << "#xmax = " << xmax+1 << std::endl;
  // write2d(file3,0.,x,y,my0);*/

    
   // Boucle temp :
   double CFL = 0.1;
   double dt = CFL*dx;
   int const Nt = 500;

  array_2d Rho1((boost::extents[Nc][Nc]));
  array_2d mx1((boost::extents[Nc][Nc]));
  array_2d my1 ((boost::extents[Nc][Nc]));
  
  
  for(int it = 1; it < Nt;it++){
    double t = it*dt;

    for(int i= 1; i<Nc-1;i++){
      for(int j =1; j< Nc-1;j++){
	Rho1[i][j] = Rho0[i][j]-dt/dx* Flux_LF( mx0[i][j],mx0[i-1][j],mx0[i+1][j],Rho0[i][j],Rho0[i-1][j],Rho0[i+1][j], ux[i][j],ax) - dt/dy*Flux_LF( my0[i][j],my0[i][j-1],my0[i][j+1],Rho0[i][j],Rho0[i][j+1],Rho0[i][j+1], uy[i][j],ay);	
      }

    }

    
    for(int i= 1; i<Nc-1;i++){
      for(int j =1; j< Nc-1;j++){
	 double Fi = mx0[i][j]*mx0[i][j]/Rho0[i][j] + ay*ay*Rho0[i][j];
	 double Fim = mx0[i-1][j]*mx0[i-1][j]/Rho0[i-1][j] + ax*ax*Rho0[i-1][j];
	 double Fip = mx0[i+1][j]*mx0[i+1][j]/Rho0[i+1][j] + ax*ax*Rho0[i+1][j];

	 double Fj = mx0[i][j]*mx0[i][j]/Rho0[i][j] + ay*ay*Rho0[i][j];
	 double Fjm = mx0[i][j-1]*mx0[i][j-1]/Rho0[i][j-1] + ax*ax*Rho0[i][j-1];
	 double Fjp = mx0[i][j+1]*mx0[i][j+1]/Rho0[i][j+1] + ax*ax*Rho0[i][j+1];
	
	mx1[i][j] = mx0[i][j]-dt/dx* Flux_LF( Fi,Fim,Fip,mx0[i][j],mx0[i-1][j],mx0[i+1][j], uy[i][j],ax) - dt/dy*Flux_LF( Fj,Fjm,Fjp,mx0[i][j],mx0[i][j+1],mx0[i][j+1], uy[i][j],ay);	
      }
    }

      for(int i= 1; i<Nc-1;i++){
      for(int j =1; j< Nc-1;j++){
	 double Fi = mx0[i][j]*my0[i][j]/Rho0[i][j] + ay*ay*Rho0[i][j];
	 double Fim = mx0[i-1][j]*my0[i-1][j]/Rho0[i-1][j] + ay*ay*Rho0[i-1][j];
	 double Fip = mx0[i+1][j]*my0[i+1][j]/Rho0[i+1][j] + ay*ay*Rho0[i+1][j];

	 double Fj = mx0[i][j]*my0[i][j]/Rho0[i][j] + ay*ay*Rho0[i][j];
	 double Fjm = mx0[i][j-1]*my0[i][j-1]/Rho0[i][j-1] + ay*ay*Rho0[i][j-1];
	 double Fjp = mx0[i][j+1]*my0[i][j+1]/Rho0[i][j+1] + ay*ay*Rho0[i][j+1];
	
	my1[i][j] = my0[i][j]-dt/dx* Flux_LF( Fi,Fim,Fip,my0[i][j],my0[i-1][j],my0[i+1][j], uy[i][j],ax) - dt/dy*Flux_LF( Fj,Fjm,Fjp,my0[i][j],my0[i][j+1],my0[i][j+1], uy[i][j],ay);	
      }
      }
 
      condition_bord(mx1, Nc);
      condition_bord(my1, Nc);
      condition_bord(Rho1, Nc);
      
      if(it == 299){
	//write2d(file,t,x,y,Rho1);
	write2d(file2,t,x,y,Rho1);
	//write2d(file3,t,x,y,my1);
      }


      mx0 = mx1;
      my0 = my1;
      Rho0= Rho1;

    

  }


      
	

	


    

    

  

  
 


  

  
  










  



}
