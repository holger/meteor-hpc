#include <iostream> 
#include<cmath>
#include <fstream>
#include <functional>
#include<boost/multi_array.hpp>



using array_1d = boost::multi_array<double,1>;
using array_2d = boost::multi_array<double,2>;


void condition_bord(array_2d& u,int N){
  for (int i =1; i<N-1;i++){
    u[i][0]=u[i][N-2];
    u[i][N-1] = u[i][1];
      
  }
  
  for(int j =1; j<N-1;j++){
      u[0][j] = u[N-2][j];
      u[N-1][j] = u[1][j];
  }
 
}

inline void write2d(std::ofstream& file, double t,array_1d& x, array_1d& y,array_2d& u){
  file << "#time = "<< t <<std::endl;
  int nx =  u.shape()[0];
  int ny =  u.shape()[1];
  for(int ix = 0 ; ix < nx ; ++ix)
    {
      for(int iy =0; iy<ny;++iy)
	{
	  file << x[ix] << " " << y[iy] << " " << u[ix][iy]<<std::endl;
	}
      file << std::endl<<std::endl;
    }
  file << std::endl<<std::endl;
}

int main()

{
  int const Nc = 100;
  double xmax = 2*3.14;
  double xmin =0.;
  double ax =0;

  double ymax =2*3.14;
  double ymin = 0;
  double ay =1;

  double dx = (xmax-xmin)/(Nc-2);
  double dy = (ymax-ymin)/(Nc-2);

  array_1d x((boost::extents[Nc]));
  array_1d y((boost::extents[Nc]));
  for (int i =0; i <Nc;i++){
    x[i] = xmin + i*dx;
    y[i] = ymin + i*dy;
  }

  array_2d Ut0((boost::extents[Nc][Nc]));
  
  for(int i =0; i<Nc;i++){
    for(int j=0; j<Nc;j++){
      Ut0[i][j] = 1./dx*1./dy*(cos(i*dx+dx)-cos(i*dx))*(cos(j*dy+dy)-cos(j*dy));
    }
  }
  
  std::ofstream file("Upwind_2d.txt");
  file << "#title = 2D Advection" << std::endl;
  file << "#dim = 2"<<std::endl;
  file << "#xmin = " << xmin << std::endl;
  file << "#xmax = " << xmax << std::endl;
  file << "#ymin = " << ymin << std::endl;
  file << "#ymax = " << ymax << std::endl;
  file << "#zmin = " << -1.5 << std::endl;
  file << "#zmax = " << +1.5 << std::endl;
  //write2d(file,0.,x,y,Ut0);

  double dt = 0.1*dx/ay;
  std::cout << "dt = " << dt << " lx = " << dx*Nc << " dx = " << dx << " lambda = " << xmax << " T = " << xmax/ay << " Nt = " << xmax/ay/dt << std::endl;
  int const Nt = 0.5*xmax/ay/dt;
  
  array_2d Ut1((boost::extents[Nc][Nc]));
  
  for (int it=1; it<Nt+1;it++){
    double t = it*dt;
    
    for(int i =1; i< Nc-1;i++){
      for(int j =1; j<Nc-1;j++){
	Ut1[i][j] = Ut0[i][j] - dt*(ax*(Ut0[i][j]-Ut0[i-1][j])/dx + ay*(Ut0[i][j] - Ut0[i][j-1])/dy);
      }
    }
    condition_bord(Ut1,Nc);
    
    Ut0 = Ut1;
  }
  write2d(file,Nt,x,y,Ut1);
}

