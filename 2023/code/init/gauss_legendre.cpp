// g++ -o gauss_legendre gauss_legendre.cpp -D_GLIBCXX_DEBUG

#include <iostream>
#include <vector>
#include <functional>
#include <cmath>

double f1(double x)
{
  return x;
}

double f2(double x)
{
  return x*x;
}

double f3(double x)
{
  return x*x*x;
}

double f4(double x)
{
  return x*x*x*x;
}

double f2d(double x, double y) {
  return x*y*sin(x)*sin(y);
}

std::vector<std::vector<double>> dat_order1={{0},
					 {2}};
std::vector<std::vector<double>> dat_order2={{-0.5773502691896257645091488,0.5773502691896257645091488},
					 {1,1}};
std::vector<std::vector<double>> dat_order3={{-0.7745966692414833770358531,0,0.7745966692414833770358531},
					 {0.555555555555555555555555,0.8888888888888888888888889,0.555555555555555555555555}};
std::vector<std::vector<double>> dat_order4={{-0.8611363115940526,-0.3399810435848563,0.3399810435848563,0.8611363115940526},
					     {0.3478548451374538,0.6521451548625461,0.6521451548625461,0.3478548451374538}};
std::vector<std::vector<double>> dat_order5={{-0.9061798459386640,-0.5384693101056831,0.0000000000000000,0.5384693101056831,0.9061798459386640},
					     {0.2369268850561891,0.4786286704993665,0.5688888888888889,0.4786286704993665,0.2369268850561891}};
std::vector<std::vector<double>> dat_order6={{-0.9324695142031521,-0.6612093864662645,-0.2386191860831969,0.2386191860831969,0.6612093864662645,0.9324695142031521},
					     {0.1713244923791704,0.3607615730481386,0.4679139345726910,0.4679139345726910,0.3607615730481386,0.1713244923791704}};

std::vector<std::vector<std::vector<double>>> dat = {dat_order1, dat_order2, dat_order3, dat_order4, dat_order5, dat_order6};

double gaussLegendre(int order, std::function<double(double)> f, double x0 = -1, double x1 = 1)
{
  double result = 0;
  int number_nodes = dat[order][0].size();
  for(int i=0; i<number_nodes; i++) {
    double xi = dat[order][0][i];
    double wi = dat[order][1][i];
    double xit = (x1-x0)/2*xi-(x1-x0)/2+x1;
    result += f(xit)*wi;
  }

  return result*(x1-x0)/2;
}

double gaussLegendre2d(int order, std::function<double(double,double)> f, double x0 = -1, double x1 = 1, double y0 = -1, double y1 = 1)
{
  double result = 0;
  int number_nodes = dat[order][0].size();
  for(int i=0; i<number_nodes; i++) {
    for(int j=0; j<number_nodes; j++) {
      double xi = dat[order][0][i];
      double wi = dat[order][1][i];
      double yj = dat[order][0][j];
      double wj = dat[order][1][j];
      double ax = (x1-x0)/2;
      double bx = (x1+x0)/2;
      double xit = ax*xi + bx;
      double ay = (y1-y0)/2;
      double by = (y1+y0)/2;
      double yjt = ay*yj + by;
	
      result += f(xit, yjt)*wi*wj;
    }
  }

  result *= (x1-x0)/2*(y1-y0)/2;
  
  return result;
}

int main()
{
  std::cout << "Integration with Gauss-Legendre quadrature starts\n";

  std::cout << dat[0][1][0] << std::endl;

  std::cout << gaussLegendre(1, &f2) << std::endl;
  std::cout << gaussLegendre(2, &f3) << std::endl;
  std::cout << gaussLegendre(2, &f3, 0, 0.1) << std::endl;

  std::cout << gaussLegendre2d(5, &f2d, -2, 1, -2, 4) << std::endl;
}
