#include <assert.h>
#include <cmath>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <array>
#include <algorithm>

#include "derive.hpp"

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

double sqr(double a)
{
  return a*a;
}

void computeError(const std::vector<double>& ref, const std::vector<double> vec, const std::string& fileName, double dx) {
  std::ofstream out(fileName.c_str(), std::ios::app);

  double error = 0;
  for(int i=0;i<ref.size(); i++)
    error += sqr(ref[i] - vec[i]);
  
  error *= dx;
  error = sqrt(error);

  out << ref.size() << "\t" << error << std::endl;
}

double minmod(double u1, double u2)
{
  double u = 0;
  if (u1*u2 > 0) 
    if(fabs(u1) < fabs(u2)) u = u1;
    else 
      if (fabs(u2) <= fabs(u1)) u = u2;
  
  return u;
}

double minmod3(double u1, double u2, double u3)
{
  double u = 0;
  
  if (u1*u2 > 0) {
    std::array<double,3> vec;
    vec[0] = u1;
    vec[1] = u2;
    vec[2] = u3;
    std::array<double,3> vecMod;
    vecMod[0] = fabs(u1);
    vecMod[1] = fabs(u2);
    vecMod[2] = fabs(u3);
    
    auto minVal = std::min_element(vecMod.begin(),vecMod.end());
    int index = minVal - vecMod.begin();
    
    u = vec[index];
  }
  
  return u;
}

void periodic(std::vector<double>& vec)
{
  vec[0] = vec[vec.size()-4];
  vec[1] = vec[vec.size()-3];
  vec[vec.size()-2] = vec[2];
  vec[vec.size()-1] = vec[3];
}

void periodic3(std::vector<double>& vec)
{
  vec[0] = vec[vec.size()-6];
  vec[1] = vec[vec.size()-5];
  vec[2] = vec[vec.size()-4];
  vec[vec.size()-3] = vec[3];
  vec[vec.size()-2] = vec[4];
  vec[vec.size()-1] = vec[5];
}

void kurganov_tadmor(const std::vector<double>& vec, std::vector<double>& rhs, double dx, double v)
{
  std::vector<double> uxnj(vec.size());
  
  for(int i=1;i<vec.size()-1;i++) {
    //    uxnj[i] = minmod((vec[i] - vec[i-1])/dx,(vec[i+1] - vec[i])/dx);
    uxnj[i] = minmod3((vec[i] - vec[i-1])/dx,(vec[i+1] - vec[i])/dx,(vec[i+1] - vec[i-1])/(2*dx));
  }
  
  for(int i=1;i<vec.size()-1;i++) {
    double upjp5 = vec[i+1] - 0.5*dx*uxnj[i+1];
    double umjp5 = vec[i] + 0.5*dx*uxnj[i];
    double upjm5 = vec[i] - 0.5*dx*uxnj[i];
    double umjm5 = vec[i-1] + 0.5*dx*uxnj[i-1];
    
    double ajp5 = fabs(v);
    double ajm5 = fabs(v);

    rhs[i] = -0.5/dx*((v*upjp5 + v*umjp5) - (v*upjm5 + v*umjm5)) + 0.5/dx*(ajp5*(upjp5 - umjp5) - ajm5*(upjm5 - umjm5));    
  }
}

int main(int argc, char* argv[])
{
  system("mkdir fields");

  cerr << "let us solve the advection equation ... \n";
  if(argc < 3) {
    cerr << "parameters : nx cfl\n";
    exit(1);
  }

  int nx = atoi(argv[1]);
  double cfl = atof(argv[2]);
  double v = 1;
  double lx = 1;
  double dx = lx/nx;
  double dt = cfl*dx/fabs(v);
  double lambda = lx-4*dx;
  int nSteps = round(lambda/v/dt);
  
  std::vector<double> u(nx);
  std::vector<double> u3(nx);
  std::vector<double> u_upwind(nx);
  std::vector<double> u_downwind(nx);
  std::vector<double> u_center(nx);
  std::vector<double> u_center_temp(nx);
  std::vector<double> u_lf(nx);
  std::vector<double> u_lf0(nx);
  std::vector<double> u_lw(nx);
  std::vector<double> u_lw0(nx);
  std::vector<double> u_fromm(nx);
  std::vector<double> u_fromm_rk2(nx);
  std::vector<double> u_fromm_rk2_temp(nx);
  std::vector<double> u_ppm(nx);
  std::vector<double> u_ppm_1(nx);
  std::vector<double> u_ppm_2(nx);
  std::vector<double> u_ppm_3(nx);
  std::vector<double> fi(nx);
  std::vector<double> fo(nx);
  std::vector<double> u_lin(nx);
  std::vector<double> u_lin_1(nx);
  std::vector<double> u_lin_2(nx);
  std::vector<double> u_lin_3(nx);
  std::vector<double> u_lin_temp(nx);
  std::vector<double> u_leer(nx);
  std::vector<double> si_leer(nx);
  std::vector<double> so_leer(nx);
  std::vector<double> u_kt(nx);
  std::vector<double> u_ktsd(nx);
  std::vector<double> u_ktsdrk2(nx);
  std::vector<double> u_ktsdrk3(nx);

  std::vector<double> ux(nx);
  std::vector<double> ux_upwind(nx);
  std::vector<double> ux_downwind(nx);
  std::vector<double> ux_center(nx);

  cerr << "allocated " << nx << " element vectors \n";

  for(int i=0;i<nx;i++)
    u[i]=sin(2*M_PI/lambda*i*dx);
  
  double lambda3 = lx-6*dx;

  for(int i=0;i<nx;i++)
    u3[i]=sin(2*M_PI/lambda3*i*dx);

  int nSteps3 = round(lambda3/v/dt);

  std::cout << "nSteps = " << nSteps << std::endl;
  std::cout << "nSteps3 = " << nSteps3 << "\t" << round(lambda3/v/dt) << std::endl;

  // for(int i=0;i<nx;i++)
  //   if(i*dx > 0.4 && i*dx < 0.6)
  //     u[i] = 2;
  //   else
  //     u[i] = 1;
    
  // for(int i=0;i<nx;i++)
  //   if(i*dx > 0.25 && i*dx < 0.25+0.5)
  //     u[i] = sin(2*M_PI/1*((i*dx-0.25)));
  //   else
  //     u[0] = 0;

  // for(int i=nx/2;i<nx;i++)
  //   u[i] = 0;
  // for(int i=0;i<nx/2;i++)
  //   u[i] = 1;

  // for(int i=0;i<nx;i++)
  //   u[i] = 0;
  // for(int i=150;i<200;i++)
  //   u[i] = 1;

  u_upwind = u;
  u_downwind = u;
  u_center = u;
  u_lw = u;
  u_lf = u;
  u_fromm = u;
  u_fromm_rk2 = u;
  u_lin = u;
  u_ppm = u3;
  u_leer = u;
  u_kt = u;
  u_ktsd = u;
  u_ktsdrk2 = u;
  u_ktsdrk3 = u;

  periodic3(u_ppm);
  
  for(int i=1;i<nx-1;i++)
    ux[i]=2*M_PI/lx*cos(2*M_PI/lx*(i-1)*dx);
  
  downwind(ux_downwind,u,dx);
  center(ux_center,u,dx);

  periodic(ux_upwind);
  periodic(ux_downwind);
  periodic(ux_center);
  
  {
    ofstream out("u_init.txt");
    for(int i=0;i<nx;i++)
      out << i*dx << "\t" << u[i] 
	  << "\t" << ux[i] 
	  << "\t" << ux_upwind[i] 
	  << "\t" << ux_downwind[i] 
	  << "\t" << ux_center[i] <<  std::endl;
  }

  std::stringstream filenameStream;
  filenameStream << "fields/description.txt";
  std::ofstream out(filenameStream.str().c_str());
  
  out << "1\t x\n"
      << "2\t upwind\n"
      << "3\t downwind\n"
      << "4\t center\n"
      << "5\t lax-friedrich\n"
      << "6\t lax-wendroff\n"
      << "7\t fromm\n"
      << "8\t fromm_rk2\n"
      << "9\t linear reconstruction + RK2\n"
      << "10\t parabolic reconstruction + RK2\n"
      << "11\t van leer\n"
      << "12t in-slope van leer\n"
      << "13\t out-slope van leer\n"
      << "14\t kurganov-tadmor fully discrete\n"
      << "15\t kurganov-tadmor semi discrete euler\n"
      << "16\t kurganov-tadmor semi discrete rk2\n"
      << "17\t kurganov-tadmor semi discrete rk3\n";
  out.close();

  for(int step=0;step<=nSteps;step++) {
    std::cout << "step = " << step << std::endl;
    
    if(step == nSteps) {
      computeError(u, u_upwind,"fields/error_up.txt", dx);
      computeError(u, u_center,"fields/error_center.txt", dx);
      computeError(u, u_lf,"fields/error_lf.txt", dx);
      computeError(u, u_lw,"fields/error_lw.txt", dx);
      computeError(u, u_fromm,"fields/error_fromm.txt", dx);
      computeError(u, u_fromm_rk2,"fields/error_fromm_rk2.txt", dx);
      computeError(u, u_lin,"fields/error_lin.txt", dx);
      computeError(u, u_leer,"fields/error_leer.txt", dx);
      computeError(u, u_kt,"fields/error_kt.txt", dx);
      computeError(u, u_ktsd,"fields/error_ktsd.txt", dx);
      computeError(u, u_ktsdrk2,"fields/error_ktsdrk2.txt", dx);
      computeError(u, u_ktsdrk3,"fields/error_ktsdrk3.txt", dx);
    }
    
    if(step == nSteps3) {
      computeError(u3, u_ppm,"fields/error_ppm.txt", dx);
    }
    
    if(step % 10 == 0 || step == nSteps) {
      
      filenameStream.str("");
      filenameStream.clear();

      if(step == nSteps)
	filenameStream << "fields/u-final_" << nx << ".txt";
      else
	filenameStream << "fields/u_" << nx << "-" << step << ".txt";

      std::cout << "writing to file " << filenameStream.str() << "\n";
      
      std::ofstream out(filenameStream.str().c_str());
      for(int i=0;i<nx;i++)
	out << i*dx << "\t" << u_upwind[i] 
	    << "\t" << u_downwind[i] 
	    << "\t" << u_center[i]
	    << "\t" << u_lf[i] 
	    << "\t" << u_lw[i] 
	    << "\t" << u_fromm[i]
	    << "\t" << u_fromm_rk2[i]
	    << "\t" << u_lin[i]
	    << "\t" << u_ppm[i]
	    << "\t" << u_leer[i] 
	    << "\t" << si_leer[i] 
	    << "\t" << so_leer[i] 
	    << "\t" << u_kt[i] 
	    << "\t" << u_ktsd[i] 
	    << "\t" << u_ktsdrk2[i]
	    << "\t" << u_ktsdrk3[i] << std::endl;
    }
    
    for(int i=1;i<nx-1;i++) {
      if(v > 0)
	ux_upwind[i] = (u_upwind[i]-u_upwind[i-1])/dx;
      else
	ux_upwind[i] = (u_upwind[i+1]-u_upwind[i])/dx;
    }

    for(int i=1;i<nx-1;i++)
      u_upwind[i] -= v*dt*ux_upwind[i];
    
    periodic(u_upwind);
    upwind(ux_upwind,u_upwind,dx);

    for(int i=1;i<nx-1;i++) 
      u_downwind[i] -= v*dt*ux_downwind[i];
    periodic(u_downwind);
    downwind(ux_downwind,u_downwind,dx);
    
    for(int i=1;i<nx-1;i++) 
      u_center_temp[i] = u_center[i] - v*dt/2*ux_center[i];

    periodic(u_center_temp);

    center(ux_center,u_center_temp,dx);

    for(int i=1;i<nx-1;i++) 
      u_center[i] -= v*dt*ux_center[i];

    periodic(u_center);
    center(ux_center,u_center,dx);

    // lax-friedrich ----------------------------------------------------------------------------------------

    u_lf0 = u_lf;
    for(int i=1;i<nx-1;i++) 
      u_lf[i] = 0.5*(u_lf0[i+1]+u_lf0[i-1]) - 0.5*v*dt/dx*(u_lf0[i+1]-u_lf0[i-1]);
    periodic(u_lf);

    // lax-wendroff -----------------------------------------------------------------------------------------

    u_lw0 = u_lw;
    for(int i=1;i<nx-1;i++) 
      u_lw[i] = (1-sqr(v*dt/dx))*u_lw0[i] - 0.5*v*dt/dx*((1-v*dt/dx)*u_lw0[i+1]-(1+v*dt/dx)*u_lw0[i-1]);
    periodic(u_lw);
    
    // fromm -------------------------------------------------------------------------------------------------
    
    if(v > 0) {
      for(int i=2;i<nx;i++)
	fi[i] = v*(u_fromm[i-1] + 0.5*(1-cfl)*(u_fromm[i] - u_fromm[i-2])/2);
      for(int i=1;i<nx-1;i++)
	fo[i] = v*(u_fromm[i] + 0.5*(1-cfl)*(u_fromm[i+1] - u_fromm[i-1])/2);
    }
    else {
      for(int i=0;i<nx-2;i++)
	fi[i] = -v*(u_fromm[i+1] - 0.5*(1-cfl)*(u_fromm[i+2] - u_fromm[i])/2);
      for(int i=1;i<nx-1;i++)
	fo[i] = -v*(u_fromm[i] - 0.5*(1-cfl)*(u_fromm[i+1] - u_fromm[i-1])/2);
    }

    for(int i=2;i<nx-2;i++)
      u_fromm[i] += dt/dx*(fi[i] - fo[i]);

    periodic(u_fromm);

    // fromm rk2 -------------------------------------------------------------------------------------------------
    {
      auto fluxFunc = [=](std::vector<double>& fi, std::vector<double>& fo, std::vector<double> const& u) {
	if(v > 0) {
	  for(int i=2;i<nx;i++)
	    fi[i] = v*(u[i-1] + 0.5*(1-cfl)*(u[i] - u[i-2])/2);
	  for(int i=1;i<nx-1;i++)
	    fo[i] = v*(u[i] + 0.5*(1-cfl)*(u[i+1] - u[i-1])/2);
	}
	else {
	  for(int i=0;i<nx-2;i++)
	    fi[i] = -v*(u[i+1] - 0.5*(1-cfl)*(u[i+2] - u[i])/2);
	  for(int i=1;i<nx-1;i++)
	    fo[i] = -v*(u[i] - 0.5*(1-cfl)*(u[i+1] - u[i-1])/2);
	}
      };	

      fluxFunc(fi, fo, u_fromm_rk2);
      
      for(int i=2;i<nx-2;i++)
	u_fromm_rk2_temp[i] = u_fromm_rk2[i] + dt/dx*(fi[i] - fo[i])/2;
      
      periodic(u_fromm_rk2_temp);
      
      fluxFunc(fi, fo, u_fromm_rk2_temp);
      
      for(int i=2;i<nx-2;i++)
	u_fromm_rk2[i] += dt/dx*(fi[i] - fo[i]);    
      
      periodic(u_fromm_rk2);

    }
    
    // linear reconstruction + RK2 -------------------------------------------------------------------------------------------------
    {
      // fonction lambda
      auto fluxFunc = [=](std::vector<double>& fi, std::vector<double>& fo, std::vector<double> const& u_lin) {
	if(v > 0) {
	  for(int i=2;i<nx;i++)
	    fi[i] = v*(u_lin[i-1] + 0.5*(u_lin[i] - u_lin[i-2])/2);
	  for(int i=1;i<nx-1;i++)
	    fo[i] = v*(u_lin[i] + 0.5*(u_lin[i+1] - u_lin[i-1])/2);
	}
	else {
	  for(int i=0;i<nx-2;i++)
	    fi[i] = -v*(u_lin[i+1] - 0.5*(u_lin[i+2] - u_lin[i])/2);
	  for(int i=1;i<nx-1;i++)
	    fo[i] = -v*(u_lin[i] - 0.5*(u_lin[i+1] - u_lin[i-1])/2);
	}
      };

      // fluxFunc(fi, fo, u_lin);

      // for(int i=2;i<nx-2;i++)
      // 	u_lin_1[i] = u_lin[i] + dt/dx*(fi[i] - fo[i]);
      
      // periodic(u_lin_1);

      // fluxFunc(fi, fo, u_lin_1);

      // for(int i=2;i<nx-2;i++)
      // 	u_lin_2[i] = 0.75*u_lin[i] + 0.25*u_lin_1[i] + 0.25*dt/dx*(fi[i] - fo[i]);
      
      // periodic(u_lin_2);
      
      // fluxFunc(fi, fo, u_lin_2);
      
      // for(int i=2;i<nx-2;i++)
      // 	u_lin_3[i] = u_lin[i]/3. + 2*u_lin_2[i]/3. + 2*dt/dx*(fi[i] - fo[i])/3.;    
      
      // periodic(u_lin_3);

      // u_lin = u_lin_3;

      fluxFunc(fi, fo, u_lin);
      
      for(int i=2;i<nx-2;i++)
	u_lin_temp[i] = u_lin[i] + dt/dx*(fi[i] - fo[i])/2;
      
      periodic(u_lin_temp);
      
      fluxFunc(fi, fo, u_lin_temp);
      
      for(int i=2;i<nx-2;i++)
	u_lin[i] += dt/dx*(fi[i] - fo[i]);    
      
      periodic(u_lin);
    }
    
    // piece-wise parabolic (ppm) + RK3 -------------------------------------------------------------------------------------------------
    {
      auto fluxFunc = [=](std::vector<double>& fi, std::vector<double>& fo, std::vector<double> const& u) {
	if(v > 0) {
	    
	  for(int i=3;i<nx-1;i++) {
	    fi[i] = v*(5./6*u[i-1] + u[i]/3 - u[i-2]/6);
	  }
	   
	  for(int i=2;i<nx-2;i++) {
	    fo[i] = v*(5./6*u[i] + u[i+1]/3 - u[i-1]/6);
	  }
	}
	else {
	  // for(int i=0;i<nx-2;i++)
	  //   fi[i] = -v*(u_ppm[i+1] - 0.5*(u_ppm[i+2] - u_ppm[i])/2);
	  // for(int i=1;i<nx-1;i++)
	  //   fo[i] = -v*(u_ppm[i] - 0.5*(u_ppm[i+1] - u_ppm[i-1])/2);
	}
      };

      fluxFunc(fi, fo, u_ppm);

      for(int i=3;i<nx-3;i++)
	u_ppm_1[i] = u_ppm[i] + dt/dx*(fi[i] - fo[i]);
      
      periodic3(u_ppm_1);

      fluxFunc(fi, fo, u_ppm_1);

      for(int i=3;i<nx-3;i++)
	u_ppm_2[i] = 0.75*u_ppm[i] + 0.25*u_ppm_1[i] + 0.25*dt/dx*(fi[i] - fo[i]);
      
      periodic3(u_ppm_2);
      
      fluxFunc(fi, fo, u_ppm_2);
      
      for(int i=3;i<nx-3;i++)
	u_ppm_3[i] = u_ppm[i]/3. + 2*u_ppm_2[i]/3. + 2*dt/dx*(fi[i] - fo[i])/3.;    
      
      periodic3(u_ppm_3);

      u_ppm = u_ppm_3;
    }

    // van-leer ----------------------------------------------------------------------------------------------

    if(v > 0) {
      for(int i=2;i<nx;i++) {
	double phi = sgn((u_leer[i]-u_leer[i-1])*(u_leer[i-1]-u_leer[i-2]));
	double slope = 0;
	si_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i]-u_leer[i-1]);
	  si_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i-1]-u_leer[i-2]);
	  double slope2 = 0.5*fabs(u_leer[i]-u_leer[i-2]);
	  if(slope1 < slope) { slope = slope1; si_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; si_leer[i] = 3; }
	  slope *= sgn(u_leer[i]-u_leer[i-2]);
	}
	fi[i] = v*(u_leer[i-1] + 0.5*(1-cfl)*slope);
      }

      for(int i=1;i<nx-1;i++) {
	double phi = sgn((u_leer[i+1]-u_leer[i])*(u_leer[i]-u_leer[i-1]));
	double slope = 0;
	so_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i+1]-u_leer[i]);
	  so_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i]-u_leer[i-1]);
	  double slope2 = 0.5*fabs(u_leer[i+1]-u_leer[i-1]);
	  if(slope1 < slope) { slope = slope1; so_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; so_leer[i] = 3; }
	  slope *= sgn(u_leer[i+1]-u_leer[i-1]);
	}
	fo[i] = v*(u_leer[i] + 0.5*(1-cfl)*slope);
      }
    }
    else {
      for(int i=2;i<nx;i++) {
	double phi = sgn((u_leer[i+2]-u_leer[i+1])*(u_leer[i+1]-u_leer[i]));
	double slope = 0;
	si_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i+1]-u_leer[i]);
	  si_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i+2]-u_leer[i+1]);
	  double slope2 = 0.5*fabs(u_leer[i+2]-u_leer[i]);
	  if(slope1 < slope) { slope = slope1; si_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; si_leer[i] = 3; }
	  slope *= sgn(u_leer[i+2]-u_leer[i]);
	}
	fi[i] = -v*(u_leer[i+1] - 0.5*(1-cfl)*slope);
      }
      
      for(int i=1;i<nx-1;i++) {
	double phi = sgn((u_leer[i+1]-u_leer[i])*(u_leer[i]-u_leer[i-1]));
	double slope = 0;
	so_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i+1]-u_leer[i]);
	  so_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i]-u_leer[i-1]);
	  double slope2 = 0.5*fabs(u_leer[i+1]-u_leer[i-1]);
	  if(slope1 < slope) { slope = slope1; so_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; so_leer[i] = 3; }
	  slope *= sgn(u_leer[i+1]-u_leer[i-1]);
	}
	fo[i] = -v*(u_leer[i] - 0.5*(1-cfl)*slope);
      }
    }

    for(int i=2;i<nx-1;i++)
      u_leer[i] += dt/dx*(fi[i] - fo[i]);

    periodic(u_leer);

    // Kurganov-Tadmor - fully discrete
    
    std::vector<double> uxnj(nx);
    std::vector<double> onp1jp5(nx);
    std::vector<double> onp1j(nx);
    std::vector<double> uxnp1jp5(nx);
    
    double lambda = dt/dx;
    double anjp5 = fabs(v);
    double anjm5 = fabs(v);
    double anjp3 = fabs(v);
    
    for(int i=1;i<nx-1;i++) 
      uxnj[i] = minmod((u_kt[i] - u_kt[i-1])/dx,(u_kt[i+1] - u_kt[i])/dx);
    
    for(int i=1;i<nx-1;i++) {
      double unjp5r = u_kt[i+1] - dx*uxnj[i+1]*(1-lambda*anjp5);
      double unjp5l = u_kt[i] + dx*uxnj[i+1]*(1-lambda*anjp5);
      double unjm5r = u_kt[i] - dx*uxnj[i]*(1-lambda*anjm5);
      
      double unp5jp5r = unjp5r - 0.5*dt*v*uxnj[i+1];
      double unp5jp5l = unjp5l - 0.5*dt*v*uxnj[i];
      double unp5jm5r = unjm5r - 0.5*dt*v*uxnj[i];
      
      onp1jp5[i] = 0.5*(u_kt[i] + u_kt[i+1]) + 0.25*(dx - anjp5*dt)*(uxnj[i] - uxnj[i+1]) - 0.5/anjp5*(v*unp5jp5r - v*unp5jp5l);
      onp1j[i] = u_kt[i] + 0.5*dt*(anjm5-anjp5)*uxnj[i] - lambda/(1-lambda*(anjm5 + anjp5))*(v*unp5jp5l - v*unp5jm5r);
    }
    
    for(int i=1;i<nx-1;i++) {
      uxnp1jp5[i] = 2./dx*minmod((onp1j[i+1] - onp1jp5[i])/(1+lambda*(anjp5 - anjp3)), (onp1jp5[i] - onp1j[i])/(1+lambda*(anjp5 - anjm5)));
    }
    
    for(int i=1;i<nx-1;i++) {
      u_kt[i] = lambda*anjm5*onp1jp5[i-1] + lambda*anjp5*onp1jp5[i] + (1 - lambda*(anjm5+anjp5))*onp1j[i] + 0.5*dx*(sqr(lambda*anjm5)*uxnp1jp5[i-1] - sqr(lambda*anjp5)*uxnp1jp5[i]);
    }
    
    periodic(u_kt);

    // Kurganov-Tadmor - semi discrete

    std::vector<double> u_rhs(u_ktsd);
    
    kurganov_tadmor(u_ktsd, u_rhs, dx, v);

    for(int i=1;i<nx-1;i++) {
      u_ktsd[i] += dt*u_rhs[i];
    }

    periodic(u_ktsd);

    // Kurganov-Tadmor - semi discrete - rk2

    std::vector<double> u_ktsd0(u_ktsdrk2);
    
    kurganov_tadmor(u_ktsdrk2, u_rhs, dx, v);

    for(int i=1;i<nx-1;i++) {
      u_ktsd0[i] += 0.5*dt*u_rhs[i];
    }

    periodic(u_ktsd0);
    
    kurganov_tadmor(u_ktsd0, u_rhs, dx, v);

    for(int i=1;i<nx-1;i++) {
      u_ktsdrk2[i] += dt*u_rhs[i];
    }
    
    periodic(u_ktsdrk2);

    // Kurganov-Tadmor - semi discrete - rk3
    
    kurganov_tadmor(u_ktsdrk3, u_rhs, dx, v);
    
    for(int i=1;i<nx-1;i++) {
      u_ktsd0[i] = u_ktsdrk3[i] + dt*u_rhs[i];
    }
    periodic(u_ktsd0);
    
    std::vector<double> u_ktsd1(u_ktsdrk2.size());  
    kurganov_tadmor(u_ktsd0, u_rhs, dx, v);
    for(int i=1;i<nx-1;i++) {
      u_ktsd1[i] = 0.75*u_ktsdrk3[i] + 0.25*u_ktsd0[i] + 0.25*dt*u_rhs[i];
    }
    periodic(u_ktsd1);
    
    kurganov_tadmor(u_ktsd1, u_rhs, dx, v);
    for(int i=1;i<nx-1;i++) {
      u_ktsdrk3[i] = u_ktsdrk3[i]/3. + 2*u_ktsd1[i]/3. + 2*dt*u_rhs[i]/3.;
    }
    periodic(u_ktsdrk3);
  }
}
