#ifndef matrix_h
#define matrix_h

#include <assert.h>
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

class Matrix
{
  // toutes les variable public sont accessible de l'exterieur
public:

  // constructeur par defaut
  Matrix();
  // constructeur spécialisé
  Matrix(int nx, int ny, string name);
  // destructeur
  ~Matrix();

  // convention : tous les membres se terminent en _
  double** array_;

  int nx_;
  int ny_;
  
  // classe standard qui permet de représenter et manipuler ! une chaîne de caractères
  string name_;

  // fonctions membres
  void periodic();
  void setBoundary();
  void allocate();
  void deallocate();
  void write(string filename);
  void out();
  
  // operateurs d'access surchargés
  // operateur de lecture
  double operator()(int i, int j) const;
  // operateur d'écriture
  double& operator()(int i, int j);
  // operateur de lecture pour vecteur
  double operator()(int i) const;
  // operateur d'écriture pour vecteur
  double& operator()(int i);
  // operateurs de copy, sommation et multiplication
  Matrix& operator=(const Matrix& matrix);
  Matrix& operator=(double value);
  Matrix& operator+=(const Matrix& matrix);
  Matrix& operator*=(double value);
  
  bool get_allocated() const {return allocated_;}

  // les variable privées ne sont que manipulable par les fonctions membres
private:
  bool allocated_;
};

// définitions des contructeurs
Matrix::Matrix()
{
  cout << "Matrix::Matrix()\n"; 
  nx_ = 0;
  ny_ = 0;
  allocated_ = false;
  array_ = NULL;
  name_ = "";
}

// constructeur specialisé
Matrix::Matrix(int nx, int ny, string name)
{
  nx_ = nx;
  ny_ = ny;
  name_ = name;
  allocated_ = false;
  
  allocate();
}

// destructeur
Matrix::~Matrix()
{
  if(allocated_)
    deallocate();
}

// définitions des fonctions membres
void Matrix::allocate()
{
  assert(!allocated_);

  array_ = new double*[nx_];
  for(int i=0;i<nx_;i++)
    array_[i] = new double[ny_];

  allocated_ = true;
}

void Matrix::deallocate()
{
  assert(allocated_);

  for(int i=0;i<nx_;i++)
    delete [] array_[i];

  delete [] array_;

  allocated_ = false;
}

void Matrix::periodic()
{
  this->array_[0][0] = this->array_[nx_-2][0];
  this->array_[nx_-1][0] = this->array_[1][0];
}

// conditions de bord pour la chaleur
void boundaryWallPeriodic(Matrix& matrix)
{
  int nx = matrix.nx_;
  int ny = matrix.ny_;

  // moving wall en y
  for(int i=0;i<nx;i++) {
    matrix(i,0) = 0;
    matrix(i,ny-1) = 1;
  }
  
  // périodique en x
  for(int j = 0; j < ny; j++) {
    matrix(0,j) = matrix(nx-2,j);
    matrix(nx-1,j) = matrix(1,j);
  }
}

// conditions de bord pour la pression
void boundaryNeumannPeriodic(Matrix& matrix)
{
  int nx = matrix.nx_;
  int ny = matrix.ny_;

  // von-Neumann en y
  for(int i=0;i<nx;i++) {
    matrix(i,0) = matrix(i,1);
    matrix(i,ny-1) = matrix(i,ny-2);
  }
  
  // // périodique en x
  // for(int j = 0; j < ny; j++) {
  //   matrix(0,j) = matrix(nx-3,j);
  //   matrix(nx-1,j) = matrix(2,j);
  // }
  
  // zero grandient en x
  for(int j = 0; j < ny; j++) {
    matrix(0,j) = matrix(1,j);
    matrix(nx-1,j) = matrix(nx-2,j);
  }
  
}

void boundaryUSlipPeriodic(Matrix& matrix)
{
  int nx = matrix.nx_;
  int ny = matrix.ny_;
  
  // vitesse constante = 1 en y
  for(int i=0;i<nx;i++) {
    matrix(i,0) = 2-matrix(i,1);
    matrix(i,ny-1) = 2-matrix(i,ny-2);
  }

  // // périodique en x  
  // for(int j=0;j<ny;j++) {
  //   matrix(0,j) = matrix(nx-2,j);
  //   matrix(nx-1,j) = matrix(1,j);
  // }

  // constante a l'entree et sortie  
  for(int j=0;j<ny;j++) {
    matrix(0,j) = 1;
    matrix(nx-1,j) = 1;
  }
}

void boundaryVSlipPeriodic(Matrix& matrix)
{
  // cerr << "boundaryVSlipPeriodic for " << matrix.name_ << std::endl;
  int nx = matrix.nx_;
  int ny = matrix.ny_;
  
  // non-penetrant en y
  for(int i=0;i<nx;i++) {
    matrix(i,0) = 0;
    matrix(i,ny-1) = 0;
  }

  // // périodique en x
  // for(int j=0;j<ny;j++) {
  //   matrix(0,j) = matrix(nx-3,j);
  //   matrix(nx-1,j) = matrix(2,j);
  // }

  // vitesse zero en y
  for(int i=0;i<ny;i++) {
    matrix(0,i) = -matrix(1,i);
    matrix(nx-1,i) = -matrix(nx-2,i);
  }
}

void Matrix::setBoundary()
{
  //  std::cerr << "setting boundary for " << name_ << std::endl;
  
  bool set = false;
  if(name_.rfind("p") != std::string::npos) {
    boundaryNeumannPeriodic(*this);
    set = true;
  }
  if(name_.rfind("u") != std::string::npos) {
    boundaryUSlipPeriodic(*this);
    set = true;
  }
  if(name_.rfind("v") != std::string::npos) {
    boundaryVSlipPeriodic(*this);
    set = true;
  }
  if(name_.rfind("c") != std::string::npos) {
    boundaryWallPeriodic(*this);
    set = true;
  }
  if(set == false) {
    std::cerr << "ERROR: no boundary set for " << name_ << std::endl;
    exit(1);
  }
}

double Matrix::operator()(int i, int j) const
{
  assert(i>=0 && i<nx_);
  assert(j>=0 && j<ny_);
  
  return array_[i][j];
}

double& Matrix::operator()(int i, int j)
{
  assert(i>=0 && i<nx_);
  assert(j>=0 && j<ny_);
  
  return array_[i][j];
}

double Matrix::operator()(int i) const
{
  assert(i>=0 && i<nx_);
  assert(ny_ == 1);
  
  return array_[i][0];
}

double& Matrix::operator()(int i)
{
  assert(i>=0 && i<nx_);
  assert(ny_ == 1);
  
  return array_[i][0];
}

Matrix& Matrix::operator=(const Matrix& matrix)
{
  assert(allocated_ && matrix.allocated_);
  assert(nx_ == matrix.nx_ && ny_ == matrix.ny_);
  
  for(int i=0;i<nx_;i++)
    for(int j=0;j<ny_;j++) {
      //      cerr << i << "," << j << "\t";
      array_[i][j] = matrix.array_[i][j];  
    }

  return *this;
}

Matrix& Matrix::operator=(double value)
{
  assert(allocated_);
  
  for(int i=0;i<nx_;i++)
    for(int j=0;j<ny_;j++)
      array_[i][j] = value;

  return *this;
}

Matrix& Matrix::operator+=(const Matrix& matrix)
{
  assert(allocated_ && matrix.allocated_);
  assert(nx_ == matrix.nx_ && ny_ == matrix.ny_);
  
  for(int i=0;i<nx_;i++)
    for(int j=0;j<ny_;j++)
      array_[i][j] += matrix.array_[i][j];

  return *this;
}

Matrix& Matrix::operator*=(double value)
{
  assert(allocated_);

  for(int i=0;i<nx_;i++)
    for(int j=0;j<ny_;j++)
      array_[i][j] *= value;

  return *this;
}

// écrit le champ en format vtk
void Matrix::write(string filename)
{
  //  printf("write %s\n",matrix->name);
  assert(allocated_);

  FILE *fp;

  ofstream out(filename.c_str());

  out << "<?xml version=\"1.0\"?>\n";
  out << "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
  out << "<ImageData WholeExtent=\"0 " << nx_-1 << " 0 " << ny_-1 << " 0 0\" Origin=\"0 0 0\" Spacing=\"1 1 1\">\n";
  out << "<Piece Extent=\"0 " << nx_-1 << " 0 " << ny_-1 << " 0 0\">\n";
  out << "<PointData Scalars=\"" << name_ << "\">\n";
  out << "<DataArray type=\"Float32\" Name=\"" << name_ << "\" format=\"ascii\">\n";
  
  for(int j=0;j<ny_;j++) 
    for(int i=0;i<nx_;i++) 
      out << array_[i][j] << "\t";

  out << "\n</DataArray>\n" << "</PointData>\n" << "</Piece>\n" << "</ImageData>\n" << "</VTKFile>\n";
  out.close();
}

// écrit le champ en format vtk
void Matrix::out()
{
  for(int j=0;j<ny_;j++) {
    for(int i=0;i<nx_;i++) {
      cout << array_[i][j] << "\t";
    }
    cout << endl;
  }  
}

// calcule le module de la différence maximal entre deux champ
double diff(const Matrix& newMatrix, const Matrix& oldMatrix)
{
  assert(newMatrix.nx_ == oldMatrix.nx_);
  assert(newMatrix.ny_ == oldMatrix.ny_);
  
  int nx = newMatrix.nx_;
  int ny = newMatrix.ny_;
  assert(nx == oldMatrix.nx_);
  assert(ny == oldMatrix.ny_);

  double difference = 0;

  int x = 0;
  int y = 0;
  double newVal = newMatrix(0,0);
  double oldVal = oldMatrix(0,0);

  //  max norm
  for(int i=0;i<nx;i++)
    for(int j=0;j<ny;j++) {
      if(fabs(newMatrix(i,j)-oldMatrix(i,j)) > difference) {
  	difference = fabs(newMatrix(i,j)-oldMatrix(i,j));
  	x = i;
  	y = j;
  	newVal = newMatrix(i,j);
  	oldVal = newMatrix(i,j);
      }
    }
  
  return difference;
}

#endif
