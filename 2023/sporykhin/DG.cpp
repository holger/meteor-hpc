#include <iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <map>
#include <functional>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"
#include <cassert>

#define assertm(exp, msg) assert(((void)msg, exp))

namespace kk = Kokkos;

//double y;
//const unsigned int nt = round((xmax - xmin)/dt);
//double error = 0;
//double term = 0;

KOKKOS_INLINE_FUNCTION
double f(double const& x)
{
    double y;

    y = sin(x);

    return y;
}

template<typename FunctionType>
KOKKOS_INLINE_FUNCTION
double gaussQuadExt(int const& n, int const& i, double const& dx, FunctionType fx) //[a,b] interval
{
    double result;
    double a = dx*(i-0.5);
    double b = dx*(i+0.5);
    const double gx2 = 1./sqrt(3.); //gauss x for n = 2
    const double gx3 = sqrt(3./5.); //gauss x for n = 3
    const double w3m = 8./9.; //weight 3 middle
    const double w3s = 5./9.; //weight 3 side

    switch (n)
    {

        case 1:
            result = (b-a)*0.5 * 2 * fx((b-a)*0.5*0 + (a+b)*0.5);
            break;

        case 2:
            result = (b-a)*0.5 * (fx((b-a)*0.5*-gx2 + (a+b)*0.5)+fx((b-a)*0.5*gx2 + (a+b)*0.5));
            break;
        
        case 3:
            result = (b-a)*0.5 * (w3s*fx((b-a)*0.5*-gx3 + (a+b)*0.5)+w3m*fx((b-a)*0.5*0 + (a+b)*0.5)+ w3s*fx((b-a)*0.5*gx3 + (a+b)*0.5));
            break;
    }

    return result;
}

KOKKOS_INLINE_FUNCTION
void periodicBoundaries(kk::View<double*> view)
{
    const int N = view.extent(0);
    
    kk::parallel_for("Periodic", 1, KOKKOS_LAMBDA(int i)
    {
        view(0) = view(N-2);
        view(N-1) = view(1);
    });
}

KOKKOS_INLINE_FUNCTION
int P0(double const& x)
{
    return 1;
}

KOKKOS_INLINE_FUNCTION
double P1(double const& x, int const& i, double const& dx)
{
    double x0 = dx*(i-0.5);
    double x1 = dx*(i+0.5);

    return (2*x - 2*x1)/dx+1;
}

KOKKOS_INLINE_FUNCTION
void c0f(kk::View<double*> rhs0, const kk::View<double*> c0_o, const kk::View<double*> c1_o, double const& v, double const& dx)
{
    kk::parallel_for("c0f", kk::RangePolicy<>(1,rhs0.extent(0)-1), KOKKOS_LAMBDA(unsigned int i)
    {
        rhs0(i) = -v/dx * (c0_o(i) - c0_o(i-1) + c1_o(i) - c1_o(i-1));
        //std::cout << "u[i] = " << rhs0[i] << " c0[i] = " << c0_o[i] << " c1[i] = " << c1_o[i] << std::endl;
    });

    periodicBoundaries(rhs0);
}

KOKKOS_INLINE_FUNCTION
void c1f(kk::View<double*> rhs1, const kk::View<double*> c0_o, const kk::View<double*> c1_o, double const& v, double const& dx)
{
    kk::parallel_for("c1f", kk::RangePolicy<>(1,rhs1.extent(0)-1), KOKKOS_LAMBDA(unsigned int i)
    {
        rhs1(i) = 3*v/dx * (c0_o(i) - c0_o(i-1) - c1_o(i) - c1_o(i-1));
    });

    periodicBoundaries(rhs1);
}

void write(std::ofstream& file, double t, const kk::View<double*, kk::HostSpace> hostx, const kk::View<double*, kk::HostSpace> hostu, const int nx) //& takes the reference of the var being copied rather than copying it
{
  file << "#time = " << t << std::endl;
  for(int ix = 0; ix < nx; ix++)
  {
    file << hostx(ix) << " " << hostu(ix) << std::endl;
  }
  file << std::endl << std::endl;

}

int 
main(int argc, char* argv[]) {
    kk::initialize();
    printf("Running in execution space %s, HostSpace %s\n",
        typeid(Kokkos::DefaultExecutionSpace).name(), typeid(Kokkos::DefaultHostExecutionSpace).name());
    
    {
        // std::string key;
        // double value;

        // std::ifstream infile("config.txt");

        // std::map<std::string, double> boolMap;

        // while (infile >> key >> value)
        // {
        //     boolMap[key] = value;
        // }
        // const int nx = boolMap["N"];

        // infile.close();

        int nx;

        if (argc == 1)
        {
        nx = 100;
        std::cout << "Using default array size, N=100" << std::endl;
        }
        else if(argc == 2)
        {
        assertm(std::atoi(argv[1]) > 0, "Negative array sizes not allowed");
        nx = std::atoi(argv[1]);
        }
        else
        {
        std::cout << "Invalid amount of arguments passed" << std::endl;
        return 1;
        }

        //const int N = 200;
        const double xmin = 0.;
        const double xmax = 2*M_PI;
        const double dx = (xmax - xmin) / (nx-2.);
        double t = 0.0;
        const double CFL = 0.1;
        const double v = 1.0;
        double dt = CFL*dx;
        const unsigned int nt = 2000;
        const int n = 3;

        // kk::View<float*, kk::DefaultExecutionSpace>  a("a", nx);
        // kk::View<float*, kk::DefaultExecutionSpace>  b("b", nx);
        // kk::View<float*, kk::DefaultExecutionSpace>  c("c", nx);

        std::cout << "Assigning Views" << std::endl;

        kk::View<double*> x("x", nx);
        kk::View<double*> u("u", nx);
        kk::View<double*> c0("c0", nx);
        kk::View<double*> c1("c1", nx);
        kk::View<double*> c0_o("c0", nx);
        kk::View<double*> c1_o("c1", nx);
        kk::View<double*> c0s("c0", nx);
        kk::View<double*> c1s("c1", nx);
        kk::View<double*> rhs0("c0", nx);
        kk::View<double*> rhs1("c1", nx);

        //kk::deep_copy(a, 3);

        auto hostx = kk::create_mirror_view(x);
        auto hostu = kk::create_mirror_view(u);

        std::cout << "Initialising" << std::endl;

        kk::Timer timer;
        kk::Timer deep_profiler;

        kk::parallel_for("initialising x", nx, KOKKOS_LAMBDA(unsigned int i)
        {
            x(i) = xmin + i*dx;
        });

        kk::parallel_for("initialising u", kk::RangePolicy<>(1,nx-1), KOKKOS_LAMBDA(unsigned int ix)
        {
            c0(ix) = gaussQuadExt(n, ix, dx, KOKKOS_LAMBDA (double const& x) {return 1/dx * f(x)*P0(x);});
            c1(ix) = gaussQuadExt(n, ix, dx, KOKKOS_LAMBDA (double const& x) {return 3/dx * f(x)*P1(x, ix, dx);});
            u(ix) = c0(ix)*P0(x(ix)) + c1(ix)*P1(x(ix), ix, dx);
        });

        periodicBoundaries(u);
        periodicBoundaries(c0);
        periodicBoundaries(c1);

        kk::fence();
        std::cout << "Initialised in " << timer.seconds() << " seconds" << std::endl;

        kk::deep_copy(c0_o, c0);
        kk::deep_copy(c1_o, c1);

        kk::deep_copy(hostx, x);
        kk::deep_copy(hostu, u);

        // std::ofstream profile("initial_profile.txt");

        // for(int i = 0; i < nx; i++)
        // {
        //     profile << hostx(i) << " " << hostu(i) << std::endl;
        // }

        // profile.close();

        //writing numerical solution
        // std::ofstream file("flux.txt");

        // file << "#title = RK2 Discontinuous Galerkin" << std::endl;
        // file << "#dim = 1" << std::endl;
        // file << "#xmin = " << xmin << std::endl;
        // file << "#xmax = " << xmax << std::endl;
        // file << "#ymin = " << -2. << std::endl;
        // file << "#ymax = " << 2. << std::endl;

        // write(file, t, hostx, hostu, nx);

        std::cout << "Entering time loop" << std::endl;
        timer.reset();

        for(unsigned int it = 0; it < nt+1; it++)
        {
            t += it*dt;

            c0f(rhs0, c0_o, c1_o, v, dx);
            c1f(rhs1, c0_o, c1_o, v, dx);

            //kk::fence();

            kk::parallel_for("RHS 0s", nx, KOKKOS_LAMBDA(unsigned int i)
            {
                c0s(i) = c0_o(i) + rhs0(i)*dt*0.5;
                rhs0(i) = c0s(i);
            });

            kk::parallel_for("RHS 1s", nx, KOKKOS_LAMBDA(unsigned int i)
            {
                c1s(i) = c1_o(i) + rhs1(i)*dt*0.5;
                rhs1(i) = c1s(i);
            });

            //kk::fence();

            c0f(rhs0, c0s, c1s, v, dx);
            c1f(rhs1, c0s, c1s, v, dx);

            //kk::fence();

            kk::parallel_for("RHS 0", nx, KOKKOS_LAMBDA(unsigned int i)
            {
                c0(i) = c0_o(i) + rhs0(i)*dt;
                c0_o(i) = c0(i);
            });

            kk::parallel_for("RHS 1", nx, KOKKOS_LAMBDA(unsigned int i)
            {
                c1(i) = c1_o(i) + rhs1(i)*dt;
                c1_o(i) = c1(i);
            });

            //kk::fence();

            kk::parallel_for("Final u", kk::RangePolicy<>(1,nx-1), KOKKOS_LAMBDA(unsigned int ix)
            {
                u(ix) = c0(ix)*P0(x(ix)) + c1(ix)*P1(x(ix), ix, dx);
            });

            periodicBoundaries(u);

            //remainder of it devided by snapshot
            // if ((it % (int) 10) == 0)
            // {
            //     kk::deep_copy(hostu, u);
            //     std::cout << "writing at t = " << t << std::endl;
            //     write(file, t, hostx, hostu, nx);
            // }


        }
        kk::fence();
        std::cout << nx << " Array size simulation done in " << timer.seconds() << " seconds!" << std::endl;

        // std::cout << "sin of " << hostx(1) << " is " << hostu(1) << std::endl;
        // std::cout << "u(0) " << hostu(0) << " u(N-2) " << hostu(nx-2) << std::endl;
        // std::cout << "u(0) " << hostu(0) << " u(N-1) " << hostu(nx-1) << std::endl;

        

        // kk::Timer timer;

        // kk::parallel_for("copy", nx, KOKKOS_LAMBDA(int i) {
        //     b(i) = a(i);
        // });

        // std::cout << "Time to parallel_for copy " << nx << " elements: " << timer.seconds() << std::endl;

        // timer.reset();

        // std::cout << "printing timer for testing: " << timer.seconds() << std::endl; 
        
        // timer.reset();

        // kk::deep_copy(c, a);

        // std::cout << "Time to deep_copy " << nx << " elements: " << timer.seconds() << std::endl;

    }
    kk::finalize();
    return 0;
}

