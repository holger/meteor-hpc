#include <iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <map>
#include <functional>
#include <cassert>
#include <chrono>

using namespace std::chrono;

#define assertm(exp, msg) assert(((void)msg, exp))


class Vector
{
public:

    //constructor
    Vector(int n)
    {
        data_ = new double[n];
        n_ = n;
    }

    //copy-constructor
    Vector(Vector const& vec)
    {
        //std::cout << "Vector(Vector const& vec)\n";

        n_ = vec.n_;
        data_ = new double[n_];

        for(int i = 0; i < n_; i++)
        {
            data_[i] = vec.data_[i];
        }
    }

    //operator overloading
    double& operator[](int i)
    {
        if(i >= n_ || i < 0)
        {
            std::cout << "ERROR in double& operator[](int i): " << i << " out of bounds [0," << n_-1 << "]\n";
            exit(1);
        }
        return data_[i];
    }

        //operator overloading
    double operator[](int i) const
    {
        if(i >= n_ || i < 0)
        {
            std::cout << "ERROR in double& operator[](int i): " << i << " out of bounds [0," << n_-1 << "]\n";
            exit(1);
        }
        return data_[i];
    }

    Vector const& operator*=(double a)
    {
        for(int i = 0; i < n_; i++)
        {
            data_[i] *= a;
        }

        return *this;
    }

    // Vector const& operator*(double a)
    // {
    //     for(int i = 0; i<n_; i++)
    //     {
    //         data_[i] *= a;
    //     }

    //     return *this;
    // }

    Vector operator+(Vector const& u) const
    {
        if(u.size() != n_)
        {
            std::cout << "ERROR in Vector const& operator+, adding vectors of different sizes" << std::endl;
            exit(1);
        }

        Vector v_temp(u);

        for(int i = 0; i < n_; i++)
        {
            v_temp[i] = u.data_[i] + data_[i];
        }

        return v_temp;
    }

    Vector const& operator=(Vector const& vec) //same here
    {
        for(int i = 0; i < n_; i++)
        {
            data_[i] = vec.data_[i];
        }

        return *this;
    }

    int size() const
    {
        return n_;
    }

    double* data()
    {
        return data_;
    }

private:
    double* data_;
    int n_;

};

Vector operator*(Vector const& v, double a)
{
    Vector v_temp(v);
    for(int i = 0; i<v.size(); i++)
    {
        v_temp[i] *= a;
    }

    return v_temp;
}


double y;
double result;
double gx2 = 1./sqrt(3.); //gauss x for n = 2
double gx3 = sqrt(3./5.); //gauss x for n = 3
double w3m = 8./9.; //weight 3 middle
double w3s = 5./9.; //weight 3 side
double a;
double b;
const int n = 3;
int N;
double xmin;
double xmax;
double dx;
double dxlong;
double x0;
double x1;
double t;
double CFL;
double v;
double dt;
unsigned int nt;
//const unsigned int nt = round((xmax - xmin)/dt);
// double error = 0;
// double term = 0;

double f(double const& x)
{
    y = sin(x);

    return y;
}

double gaussQuad(int const& n, std::function<double(double const& x)> fx) //[-1,1] interval
{
    switch (n)
    {
        case 1:
            result = 2*fx(0);
            break;

        case 2:
            result = fx(-gx2) + fx(gx2);
            break;

        case 3:
            result = w3s * fx(-gx3) + w3m * fx(0) + w3s * fx(gx3);
            break;
    }

    return result;
}

double gaussQuadExt(int const& n, int const& i, std::function<double(double const& x)> fx) //[a,b] interval
{
    a = dx*(i-0.5);
    b = dx*(i+0.5);
    //std::cout << "a = " << a << " b = " << b << std::endl;
    switch (n)
    {

        case 1:
            result = (b-a)*0.5 * 2 * fx((b-a)*0.5*0 + (a+b)*0.5);
            break;

        case 2:
            result = (b-a)*0.5 * (fx((b-a)*0.5*-gx2 + (a+b)*0.5)+fx((b-a)*0.5*gx2 + (a+b)*0.5));
            break;

        case 3:
            result = (b-a)*0.5 * (w3s*fx((b-a)*0.5*-gx3 + (a+b)*0.5)+w3m*fx((b-a)*0.5*0 + (a+b)*0.5)+ w3s*fx((b-a)*0.5*gx3 + (a+b)*0.5));
            break;
    }

    return result;
}

void periodicBoundaries(Vector& u)
{
    u[0] = u[N-2];
    u[N-1] = u[1];
}

int P0(double const& x)
{
    return 1;
}

double P1(double const& x, int const& i)
{
    x0 = dx*(i-0.5);
    x1 = dx*(i+0.5);

    return (2*x - 2*x1)/dx+1;
}

double f0(double const& x)
{
    return 1/dx * f(x) * P0(x);
}

// double f1(double const& x)
// {
//     return 3/dx * f(x) * P1(x);
// }

void c0f(Vector& rhs0, const Vector& c0_o, const Vector& c1_o)
{
    for(int i = 1; i < rhs0.size()-1; i++)
    {
        rhs0[i] = -v/dx * (c0_o[i] - c0_o[i-1] + c1_o[i] - c1_o[i-1]);
        //std::cout << "u[i] = " << rhs0[i] << " c0[i] = " << c0_o[i] << " c1[i] = " << c1_o[i] << std::endl;
    }

    periodicBoundaries(rhs0);
}

void c1f(Vector& rhs1, const Vector& c0_o, const Vector& c1_o)
{
    for(int i = 1; i < rhs1.size()-1; i++)
    {
        rhs1[i] = 3*v/dx * (c0_o[i] - c0_o[i-1] - c1_o[i] - c1_o[i-1]);
    }

    periodicBoundaries(rhs1);
}

void write(std::ofstream& file, double t, double* const& x, double* const& u, int nx) //& takes the reference of the var being copied rather than copying it
{
  file << "#time = " << t << std::endl;
  for(int ix = 0; ix < nx; ix++)
  {
    file << x[ix] << " " << u[ix] << std::endl;
  }
  file << std::endl << std::endl;

}

int main(int argc, char* argv[])
{

    if (argc == 1)
    {
        N = 100;
        std::cout << "Using default array size, N=100" << std::endl;
    }
    else if(argc == 2)
    {
        assertm(std::atoi(argv[1]) > 0, "Negative array sizes not allowed");
        N = std::atoi(argv[1]);
    }
    else
    {
        std::cout << "Invalid amount of arguments passed" << std::endl;
        return 1;
    }

    xmin = 0.;
    xmax = 2*M_PI;
    dx = (xmax - xmin) / (N-2.);
    dxlong = (xmax - xmin) / (N*10 - 2);
    t = 0.0;
    CFL = 0.1;
    v = 1.0;
    dt = CFL*dx;
    //nt = 2000;
    nt = round((xmax - xmin)/dt);
    double error = 0;
    double term = 0;

    std::cout << "Defining vectors" << std::endl;

    Vector x(N);
    Vector xlong(N*10);
    Vector u(N);
    Vector ulong(N*10);
    Vector c0(N);
    Vector c1(N);
    Vector c0_o(N);
    Vector c1_o(N);
    Vector c0s(N);
    Vector c1s(N);
    Vector rhs0(N);
    Vector rhs1(N);
    Vector ua(N);

    for(int i = 0; i < x.size(); i++)
    {
        x[i] = xmin + i*dx;
        //std::cout << x[i] << std::endl;
    }

    for(int i = 0; i < xlong.size(); i++)
    {
        xlong[i] = xmin + i*dxlong;
    }

    for(int ix = 1; ix < x.size()-1; ++ix)
    {
        c0[ix] = gaussQuadExt(n, ix, [] (double const& x) {return 1/dx * f(x)*P0(x);});
        c1[ix] = gaussQuadExt(n, ix, [&] (double const& x) {return 3/dx * f(x)*P1(x, ix);});
        u[ix] = c0[ix]*P0(x[ix]) + c1[ix]*P1(x[ix], ix);

    }

    periodicBoundaries(c0);
    periodicBoundaries(c1);
    periodicBoundaries(u);
    ua = u;

    c0_o = c0;
    c1_o = c1;

    // for(int ix = 0; ix < 10; ix++)
    // {
    //     ulong[ix] = ulong[N*10 - 20 + ix];
    //     ulong[N*10 - 10 + ix] = ulong[10+ix];
    // }

    for(int ix = 0; ix < x.size(); ++ix)
    {
        for(int i = 0; i < 10; i++)
        {
            ulong[10*ix+i] = c0[ix]*P0(xlong[10*ix+i]) + c1[ix]*P1(xlong[10*ix+i], ix);
        }
    }

    // std::cout << "f(2) = " << f(2) << std::endl;
    // std::cout << "Integral of f(x) on [-1,1] = " << gaussQuad(n, sin) << " for order " << n << std::endl;
    // std::cout << "Integral of f(x) on [1/2;3/2] = " << gaussQuadExt(n, 1, [] (double const& x) {return sin(x);}) << " for order " << n << std::endl;

    // std::ofstream profile("initial_profile.txt");

    // for(int i = 0; i < xlong.size(); i++)
    // {
    //     profile << xlong[i] << " " << ulong[i] << std::endl;
    // }

    // profile.close();

    // writing numerical solution
    // std::ofstream file("flux.txt");

    // file << "#title = RK2 Discontinuous Galerkin" << std::endl;
    // file << "#dim = 1" << std::endl;
    // file << "#xmin = " << xmin << std::endl;
    // file << "#xmax = " << xmax << std::endl;
    // file << "#ymin = " << -2. << std::endl;
    // file << "#ymax = " << 2. << std::endl;

    // write(file, t, xlong.data(), ulong.data(), xlong.size());

    //time loop

    std::cout << "Entering time loop" << std::endl;

    auto start = high_resolution_clock::now();
    for(unsigned int it = 1; it < nt+1; it++)
    {
        t += it*dt;

        // c0 = c0 + dt * c0f(c0_o + dt*0.5*(c0f(c0_o, c0, c1)), c0, c1);

        c0f(rhs0, c0_o, c1_o);
        c0s = c0_o + rhs0*dt*0.5; //DOESN'T WORK FOR SOME REASON
        c1f(rhs1, c0_o, c1_o);
        c1s = c1_o + rhs1*dt*0.5;

        rhs0 = c0s;
        rhs1 = c1s;

        c0f(rhs0, c0s, c1s);
        c1f(rhs1, c0s, c1s);

        c0 = c0_o + rhs0*dt;
        c1 = c1_o + rhs1*dt;




        for(int ix = 1; ix < x.size()-1; ++ix)
        {
            //c0[ix] = -dt*v/dx * (c0_o[ix] - c0_o[ix-1] + c1_o[ix] - c1_o[ix-1]) + c0_o[ix];
            //c1[ix] = 3*dt*v/dx * (c0_o[ix] - c0_o[ix-1] - c1_o[ix] - c1_o[ix-1]) + c1_o[ix];

            u[ix] = c0[ix]*P0(x[ix]) + c1[ix]*P1(x[ix], ix);

        }

        periodicBoundaries(u);
        //periodicBoundaries(c0);
        //periodicBoundaries(c1);
        c1_o = c1;
        c0_o = c0;

        //analytical solution at time t

        // for(int ix = 1; ix < x.size()-1; ++ix)
        // {
        //     a = dx*(ix-0.5);
        //     b = dx*(ix+0.5);
        //     ua[ix] = (cos(0.1*v*t - a) - cos(0.1*v*t - b))/dx;
        // }

        // //uncomment for long u

        // for(int ix = 0; ix < x.size(); ++ix)
        // {
        //     for(int i = 0; i < 10; i++)
        //     {
        //         ulong[10*ix+i] = c0[ix]*P0(xlong[10*ix+i]) + c1[ix]*P1(xlong[10*ix+i], ix);
        //     }
        // }

        // // remainder of it devided by snapshot
        // if ((it % (int) 10) == 0)
        // {
        //     std::cout << "writing at t = " << t << std::endl;
        //     write(file, t, xlong.data(), ulong.data(), xlong.size());
        // }
    }
    auto stop = high_resolution_clock::now();

    auto duration = duration_cast<seconds>(stop - start);
    auto milliduration = duration_cast<milliseconds>(stop - start);
    auto microduration = duration_cast<microseconds>(stop - start);

    std::cout << N << " Array size simulation done in " << duration.count() << " seconds!" << std::endl;
    std::cout << N << " Array size simulation done in " << milliduration.count() << " milliseconds!" << std::endl;
    std::cout << N << " Array size simulation done in " << microduration.count() << " microseconds!" << std::endl;


    for(int ix = 0; ix<x.size(); ++ix)
    {
        term = u[ix] - sin(x[ix]);
        error += term*term;
    }

    std::cout << "error after 1 period = " << sqrt(error*dx) << std::endl;

    // file.close();

}