# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 13:24:51 2023

@author: berej
"""

import numpy as np
from matplotlib import pyplot as plt

#import scienceplots

#plt.style.use(['science', 'ieee'])

plt.close('all')

data = np.loadtxt('initial_profile.txt', unpack = True)

fig, axs = plt.subplots(num = 'Initial Profile')
axs.scatter(data[0], data[1], s = 2)
axs.set_xlabel('x')
axs.set_ylabel('Velocity profile')

N_upwind = np.array([10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 128, 200, 256, 500, 512, 1000])
err_upwind_CFL1 = np.array([1.55016, 0.673475, 0.422763, 0.307209, 0.241064, 0.198291, 0.168383, 0.146302, 0.129335, 0.115891, 0.0897578, 0.056806, 0.0441866, 0.0224521, 0.0219218, 0.0111813])

N = np.array([10, 20, 30, 40, 50, 100, 200, 300, 400 ,500, 1000, 2000])
err = np.array([0.12607, 0.0160312, 0.00561508, 0.00279222, 0.001656572, 0.000354264, 8.16767e-05, 3.5317e-05, 1.95929e-05, 1.24354e-05,
       3.05727e-06, 7.57921e-07])
#err_n1 = np.array([0.273049, 0.0775336, 0.0450938, 0.031934, 0.0247602, 0.0117015, 0.00570346, 0.00377144, 0.00281725, 0.00224842, 0.00111889, 0.000558135])
err_n1 = np.array([0.170609, 0.0249121, 0.00928662, 0.00478572, 0.00290597, 0.000653723, 0.000154981, 6.76681e-05, 3.77263e-05, 2.40163e-05, 5.94024e-06, 1.47714e-06])
err_RK0 = np.array([0.376773, 0.189627, 0.123866, 0.0917182, 0.0727584, 0.035713, 0.0176784, 0.011745, 0.00879343])

N_kokkos = np.array([10, 100, 1000, 10e4, 10e5, 10e6, 10e7, 10e8, 10e9])

# =============================================================================
# cuda_par_cop = np.array([1.55e-05, 1.49e-05, 1.72e-05, 1.39e-05, 1.49e-05, 3.91e-05, 3.97e-05, 4.02e-05, 5.93e-05])
# cuda_deep_cop = np.array([0.0001346, 0.0001764, 0.0001492, 0.0001364, 0.0002672, 0.0013146, 0.0109391, 0.104306, 1.58657])
# openmp_par_cop = np.array([7.5e-06, 7.7e-06, 1.16e-05, 2.48e-05, 0.0001704, 0.0015988, 0.0176386, 0.169114, 3.20656])
# openmp_deep_cop = np.array([4.3e-06, 4.4e-06, 3.2e-06, 4.2e-06, 3.87e-05, 0.0006424, 0.0045259, 0.0399585, 2.48723])
# serial_par_cop = np.array([4.1e-06, 5.2e-06, 1.48e-05, 0.0001107, 0.001095, 0.0109022, 0.108346, 1.0878])
# serial_deep_cop = np.array([4.1e-06, 5.8e-06, 4.3e-06, 5.4e-06, 1.75e-05, 0.0002845, 0.0028509, 0.0305461])
# =============================================================================

serial_par_cop = np.array([4.9e-06, 5.9e-06, 1.5e-05, 0.00012, 0.0010916, 0.0109797, 0.109179, 1.09584])
serial_deep_cop = np.array([4.2e-06, 4.6e-06, 4.4e-06, 5.9e-06, 2.33e-05, 0.0003625, 0.0031363, 0.0258445])

openmp_par_cop = np.array([1.25e-05, 1.39e-05, 1.87e-05, 2.93e-05, 0.0001845, 0.0015747, 0.0200131, 0.229745, 5.35379])
openmp_deep_cop = np.array([3.1e-06, 3.2e-06, 3.4e-06, 4.1e-06, 3.72e-05, 0.000511, 0.0043222, 0.0682888, 7.04055])

cuda_par_cop = np.array([8.14e-05, 8.07e-05, 7.26e-05, 7.48e-05, 0.0001768, 0.0011076, 0.0152466, 0.137326, 1.36916])
cuda_deep_cop = np.array([0.0001991, 6.92e-05, 6.72e-05, 6.26e-05, 6.97e-05, 0.0001211, 0.0008371, 0.0013123, 0.35229])

N_sim = np.array([10, 100, 1000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 10e5, 10e6, 10e7])

serial_sim = np.array([0.0178898, 0.0629593, 0.501283, 1.92353, 2.40318, 2.88185, 3.35547, 3.84285, 4.39519, 4.8498, 48.3331])
openMP_sim = np.array([0.0885788, 0.135531, 0.350663, 0.643078, 0.77425, 0.699392, 0.890971, 1.4619, 1.71473, 1.5437, 10.9336, 102.432])
serialCUDA_sim = np.array([1.64206, 1.61548, 1.58619, 1.58363, 1.64127, 1.61929, 1.64237, 1.63696, 1.79168, 1.79132, 5.3786, 42.8791, 420.155])

N_sim_2 = np.array([4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576])

# =============================================================================
# serial_sim2 = np.array([0.0136494, 0.0155264, 0.0197757, 0.0279333, 0.0436916, 0.0744442, 0.138478, 0.259761, 0.514253, 0.997262, 1.99663, 3.96048, 7.98087, 15.9699, 31.8827, 64.008])
# openMP_sim2 = np.array([0.0924063, 0.120712, 0.166867, 0.172575, 0.181782, 0.235155, 0.302877, 0.359856, 0.559335, 0.657563, 0.951839, 1.3262, 2.42313, 4.3897, 8.52602, 16.5051, 31.547, 59.7911])
# CUDA_sim2 = np.array([1.22088, 1.2088, 1.22612, 1.18127, 1.20618, 1.22689, 1.18438, 1.21193, 1.20479, 1.21228, 1.19298, 1.20548, 1.37316, 1.88223, 3.41315, 6.65739, 12.2923, 23.8817, 48.8711])
# =============================================================================

cpp_sim = np.array([0.000725, 0.001126, 0.001920, 0.003384, 0.006484, 0.013673, 0.026704, 0.055074, 0.116243, 0.245387, 0.505137, 1.013595, 2.054198, 4.267292, 92.759647])

serial_sim2 = np.array([0.0134119, 0.0153946, 0.0194905, 0.0275894, 0.0438759, 0.0738727, 0.134706, 0.253724, 0.494327, 0.974632, 1.91824, 3.84129, 7.64548, 15.2401, 30.3941, 60.8644])
openMP_sim2 = np.array([0.0656174, 0.0698824, 0.0728392, 0.077833, 0.0848284, 0.101601, 0.122454, 0.184193, 0.241927, 0.361739, 0.512375, 0.77357, 1.39607, 2.70304, 5.16886, 10.0435, 19.8829, 39.1673, 76.4265])
CUDA_sim2 = np.array([1.05054, 1.03457, 1.05291, 1.04122, 1.05427, 1.03218, 1.04385, 1.04146, 1.03985, 1.04819, 1.04169, 1.02809, 1.21661, 1.58623, 2.81732, 5.33792, 9.7876, 18.8868, 36.7903])

lapserial = np.array([0.0148888, 0.0188723, 0.0210505, 0.0272661, 0.0401502, 0.0691162, 0.12393, 0.235451, 0.473513, 0.948569, 1.80201, 3.59113, 7.36353, 14.4014, 31.03, 58.0864])
lapopenMP = np.array([0.0748705, 0.0789693, 0.0798722, 0.0853466, 0.0955755, 0.106817, 0.12226, 0.160776, 0.259824, 0.398125, 0.709251, 1.37191, 2.66119, 5.08099, 10.2737, 20.6611, 41.2846, 80.6653])

average_ratio = 0
for i in range(10):
    average_ratio += openMP_sim2[i+1]/openMP_sim2[i]


fig, axs = plt.subplots(num = 'error')
axs.set_xlabel('Size of grid')
axs.set_ylabel('Quadratic error')
axs.set_yscale('log')
axs.set_xscale('log')

axs.plot(N, err, '-', label = "DG with RK2")
#axs.plot(N, err_n1, '-', label = "DG, RK2, GQ n=1")
axs.plot(N[0:-3], err_RK0, '-', label  = "DG with Euler")
axs.plot(N, 1/N, ls='dotted', label = r'$\frac{1}{N}$')
axs.plot(N, 1/N**2, ls = 'dotted', label = r'$\frac{1}{N^2}$')
#axs.plot(N, 1/N**3, label = r'$\frac{1}{N^3}$')
axs.plot(N_upwind, err_upwind_CFL1, ls = 'dashdot', label = 'first order Upwind')
axs.legend(fontsize = 5)

fig, axs = plt.subplots(num = 'Copy time')
axs.set_xlabel('Array size')
axs.set_ylabel('Time to copy (seconds)')
axs.set_yscale('log')
axs.set_xscale('log')
axs.set_title('Time to copy between two views')

axs.plot(N_kokkos, cuda_par_cop, '-', label='parallel copy with CUDA')
axs.plot(N_kokkos, cuda_deep_cop, ls='dotted', label='deep_copy() with CUDA')
axs.plot(N_kokkos, openmp_par_cop, '-', label='parallel copy with OpenMP')
axs.plot(N_kokkos, openmp_deep_cop, ls='dotted', label='deep_copy() with OpenMP')
#axs.plot(N_kokkos[0:-1], serial_par_cop, ls='dashed', label = 'parallel copy with Serial')
#axs.plot(N_kokkos[0:-1], serial_deep_cop, ls='dashed', label = 'deep_copy() with Serial')
#axs.plot(N_kokkos, N_kokkos/1e9, ls='dashdot', label = 'y=x/1e9')

axs.set_ylim(1e-6)


axs.legend(fontsize = 6)

fig, axs = plt.subplots(num = 'Simulation time')
axs.set_xlabel('Resolution')
axs.set_ylabel('Simulation Time')
axs.set_yscale('log')
axs.set_xscale('log')

axs.plot(N_sim_2[0:-3], serial_sim2, ls='dashdot', label = 'Serial')
axs.plot(N_sim_2, openMP_sim2, ls='dotted', label = 'OpenMP')
axs.plot(N_sim_2, CUDA_sim2, ls='-', label = 'CUDA')
#axs.plot(N_sim_2, N_sim_2/100, ls = 'dashdot', label = 'y=x')
#axs.plot(N_sim_2, (N_sim_2**(1/1.3))/100, ls = 'dashdot', label = 'y=x^1/1.3')
#axs.plot(N_sim_2[0:-3], lapserial, ls='dashdot', label = 'i5-11300H Serial')
#axs.plot(N_sim_2[0:-1], lapopenMP, ls='dotted', label = 'i5-11300H OpenMP')
#axs.plot(N_sim_2[0:-4], cpp_sim, ls = 'dashdot', label = 'C++ code')

axs.legend()

fig, axs = plt.subplots(num = 'Simulation time 2')
axs.set_xlabel('Array size')
axs.set_ylabel('Time to simulate 2000 time steps (seconds)')
axs.set_yscale('log')
axs.set_xscale('log')

axs.plot(N_sim_2[0:-3], serial_sim2, ls='dashdot', label = 'i7-9700K Serial')
axs.plot(N_sim_2, openMP_sim2, ls='dotted', label = 'i7-9700K OpenMP')
axs.plot(N_sim_2, CUDA_sim2, ls='-', label = 'Serial backend and CUDA device')
#axs.plot(N_sim_2, N_sim_2/100, ls = 'dashdot', label = 'y=x')
#axs.plot(N_sim_2, (N_sim_2**(1/1.3))/100, ls = 'dashdot', label = 'y=x^1/1.3')
axs.plot(N_sim_2[0:-3], lapserial, ls='dashdot', label = 'i5-11300H Serial')
axs.plot(N_sim_2[0:-1], lapopenMP, ls='dotted', label = 'i5-11300H OpenMP')
axs.plot(N_sim_2[0:-4], cpp_sim, ls = 'dashdot', label = 'i7-9700K C++ code')

axs.legend(fontsize=5)

# =============================================================================
# fig, axs = plt.subplots(num = 'Simulation time normalised')
# axs.set_xlabel('Array size')
# axs.set_ylabel('Time to simulate 2000 time steps (seconds)')
# axs.set_yscale('log')
# axs.set_xscale('log')
# 
# axs.plot(N_sim_2[0:-3], serial_sim2/N_sim_2[0:-3], ls='dashdot', label = 'i7-9700K Serial')
# axs.plot(N_sim_2, openMP_sim2/N_sim_2, ls='dotted', label = 'i7-9700K OpenMP')
# axs.plot(N_sim_2, CUDA_sim2/N_sim_2, ls='-', label = 'Serial backend and CUDA device')
# #axs.plot(N_sim_2, N_sim_2/100, ls = 'dashdot', label = 'y=x')
# #axs.plot(N_sim_2, (N_sim_2**(1/1.3))/100, ls = 'dashdot', label = 'y=x^1/1.3')
# axs.plot(N_sim_2[0:-3], lapserial/N_sim_2[0:-3], ls='dashdot', label = 'i5-11300H Serial')
# axs.plot(N_sim_2[0:-1], lapopenMP/N_sim_2[0:-1], ls='dotted', label = 'i5-11300H OpenMP')
# 
# axs.legend(fontsize = 6)
# =============================================================================

