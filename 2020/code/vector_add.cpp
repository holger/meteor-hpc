#include <iostream>
#include <vector>
#include <CL/sycl.hpp>

class vector_addition;

int main(int, char**) {

  std::vector<float> a = { 1.0, 2.0, 3.0, 4.0 };
  std::vector<float> b = { 4.0, 3.0, 2.0, 1.0 };
  std::vector<float> c = { 0.0, 0.0, 0.0, 0.0 };

  // CPU - implementation
  
  for(int i=0;i<4;i++)
    c[i] = a[i] + b[i];

  for(int i=0;i<4;i++) {
    std::cout << i << "\t" << c[i] << std::endl;
  }

  // GPU - implementation
  
  c = { 0.0, 0.0, 0.0, 0.0 };
  cl::sycl::cpu_selector device_selector;

  cl::sycl::queue queue(device_selector);
  std::cout << "Running on "
	    << queue.get_device().get_info<cl::sycl::info::device::name>()
	    << "\n";
  
  //  {
    cl::sycl::buffer<float, 1> a_sycl(a.data(), cl::sycl::range<1>(4));
    cl::sycl::buffer<float, 1> b_sycl(b.data(), cl::sycl::range<1>(4));
    cl::sycl::buffer<float, 1> c_sycl(c.data(), cl::sycl::range<1>(4));
  
    queue.submit( [&] (cl::sycl::handler& cgh) {
	auto a_acc = a_sycl.get_access<cl::sycl::access::mode::read>(cgh);
	auto b_acc = b_sycl.get_access<cl::sycl::access::mode::read>(cgh);
	auto c_acc = c_sycl.get_access<cl::sycl::access::mode::discard_write>(cgh);

	cgh.parallel_for<class ex1>(sycl::range<1>(4),[=](sycl::id<1> idx) {
	    c_acc[idx] = a_acc[idx] + b_acc[idx];
	  });
      });

    c_sycl.get_access<sycl::access::mode::read>();
    //  }

  for(int i=0;i<4;i++) {
    std::cout << i << "\t" << c[i] << std::endl;
  }

  return 0;
}
