#include <iostream>
#include <cstdlib>

#include "particle.hpp"

void force(double& vx, double& vy)
{
  //...
}

double energy(double vy, double vy)
{
  return 0.5(vx*vx + vy*vy);
}

void move(double& x, double& y, double vx, double vy)
{
  //  ...
}

int main()
{
  int iter = 1000;
  
  // imperative programming
  double x = 0;
  double y = 0;
  double vx = 0;
  double vy = 0;

  for(int i=0; i<iter; i++) {
    force(vx, vy);
    move(x, y, vx, vy);
  }

  // object-oriented programming
  
  Particle particle0(0,0);

  std::cout << particle0.energy() << std::endl;
  

  for(int i=0; i<iter; i++)
    particle0.force();    

  std::cout << particle0.energy() << std::endl;

  // // What we should be able to do (<> : template)
  // // vector of 1000 particles.
  // Vector<Particle> particleVector(1000);

  // // energy of the first particle
  // particleVector[0].energy(); 
}
