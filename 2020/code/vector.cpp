#include <iostream>

//#include "particle.hpp"

template<typename Type>
class Vector
{
public:
  Vector(int size) {
    size_ = size;
    data_ = new Type[size];
  }

  Type& at(int i) {
    if(i<0 || i>=size_) {
      std::cout << "ERROR\n";
      exit(0);
    }
      
    return data_[i];
  }
  
private:
  Type* data_;
    int size_;
};

int main()
{
  // int n = 10;
  // double* data = new double[n];
  // for(int i=0; i<n; i++) {
  //   data[i] = i;
  // }
    
  // for(int i=0; i<n; i++) {
  //   std::cout << i << "\t" << data[i] << std::endl;
  // }
  
  // std::cout << "data = " << data << std::endl;
  // std::cout << "&data = " << &data << std::endl;
  // std::cout << "*data = " << *data << std::endl;

  // double a = 1;
  // std::cout << "a = " << a << std::endl;
  // std::cout << "&a = " << &a << std::endl;
  
  Vector<double> vec(100);

  // for(int i=0; i<100; i++)
  //   vec[i] = i;

  // vec[0] = 0.;
  // vec[100] = 0.;

  vec.at(100) = 0.;

  // // list of convinient operations:
  // vec[0] = 0; // operator overloading
  // Vector<double> vec2(vec);  // copy-constructor
  // vec2 = vec; // assignment operator
  // // destructor
  // std::cout << vec << std::endl; // <<operator (streaming operator)
  
  
}
