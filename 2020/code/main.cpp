#include <iostream>
#include <cassert>

//#include "particle.hpp"

template<typename Type>
class Vector
{
public:
  //constructor:
  Vector(int size) {
    size_ = size;
    data_ = new Type[size];
  }

  ~Vector() {
    delete [] data_;
  }

  //copy construcor:
  Vector(const Vector &vec){
      size_=vec.size_;
      data_ = new Type[vec.size_]; //allocating new memory space for copy vector
      for(int i=0;i<vec.size_;i++){
        data_[i] = vec.data_[i];
      }
  }

  Vector& operator=(const Vector &vec) {
    assert(size_ == vec.size_);
    
    for(int i=0;i<vec.size_;i++){
      data_[i] = vec.data_[i];
    }

    return *this;
  }

  //operator overloadings:

  // [] operator overload
   Type& operator[](int index){ //returns pointer to index element of vector and does bound checking.

     assert(index >= 0 && index < size_);

     return data_[index];
     
    // try{
    //   if(index<0 || index>=size_){
    //     throw "RUNTIME ERROR: ATTEMPTED TO ACCESS ELEMENT IN VECTOR OUTSIDE OF BOUNDS";
    //   } else{
    //         return data_[index];
    //   }
    // } catch (const char* err) {
    //     std::cerr << err << std::endl;
    //     exit(1);
    //   }
    }

    // << operator overload
    friend std::ostream& operator<<(std::ostream& out, const Vector& vec){
    //prints out values in array
        out<<"[";
        for(int i=0;i<vec.size_-2;i++){
            out<<vec.data_[i]<<",";
        }
        out<<vec.data_[vec.size_-1]<<"]";
        return out;
    }

private:
  Type* data_;
  int size_;
};

int main(){

  int size = 20;
  Vector<double> vec(size);

  for(int i=0;i<size;i++){
    vec[i]=1;
  }
  
  Vector<double> vec2(vec); // copy. vec2(vec) also works
  Vector<double> vec3(vec);
  
  for(int i=0;i<size;i++){
    vec[i]=i;
  }

  vec3 = vec2 = vec;

  std::cout<<"vec: "<<vec<<std::endl;
  std::cout<<"vec2: "<<vec2<<std::endl;

  return 0;
}

// Homework: write advection code

// 1) Use Vector to initialize data with sin function
// 2) Plot data to verify initial data (with python or gnuplot or xmgrace or...)
// 3) write temporal loop (for(int t=0; t<nt; t++) {...}) for upwind scheme
// 4) perform one revolution
// 5) Plot resulting data


