class Particle
{
public:

  // default constructor
  Particle() {
    x_ = double(std::rand())/RAND_MAX;
    y_ = double(std::rand())/RAND_MAX;
    
    vx_ = 0;
    vy_ = 0;    
   }

  // constructor with 2 arguments
  Particle(double x0, double y0) {
    x_ = x0;
    y_ = y0;
    
    vx_ = 0;
    vy_ = 0;    
  }
  
  // member function
  void force() {
    double randx = double(std::rand())/RAND_MAX;
    double randy = double(std::rand())/RAND_MAX;
    
    vx_ += randx;
    vy_ += randy;
  }

  void move() {}
  
  double energy() {
    return 0.5*(vx_*vx_ + vy_*vy_);
  }

private:
  // member
  double x_;
  double y_;

  double vx_;
  double vy_;
};
