#include <iostream>
#include <fstream>
#include <cassert>
#include <cmath>
#include <CL/sycl.hpp>

template<typename accessor1D, typename accessor2D>
void write_out(int nx, int ny, std::ofstream& file, double t, accessor1D &x, accessor1D &y, accessor2D &un){
  file<<"#t = "<<t<<std::endl;
  for(int ix=1;ix<nx+1;ix++){
    for(int iy=1;iy<ny+1;iy++){
      file<<x[ix]<<"\t"<<y[iy]<<"\t"<<un[ix][iy]<<std::endl;      
    }
  }
}

int main(int argc, char* argv[]){

  int nx,ny; //Please keep nx and ny at same size
  double Stime; //simulation time in number of revolutions
  try{
    nx = std::stoi(argv[1]);
    ny = std::stoi(argv[2]);
    Stime = std::stod(argv[3]); //simulation time in number of revolutions
  }
  catch (const std::exception &exc){
    std::cerr<<"ERROR: Please provide <nx ny Stime> when calling executable"<<std::endl;
    exit(1);
  }

  cl::sycl::default_selector device_selector;

  //printing out work device:
  cl::sycl::queue queue(device_selector);
  std::cout << "Running on "<< queue.get_device().get_info<cl::sycl::info::device::name>()<< "\n";

  double vx = 1.;
  double vy = 1.;
  int snapshot = 1; //output values each 'snapshot' iterations
  std::ofstream file;
  file.open("sol.txt");
  double xmin = -5.; //self-explanatory
  double xmax = 5.;
  double ymin = -5.;
  double ymax = 5.;
  double sizex = (xmax-xmin)*.1;
  double sizey = (xmax-xmin)*.1;
  double dx = (xmax-xmin)/nx; //spatial grid step size
  double dy = (ymax-ymin)/ny;
  double t = 0.; //time
  double dt = .5/std::max({fabs(vx/dx),fabs(vy/dy)});
  double CFL = std::max({fabs(vx*dt/dx),fabs(vy*dt/dy)});
  double CFLx = vx*dt/dx; 
  double CFLy = vy*dt/dy;

  double invsigmx = .5/(sizex*sizex); //for gaussian initial condition
  double invsigmy = .5/(sizey*sizey);
  // int it = 0; //for time loop


  { //SYCL WORKSPACE
    cl::sycl::queue MQ; //my queue

    cl::sycl::buffer<double, 2> un_buff(cl::sycl::range<2>(nx+2,ny+2)); //un buffer
    cl::sycl::buffer<double, 2> un1_buff(cl::sycl::range<2>(nx+2,ny+2)); //un1 buffer
    cl::sycl::buffer<double, 1> x_buff(cl::sycl::range<1>(nx+2)); //x buffer
    cl::sycl::buffer<double, 1> y_buff(cl::sycl::range<1>(ny+2)); //y buffer

    // CREATE X GRID
    MQ.submit([&](cl::sycl::handler & cgh){ 
      //requesting access to buffer data:
      auto x_acc = x_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

      cgh.parallel_for<class initial_cond_x>(sycl::range<1>(nx),[=](sycl::id<1> idx) { 
        //DOI begins at ix=1 and ends at ix=nx+1
        int ix = idx+1;
        x_acc[ix] = xmin+(ix-1)*dx;
      }); //end parallel for
    }); //end command queue for X grid

    // CREATE Y GRID
    MQ.submit([&](cl::sycl::handler & cgh){ 
      //requesting access to buffer data:
      auto y_acc = y_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

      cgh.parallel_for<class initial_cond_y>(sycl::range<1>(ny),[=](sycl::id<1> idy) {
        //DOI begins at iy=1 and ends at iy=ny+1 
        int iy = idy+1;
        y_acc[iy] = ymin+(iy-1)*dy;
      }); //end parallel for
    }); //end command queue for Y grid

    // ASSIGN INITIAL CONDITION TO MESHGRID
    MQ.submit([&](cl::sycl::handler & cgh){ 
      //requesting access to buffer data:
      auto un_acc = un_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);
      auto x_acc = x_buff.get_access<cl::sycl::access::mode::read>(cgh);
      auto y_acc = y_buff.get_access<cl::sycl::access::mode::read>(cgh);

      cgh.parallel_for<class initial_cond_un>(sycl::range<2>(nx,ny),[=](sycl::id<2> id_xy) { 
        int ix = id_xy[0]+1;
        int iy = id_xy[1]+1;
        double argx = invsigmx*x_acc[ix]*x_acc[ix];
        double argy = invsigmy*y_acc[iy]*y_acc[iy];
        un_acc[id_xy] = cl::sycl::exp(-(argx+argy));
      }); //end parallel for
    }); //end command queue for un IC assignement
    
    //SETTING BOUNDARY CONDITIONS
    MQ.submit([&](cl::sycl::handler & cgh){ 
      auto un_acc = un_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

      cgh.parallel_for<class boundary_conditions>(sycl::range<2>(nx,ny),[=](sycl::id<2> id_xy) { 
        int ix = id_xy[0]+1;
        int iy = id_xy[1]+1;
        un_acc[0][iy] = un_acc[nx-2][iy];
        un_acc[nx-1][iy] = un_acc[1][iy];
        un_acc[ix][0] = un_acc[ix][ny-2];
        un_acc[ix][ny-1] = un_acc[ix][1];
      }); //end parallel for
    }); //end command queue

    //SETTING BOUNDARY CONDITIONS FOR CORNER CELLS
    MQ.submit([&](cl::sycl::handler & cgh){ 
      auto un_acc = un_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

      cgh.single_task<class boundary_conditions>([=](){
        un_acc[0][0] = un_acc[nx-2][ny-2]; //top left corner
        un_acc[0][ny-1] = un_acc[nx-2][1]; //top right corner
        un_acc[nx-1][0] = un_acc[1][ny-2]; //bottom left corner
        un_acc[nx-1][ny-1] = un_acc[1][1]; //bottom right corner        
      }); //end of task 
    }); //end command queue

    //CYCLE ARRAY VALUES
    MQ.submit([&](cl::sycl::handler & cgh){ 
      auto un_acc = un_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);
      auto un1_acc = un1_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

      cgh.parallel_for<class set_un1_to_un>(sycl::range<2>(nx+2,ny+2),[=](sycl::id<2> id_xy) { 
        un1_acc[id_xy] = un_acc[id_xy];
      }); //end parallel for
    }); //end command queue

    //WRITTING INITIAL CONDTION
    // auto un_acc = un_buff.get_access<cl::sycl::access::mode::read>(); 
    // auto x_acc = x_buff.get_access<cl::sycl::access::mode::read>();
    // auto y_acc = y_buff.get_access<cl::sycl::access::mode::read>();
    // write_out(nx, ny, file, t, x_acc, y_acc, un_acc);

    clock_t start,end; //measuring time required to complete for loop
    start = clock(); 
    //TIME LOOP
    while(t<Stime){
      t+=dt;
      // std::cout<<t<<std::endl;

      //CYCLE ARRAY VALUES
      MQ.submit([&](cl::sycl::handler & cgh){ 
        auto un_acc = un_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);
        auto un1_acc = un1_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

        cgh.parallel_for<class set_un_to_un1>(sycl::range<2>(nx+2,ny+2),[=](sycl::id<2> id_xy) { 
          un_acc[id_xy] = un1_acc[id_xy];
        }); //end parallel for
      }); //end command queue

      // APPLY SCHEME
      MQ.submit([&](cl::sycl::handler & cgh){ 
        auto un1_acc = un1_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);
        auto un_acc = un_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

        cgh.parallel_for<class initial_cond_un>(sycl::range<2>(nx,ny),[=](sycl::id<2> id_xy) { 
          int ix = id_xy[0]+1;
          int iy = id_xy[1]+1;
          un1_acc[ix][iy] = un_acc[ix][iy] - CFLx*(un_acc[ix][iy]-un_acc[ix-1][iy]) - CFLy*(un_acc[ix][iy]-un_acc[ix][iy-1])
          + .5*dt*dt*(vx/dx * (vy/dy *(un_acc[ix][iy]-un_acc[ix][iy-1]-un_acc[ix-1][iy]+un_acc[ix-1][iy-1])) 
          + vy/dy * (vx/dx * (un_acc[ix][iy]-un_acc[ix-1][iy]-un_acc[ix][iy-1]+un_acc[ix-1][iy-1])));
        }); //end parallel for
      }); //end command queue for scheme

      //SETTING BOUNDARY CONDITIONS
      MQ.submit([&](cl::sycl::handler & cgh){ 
        auto un1_acc = un1_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

        cgh.parallel_for<class boundary_conditions>(sycl::range<2>(nx,ny),[=](sycl::id<2> id_xy) { 
          int ix = id_xy[0]+1;
          int iy = id_xy[1]+1;
          un1_acc[0][iy] = un1_acc[nx-2][iy];
          un1_acc[nx-1][iy] = un1_acc[1][iy];
          un1_acc[ix][0] = un1_acc[ix][ny-2];
          un1_acc[ix][ny-1] = un1_acc[ix][1];
        }); //end parallel for
      }); //end command queue

      //SETTING BOUNDARY CONDITIONS FOR CORNER CELLS
      MQ.submit([&](cl::sycl::handler & cgh){ 
        auto un1_acc = un1_buff.get_access<cl::sycl::access::mode::discard_write>(cgh);

        cgh.single_task<class boundary_conditions>([=](){
          un1_acc[0][0] = un1_acc[nx-2][ny-2]; //top left corner
          un1_acc[0][ny-1] = un1_acc[nx-2][1]; //top right corner
          un1_acc[nx-1][0] = un1_acc[1][ny-2]; //bottom left corner
          un1_acc[nx-1][ny-1] = un1_acc[1][1]; //bottom right corner        
        }); //end of task 
      }); //end command queue
    } //END TIME LOOP

    end = clock(); 
    double time_taken = double(end - start) / double(CLOCKS_PER_SEC); 
    std::cout<<"Time loop execution time: "<<time_taken<<" seconds"<<std::endl;

    //write final data to file
    auto un_acc = un_buff.get_access<cl::sycl::access::mode::read>(); 
    auto x_acc = x_buff.get_access<cl::sycl::access::mode::read>();
    auto y_acc = y_buff.get_access<cl::sycl::access::mode::read>();
    write_out(nx, ny, file, t, x_acc, y_acc, un_acc);

  } //END SYCL WORKSPACE

  // file.close();
  return 0;
}