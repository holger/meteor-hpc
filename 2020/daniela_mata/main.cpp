// g++ -o mainSimple mainSimple.cpp

#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <vector>
#include <array>
#include <algorithm>
#include <functional>
#include <numeric>

using namespace std;

//declare variables global
double L = 2*M_PI, Ncell = 20., dx = L/Ncell;
double cfl = 0.1;
double N = 3; // degree of polynomial
double a = 1;
double dt = dx*cfl/a;
double t = 0, t_final = 2*M_PI; // physical time
int nt = static_cast<int> (round(t_final/dt)); // number of time steps.
int nx_min = 2.;
int nx_max = Ncell;

inline double initFunction(double x) // initial function
{
  return sin(x);
}

inline double dLagrange(int k, int N, double x) // get derivatives of lagrange polynomial
{
  iszero(k < N);
  static std::vector<double> xi = {-1.,0.,1.};
  double value = 0;
  for(int term = 0; term < N; term++) {
    if(term == k) continue;
    double valueTerm = 1./(xi[k] - xi[term]);

    for(int i = 0; i<N; i++) {
      if(i == k) continue;
      if(i == term) continue;
      valueTerm *= (x - xi[i])/(xi[k] - xi[i]); }
    value += valueTerm;
  }
  return value;
}

inline std::vector<std::array<double,3>> adding(const std::vector<std::array<double,3>>& a, const std::vector<std::array<double,3>>& b) // adding vectors
{
  std::vector<std::array<double,3>> result1(a.size());
  for(int i=0; i<a.size(); i++)
    for(int j=0; j<3; j++) {
      result1[i][j] = a[i][j] + b[i][j] ;
    }
  return result1;
}

inline std::vector<std::array<double,3>> multiplying(const double& c, const std::vector<std::array<double,3>>& d) // multiplying vector by a scalar
{
  std::vector<std::array<double,3>> result2(d.size());
  for(int i=0; i<d.size(); i++)
    for(int j=0; j<3; j++) {
      result2[i][j] = c * d[i][j] ;
    }
  return result2;
}

inline double representation(double x, std::array<double,3> values) // representation of solution profile inside cells
{
  return 0.5*x*(x-1)*values[0] + (1-x*x)*values[1] + 0.5*x*(x+1)*values[2];
}

// Core of the solution- discretised equations with b.c and fluxes
inline std::vector<std::array<double,3>> RHS(const std::vector<std::array<double,3>> &cellVec) {

  std::vector<std::array<double,3>> rhs(cellVec.size());
  std::vector<std::array<double,3>> dLcellVec0(cellVec.size());
  std::vector<std::array<double,3>> dLcellVec1(cellVec.size());
  std::vector<std::array<double,3>> dLcellVec2(cellVec.size());

  for(int i=0; i<cellVec.size(); i++) {
    dLcellVec0[i][0] = a*cellVec[i][0] * dLagrange( 0, 3, -1);
    dLcellVec0[i][1] = a*cellVec[i][1] * dLagrange( 0, 3, 0);
    dLcellVec0[i][2] = a*cellVec[i][2] * dLagrange( 0, 3, 1);

    dLcellVec1[i][0] = a*cellVec[i][0] * dLagrange( 1, 3, -1);
    dLcellVec1[i][1] = a*cellVec[i][1] * dLagrange( 1, 3, 0);
    dLcellVec1[i][2] = a*cellVec[i][2] * dLagrange( 1, 3, 1);

    dLcellVec2[i][0] = a*cellVec[i][0] * dLagrange( 2, 3, -1);
    dLcellVec2[i][1] = a*cellVec[i][1] * dLagrange( 2, 3, 0);
    dLcellVec2[i][2] = a*cellVec[i][2] * dLagrange( 2, 3, 1);
  }

  std::vector<double> fluxVecL(cellVec.size());
  std::vector<double> fluxVecR(cellVec.size());

  std::vector<double> fluxVecRM(cellVec.size());// Use these to build different kinds of fluxes
  std::vector<double> fluxVecLP(cellVec.size());
  std::vector<double> fluxVecRP(cellVec.size());
  std::vector<double> fluxVecLM(cellVec.size());

  for(int i=1; i<cellVec.size()-1; i++) {

    fluxVecLM[i] = a*cellVec[i-1][2];
    fluxVecLP[i] = a*cellVec[i][0];

    fluxVecRM[i] = a*cellVec[i][2];
    fluxVecRP[i] = a*cellVec[i+1][0];

    // upwinding
    fluxVecL[i] = fluxVecLM[i];
    fluxVecR[i] = fluxVecRM[i];
  }

  // zeroth and end cell values with B.C's
  // upwinding
  fluxVecR[0] = a*cellVec[0][2];
  fluxVecL[cellVec.size()-1] = a*cellVec[cellVec.size()-2][2];
  fluxVecR[cellVec.size()-1] = a*cellVec[cellVec.size()-1][2];
  fluxVecL[0] = fluxVecR[cellVec.size()-1];

  for(int i=0; i<cellVec.size(); i++) {
    rhs[i][0] = 2./dx*3.0 * ( dLcellVec0[i][0]* 1./3. + dLcellVec0[i][1]* 4./3. + dLcellVec0[i][2]* 1./3. + fluxVecL[i]);
    rhs[i][1] = 2./dx*3./4. * ( dLcellVec1[i][0]* 1./3. + dLcellVec1[i][1]* 4./3. + dLcellVec1[i][2]* 1./3.);
    rhs[i][2] = 2./dx*3.0 * ( dLcellVec2[i][0]* 1./3. + dLcellVec2[i][1]* 4./3. + dLcellVec2[i][2]* 1./3. - fluxVecR[i]);
  }
  return rhs;
}
//error calcs
/**
inline double l2_err(int Ncell, const std::vector<std::array<double,3>>& cellVec) {
  std::vector<std::array<double,3>> Init(cellVec.size());

  for(int i=0; i<Ncell; i++) {
    Init[i][0] = initFunction(i*dx);
    Init[i][1] = initFunction((2.*i+1)/2.*dx);
    Init[i][2] = initFunction((i+1)*dx);
  }

  std::vector<std::array<double,3>> temp(cellVec.size());
  for(int i=0; i<cellVec.size(); i++)
    for(int j=0; j<3; j++) {
      temp[i][j] = (double)pow((Init[i][j]-cellVec[i][j]),2)*dx;
    }

  double sum_of_elems = 0;
  for(int i=0; i<cellVec.size(); i++)
    for(int j=0; j<3; j++) {
      sum_of_elems += temp[i][j];
    }

  return sqrt(sum_of_elems);
} **/

inline double L2_Error(int nx){

    double pie = M_PI;
    double L = 2*pie;
    double dx = L/nx;
    double CFL = 0.1;
    double N = 3.;          // degree of polynomial
    double a = 1.;
    double dt = dx*CFL/a;
    double t = 0.;
    double t_fin = 2.*pie;  // Physical time
    int nt = static_cast<int> (round(t_fin/dt));  // num of time steps
    double err;

    std::vector<std::array<double,3>> cellVec(nx);
    /** 1 cell/elem of vector (1 cell = 3 nodes, end nodes inclusive)**/
    std::vector<std::array<double,3>> xVec(nx);
    /** vector for x coordinates **/

    for(int i=0; i<nx; i++){
        /** filling the vector of arrays with x coordinates
        and the sine function values **/
        xVec[i][0] = i*dx;
        xVec[i][1] = (2.*i+1)/2.*dx;
        xVec[i][2] = (i+1)*dx;

        cellVec[i][0] = initFunction(i*dx);
        cellVec[i][1] = initFunction((2.*i+1)/2.*dx);
        cellVec[i][2] = initFunction((i+1)*dx);
    }


    // Runge-Kutta to the order 4
    for(int step = 0; step<nt; step++){

        std::vector<std::array<double,3>> k1 = RHS(cellVec);
        std::vector<std::array<double,3>> k2 = RHS(adding(cellVec,
                                multiplying((dt*0.5),k1)));
        std::vector<std::array<double,3>> k3 = RHS(adding(cellVec,
                                multiplying((dt*0.5),k2)));
        std::vector<std::array<double,3>> k4 = RHS(adding(cellVec,
                                multiplying(dt,k3)));

        for(int i=0; i<nx; i++){
            for(int j=0; j<3; j++){
                cellVec[i][j] = cellVec[i][j] + ((k1[i][j] +
                        2.*k2[i][j] + 2.*k3[i][j] + k4[i][j])*
                        dt / 6.0);
            }
        }

        if(step==(nt-1)){
            err = L2_Error(nx);
        }
    }
    return err;

}

int main()
{

    int nx_min = 8;
    int nx_max = 200;
    int step = 1;
    double err;
    std::ofstream file;
    file.open("err.txt");


//  std::vector<std::array<double,3>> cellVec(Ncell); // 1 cell/element of vector **1 cell = 3 nodes,end nodes inclusive**
//  std::vector<std::array<double,3>> xVec(Ncell); // vector for x coordinates
//
//  for(int i=0; i<Ncell; i++) { //filling the vector of arrays with x coords and init_function values
//    xVec[i][0] = i*dx;
//    xVec[i][1] = (2.*i+1)/2.*dx;
//    xVec[i][2] = (i+1)*dx;
//    cellVec[i][0] = initFunction(i*dx);
//    cellVec[i][1] = initFunction((2.*i+1)/2.*dx);
//    cellVec[i][2] = initFunction((i+1)*dx);
//  }

  //Start of RK4
  //for (int step = 0; step < nt; step++) {
/**
    if (step == 0)
      {
	// std::string filename = "simp_solution_" + std::to_string(Ncell) + "_" + std::to_string(step) + ".data";
	// std::ofstream out(filename); // printing solution to a file
	// for(int i=0; i<Ncell; i++)
	//   for(int j=0; j<3; j++) {
	//     out << xVec[i][j] << "\t" << cellVec[i][j] << std::endl;
	//   }
	// out.close();

      } **/

    /*
    std::vector<std::array<double,3>> k1 = RHS(cellVec);
    std::vector<std::array<double,3>> k2 = RHS(adding (cellVec, multiplying((dt*0.5),k1)) );
    std::vector<std::array<double,3>> k3 = RHS(adding (cellVec, multiplying((dt*0.5),k2) ));
    std::vector<std::array<double,3>> k4 = RHS(adding (cellVec, multiplying(dt,k3)));

    for(int i=0; i<Ncell; i++)
      for(int j=0; j<3; j++) {
        cellVec[i][j] = cellVec[i][j]+ ((k1[i][j] + 2.0 * k2[i][j] + 2.0 * k3[i][j] + k4[i][j]) * dt / 6.0) ;
      }


    if ( step == (nt-1)){
      std::string filename = "simp_solution_" + std::to_string(Ncell) + "_" + std::to_string(step) + ".data";
      std::ofstream out(filename); // printing solution to a file
      for(int i=0; i<Ncell; i++)
	for(int j=0; j<3; j++) {
	  out << t << "\t" << xVec[i][j] << "\t" << cellVec[i][j] << std::endl;
	}
      out.close();

    }

    //ERROR CALCS
    /**
    for(int nx=nx_min; nx<=nx_max; nx++){
        dx = (xmax-xmin)/nx; //spatial
        L2 = upwind_error(nx,CFL,dx,sizee,v,Time,xmin,xmax);
        file<<nx<<"\t"<<L2<<std::endl;
    } **/
    /**
    if (step == (nt-1)){
      std::ofstream errout("error.data",std::ios_base::app);
//      for(int Ncell=nx_min; Ncell<= nx_max; Ncell++){
      errout<< Ncell << "\t" << l2_err(Ncell, cellVec) <<endl;
      errout.close();
      }
    } **/

    for(int nx=nx_min; nx<nx_max; nx+=step){
        err = L2_Error(nx);
        //std::cout<<nx<<std::endl;
        file<<nx<<"\t"<<err<<std::endl;
    }
  file.close();
  return 0;
}
