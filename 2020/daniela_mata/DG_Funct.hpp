#ifndef DG_FUNCT_HPP
#define DG_FUNCT_HPP

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <array>
#include <algorithm>

using namespace std;


void write_out(std::ofstream& file, double t,std::vector<std::array<double,3>> xVec, std::vector<std::array<double,3>> cellVec){
  int nx=cellVec.size();
  for(int ix=0;ix<nx;ix++){
        //for(int j=0;j<3;j++){
        file<<t<<"\t"<<xVec[ix][1]<<"\t"<<cellVec[ix][1]<<std::endl;
        //}
  }
}



double sinef(double x){
    return sin(x); // initial function of sine
}


double dL(int j, int Order, double x){
    iszero(j < Order);
    static std::vector<double> xi = {-1., 0., 1.};
    double value = 0.;
    for(int i = 0.; i<Order; i++){
        if(i == j) continue;
        double ValueTerm = 1./(xi[j] - xi[i]);

        for(int m = 0; m<Order;m++){
            if(m==j) continue;
            if(m==i) continue;
            ValueTerm *= (x-xi[m])/(xi[j]- xi[m]);
        }
        value += ValueTerm;
    }
    return value;
} //Langrange first derivative


std::vector<std::array<double,3>> add_vecs(const
        std::vector<std::array<double,3>> &vec1, const
        std::vector<std::array<double,3>> &vec2){
    std::vector<std::array<double,3>> finVec(vec1.size());
    for(int i=0; i<vec1.size(); i++){
        for(int j=0; j<3; j++){ //order number
            finVec[i][j] = vec1[i][j] + vec2[i][j];
        }
    }
    return finVec;
}


std::vector<std::array<double,3>> multi(const
        double &s, const std::vector<std::array<double,3>> &vec){
    std::vector<std::array<double,3>> finVec(vec.size());
    for(int i=0; i<vec.size(); i++){
        for(int j=0; j<3; j++){ //order number
            finVec[i][j] = s * vec[i][j];
        }
    }
    return finVec;
}


double representation(double x, std::array<double,3> vals){
    // represenation of solution profile inside cells
    double w_f;
    w_f = 0.5*x*(x-1)*vals[0] + (1-x*x)*vals[1]
                +0.5*x*(x+1)*vals[2];
    return w_f;
}


//Discretizion of equations with b.c and fluxes
std::vector<std::array<double,3>> RHS(const
        std::vector<std::array<double,3>> &cellVec){
    std::vector<std::array<double,3>> rhs(cellVec.size());
    std::vector<std::array<double,3>> dL_cellV0(cellVec.size());
    std::vector<std::array<double,3>> dL_cellV1(cellVec.size());
    std::vector<std::array<double,3>> dL_cellV2(cellVec.size());
    double a;
    double dx;

    for(int i=0; i<cellVec.size(); i++){
        // i=cell; (0,1,2)=points of Lagrange
        // a = 1
        // dL(int j, Order, point); j=index for summation of dL
        dL_cellV0[i][0] = a*cellVec[i][0] * dL(0,3,-1);
        dL_cellV1[i][1] = a*cellVec[i][1] * dL(0,3,0);
        dL_cellV2[i][2] = a*cellVec[i][2] * dL(0,3,1);

        dL_cellV0[i][0] = a*cellVec[i][0] * dL(1,3,-1);
        dL_cellV1[i][1] = a*cellVec[i][1] * dL(1,3,0);
        dL_cellV2[i][2] = a*cellVec[i][2] * dL(1,3,1);

        dL_cellV0[i][0] = a*cellVec[i][0] * dL(2,3,-1);
        dL_cellV1[i][1] = a*cellVec[i][1] * dL(2,3,0);
        dL_cellV2[i][2] = a*cellVec[i][2] * dL(2,3,1);
    }

    std::vector<double> Flux_L(cellVec.size());
    std::vector<double> Flux_R(cellVec.size());

    std::vector<double> Flux_LM(cellVec.size());
    std::vector<double> Flux_LP(cellVec.size());
    std::vector<double> Flux_RM(cellVec.size());
    std::vector<double> Flux_RP(cellVec.size());

    for(int i=1; i<cellVec.size()-1; i++){
        Flux_LM[i] = a*cellVec[i-1][2];
        Flux_LP[i] = a*cellVec[i][0];

        Flux_RM[i] = a*cellVec[i][2];
        Flux_RP[i] = a*cellVec[i+1][0];

        //UPWINDING SCHEME
        Flux_L[i] = Flux_LM[i];
        Flux_R[i] = Flux_RM[i];
    }

    //0th and Last cell values with b.c's
    //UPWINDING

    Flux_R[0] = a*cellVec[0][2];
    Flux_L[cellVec.size()-1] = a*cellVec[cellVec.size()-2][2];
    Flux_R[cellVec.size()-1] = a*cellVec[cellVec.size()-1][2];
    Flux_L[0] = Flux_R[cellVec.size()-1];

    for(int i=0; i<cellVec.size(); i++){
        rhs[i][0] = 2./dx*3.*(dL_cellV0[i][0]*1./3. +
                dL_cellV0[i][1]*4./3. + dL_cellV0[i][2]*
                1./3. + Flux_L[i]);
        rhs[i][1] = 2./dx*3./.4*(dL_cellV1[i][0]*1./3. +
                dL_cellV1[i][1]*4./3. + dL_cellV1[i][2]*1./3.);
        rhs[i][2] = 2./dx*3.*(dL_cellV2[i][0]*1./3. +
                dL_cellV2[i][1]*4./3. + dL_cellV2[i][2]*
                1./3. - Flux_R[i]);
    }
    return rhs;
}


// Calculating errors
double L2_Err(const std::vector<std::array<double,3>> &cellVec){
    std::vector<std::array<double,3>> Init(cellVec.size());
    double dx;

    for(int i=0; i<cellVec.size(); i++){
        Init[i][0] = sinef(i*dx);
        Init[i][1] = sinef((2.*i+1)/2.*dx);
        Init[i][2] = sinef((i+1)*dx);
    }

    std::vector<std::array<double,3>> temp(cellVec.size());
    for(int i=0; i<cellVec.size(); i++){
        for(int j=0; j<3; j++){
            temp[i][j] = (double)pow((Init[i][j]-
                        cellVec[i][j]),2)*dx;
        }
    }

    double sum_elems = 0;
    for(int i=0; i<cellVec.size(); i++){
        for(int j=0; j<3; j++){
            sum_elems += temp[i][j];
        }
    }
    return sqrt(sum_elems);
}


#endif // DG_FUNCT_HPP
