#!/sur/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import os
import sys 
from mauca import Data
from mpl_toolkits.mplot3d import Axes3D

plt.rc('font', **{'family' : 'serif', 'serif' : ['Computer Modern Roman']})
plt.rc('text', usetex = True)

def plot_frame(x,y,t,RHO,MOMENTX,MOMENTY):
    fig = plt.figure()
    #plt.imshow(RHO,vmin=0.,vmax=1.)
    #cb=plt.colorbar()
    #plt.axis('off')

    ax1 = fig.add_subplot(111,projection='3d')
    rh = ax1.plot_surface(x, y, RHO, rstride=1, cstride=1, edgecolor='none',cmap='viridis',vmin=0,vmax=1)
    ax1.view_init(50, 30)
    ax1.set_xlabel('x')
    ax1.set_ylabel('y')
    ax1.set_zlabel(r'$\rho_i^n$')
    ax1.set_zlim(0,1)
    fig.colorbar(rh)
    plt.title('2D Gas (Lax-Friedrichs)' + '\n time = {:f}'.format(t))


if __name__ == '__main__':
    fname = "/home/jmarquez/Bureau/Meteor4/Codes/2D_GAS_Fri_plus1.txt"
    movie = "GAS_2D_Fri_rhop1.avi"
    files =[]

# header
f = open('2D_GAS_Fri_plus1.txt',"r")
f.seek(0,0)
lines = f.readlines()
f.close()

title = lines[0].split()[2]
xmin = float(lines[1].split()[2])
xmax = float(lines[2].split()[2])
ymin = float(lines[3].split()[2])
ymax = float(lines[4].split()[2])
len_x = int(lines[6].split()[2])
len_y = int(lines[7].split()[2])
nt = int(lines[8].split()[2])
dx = float(lines[9].split()[2])
dy = float(lines[10].split()[2])
dt = lines[11].split()[2]


t = np.genfromtxt(fname,skip_header=12,usecols=0)
t = t.reshape((nt,len_x,len_y))
rho = np.genfromtxt(fname,skip_header=12,usecols=3)
rho = rho.reshape((nt,len_x,len_y))
momentx = np.genfromtxt(fname,skip_header=12,usecols=4)
momentx = momentx.reshape((nt,len_x,len_y))
momenty = np.genfromtxt(fname,skip_header=12,usecols=5)
momenty = momenty.reshape((nt,len_x,len_y))


x = np.zeros(len_x)
y = np.zeros(len_y)


for ii in range(len_x):
    x[ii] = xmin + ((ii-1)*dx + 0.5*dx)
for jj in range(len_y):
    y[jj] = ymin + ((jj-1)*dy + 0.5*dy)

X,Y = np.meshgrid(x,y)
    


for i in range(nt-1):
        print("\r progress : %.2f " % (i / nt * 100.) + '%', end = '')


        sys.stdout.flush()
        plot_frame(X,Y,t[i,0,0],rho[i],momentx[i]/rho[i],momenty[i]/rho[i])
        fname = '_tmp%04d.png' % i
        files.append(fname)
        plt.savefig(fname, dpi = 250)
        plt.cla()
        plt.clf()
    #os.system("avconv -y -f image2 -i _tmp%04d.png -b 8192k " + movie )
os.system("ffmpeg -v quiet -y -f image2 -i _tmp%04d.png -b:v 8192k " + movie )
for fname in files:
    os.remove(fname)

    plt.close()

