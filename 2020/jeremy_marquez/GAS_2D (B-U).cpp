#include <iostream>
#include <cassert>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <array>

#define quote(x) #x

//#include "particle.hpp"

template<typename Type>
class Vector{
public:
  //constructor:
  Vector(int size) {
    size_ = size;
    data_ = new Type[size];
  }

  ~Vector() {
   delete [] data_;
  }

  //copy construcor:
  Vector(const Vector &vec){
      size_=vec.size_;
      data_ = new Type[vec.size_]; //allocating new memory space for copy vector
      for(int i=0;i<vec.size_;i++){
        data_[i] = vec.data_[i];
      }
  }

  //equal operator:
  Vector& operator=(const Vector &vec) {
  assert(size_ == vec.size_);
        for(int i=0;i<vec.size_;i++){
        data_[i] = vec.data_[i];
      }
    return *this;
  }

  //operator overloadings:

  // [] operator overload
   Type& operator[](int index) const{ //returns pointer to index element of vector and does bound checking.avec l'étoile Type& return the value

    //assert(index >= 0 && index < size_);
    //return data_[index];

    try{
      if(index<0 || index>=size_){
        throw "RUNTIME ERROR: INDEX OUT OF BOUNDS.";
      } else{
            return data_[index];
      }
    } catch (const char* err) {
        std::cerr << err << std::endl;
        exit(1);
      }
    }

    // << operator overload
    friend std::ostream& operator<<(std::ostream& out,const Vector& vec){
    //prints out values in array
        out<<"[";
        for(int i=0;i<vec.size_-2;i++){
            out<<vec.data_[i]<<",";
        }
        out<<vec.data_[vec.size_-1]<<"]";
        return out;
    }

private:
  Type* data_; // reference in memory
    int size_;
};

template<typename Type>
class Matrix_2D{
public:

  //  constructor:  //
  Matrix_2D( int len_x, int len_y ) {
    size_x = len_x;
    size_y = len_y;
    data_ = new Type*[size_x];
    for(int ix=0;ix<size_x;ix++){
        data_[ix] = new Type[size_y];
    }
    //  initilization  //
    for(int ix=0;ix<size_x;ix++){
    for(int iy=0;iy<size_y;iy++){
    //data_[ix][iy] = 0.;
    }
    }
    }

  //  destructeur:  //
  ~Matrix_2D() { // ~ = destructeur
  for(int ix=0;ix<size_x;ix++){
    delete [] data_[ix];
  }
 delete [] data_; // size_x
}

  //  copy construcor:  //
  Matrix_2D(const Matrix_2D &mat){
      size_x=mat.size_x;
      size_y=mat.size_y;
      data_ = new Type*[mat.size_x]; //allocating new memory space for copy matrice
      for(int ix=0;ix<mat.size_;ix++){
        data_[ix] = new Type[mat.size_y];
        for(int iy=0;iy<mat.size_y;iy++){
            data_[ix][iy] = mat.data_[ix][iy];
        }
      }
  }


  //  equal operator:  //
  Matrix_2D& operator=(const Matrix_2D &mat) {
  assert(size_x == mat.size_x);
  assert(size_y == mat.size_y);
        for(int ix=0;ix<mat.size_x;ix++){
        for(int iy=0;iy<mat.size_y;iy++){
            data_[ix][iy] = mat.data_[ix][iy];
        }

      }
    return *this;
  }


  // () operator overload  //
  Type& operator()(int index,int indey) const{ //returns pointer to index element of vector and does bound checking.avec l'étoile Type& return the value
    try{
      if(index<0 || index>=size_x || indey<0 || indey >=size_y){
        throw "RUNTIME ERROR: INDEX OUT OF BOUNDS.";
      } else{
            return data_[index][indey];
      }
    } catch (const char* err) {
        std::cerr << err << std::endl;
        exit(1);
      }
    }

// () operator overload 1 lines  //
//  Type& operator()(int index) const{
//    try{
//      if(index<0 || index>=size_x){
//        throw "RUNTIME ERROR: INDEX OUT OF BOUNDS.";
//      } else{
//            return data_[index];
//      }
//    } catch (const char* err) {
//        std::cerr << err << std::endl;
//        exit(1);
//      }
//    }



  //  prints out values in table  //
  friend std::ostream& operator<<(std::ostream& out,const Matrix_2D& mat){
    out<<"[";
    for(int ix=0;ix<mat.size_x;ix++){
    out<<"[";
    for(int iy=0;iy<mat.size_y;iy++){
        if(iy==mat.size_y-1){
        out<<mat.data_[ix][iy];
        }
        else{
        out<<mat.data_[ix][iy]<<",";
        }
    }
    out<<"],";
    }
    out<<"]";
    return out;
}


private:
    Type** data_;// reference in memory
    int  size_x;
    int  size_y;

};

template<typename Type, typename Type_M>
void write(std::ofstream& file,double t, int nx, int ny, const Vector<Type>& x, const Vector<Type>& y, const Matrix_2D<Type_M>& Ui) {
  //file << "#time = " << t << std::endl;
  for(int ii = 1; ii <=nx; ++ii) {
      for(int jj = 1; jj <=ny; ++jj) {
    file << t <<"\t"<< x[ii] << "\t" << y[jj] << "\t" << Ui(ii,jj)[0] << "\t" << Ui(ii,jj)[1] << "\t" << Ui(ii,jj)[2] << std::endl;// add array of vn every where
  }
  }
}

template<typename Type>
double Ga_Fr_dt(double &dt, Matrix_2D<Type>& Ui ,double dx,double dy, double a,int len_x, int len_y){

double  eigX_L  =  std::max({fabs(Ui(1,1)[1]/Ui(1,1)[0]+a),fabs(Ui(1,1)[1]/Ui(1,1)[0]-a),fabs(Ui(1-1,1)[1]/Ui(1-1,1)[0]+a),fabs(Ui(1-1,1)[1]/Ui(1-1,1)[0]-a)});
double  eigX_R  =  std::max({fabs(Ui(1,1)[1]/Ui(1,1)[0]+a),fabs(Ui(1,1)[1]/Ui(1,1)[0]-a),fabs(Ui(1+1,1)[1]/Ui(1+1,1)[0]+a),fabs(Ui(1+1,1)[1]/Ui(1+1,1)[0]-a)});
double  eigY_L  =  std::max({fabs(Ui(1,1)[2]/Ui(1,1)[0]+a),fabs(Ui(1,1)[2]/Ui(1,1)[0]-a),fabs(Ui(1,1-1)[2]/Ui(1,1-1)[0]+a),fabs(Ui(1,1-1)[2]/Ui(1,1-1)[0]-a)});
double  eigY_R  =  std::max({fabs(Ui(1,1)[2]/Ui(1,1)[0]+a),fabs(Ui(1,1)[2]/Ui(1,1)[0]-a),fabs(Ui(1,1+1)[2]/Ui(1,1+1)[0]+a),fabs(Ui(1,1+1)[2]/Ui(1,1+1)[0]-a)});

double  rmax;
  rmax = std::max({eigX_L,eigX_R,eigY_L,eigY_R});

  for(int ix=2;ix<=len_x;ix++){
    for(int iy=2;iy<=len_y;iy++){


    double  eigX_L  =  std::max({fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]-a),fabs(Ui(ix-1,iy)[1]/Ui(ix-1,iy)[0]+a),fabs(Ui(ix-1,iy)[1]/Ui(ix-1,iy)[0]-a)});
    double  eigX_R  =  std::max({fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]-a),fabs(Ui(ix+1,iy)[1]/Ui(ix+1,iy)[0]+a),fabs(Ui(ix+1,iy)[1]/Ui(ix+1,iy)[0]-a)});
    double  eigY_L  =  std::max({fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]-a),fabs(Ui(ix,iy-1)[2]/Ui(ix,iy-1)[0]+a),fabs(Ui(ix,iy-1)[2]/Ui(ix,iy-1)[0]-a)});
    double  eigY_R  =  std::max({fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]-a),fabs(Ui(ix,iy+1)[2]/Ui(ix,iy+1)[0]+a),fabs(Ui(ix,iy+1)[2]/Ui(ix,iy+1)[0]-a)});

    rmax = std::max({eigX_L,eigX_R,eigY_L,eigY_R,rmax});
  }}
  dt = sqrt(dx*dx+dy*dy)/rmax;
}

template<typename Type>
std::array<double,4> Ga_Fr_ei(Matrix_2D<Type>& Ui ,int ix,int iy, double a){
double  eigX_L  =  std::max({fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]-a),fabs(Ui(ix-1,iy)[1]/Ui(ix-1,iy)[0]+a),fabs(Ui(ix-1,iy)[1]/Ui(ix-1,iy)[0]-a)});
double  eigX_R  =  std::max({fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[1]/Ui(ix,iy)[0]-a),fabs(Ui(ix+1,iy)[1]/Ui(ix+1,iy)[0]+a),fabs(Ui(ix+1,iy)[1]/Ui(ix+1,iy)[0]-a)});
double  eigY_L  =  std::max({fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]-a),fabs(Ui(ix,iy-1)[2]/Ui(ix,iy-1)[0]+a),fabs(Ui(ix,iy-1)[2]/Ui(ix,iy-1)[0]-a)});
double  eigY_R  =  std::max({fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]+a),fabs(Ui(ix,iy)[2]/Ui(ix,iy)[0]-a),fabs(Ui(ix,iy+1)[2]/Ui(ix,iy+1)[0]+a),fabs(Ui(ix,iy+1)[2]/Ui(ix,iy+1)[0]-a)});

std::array<double,4> ei;
ei[0] = eigX_L;
ei[1] = eigX_R;
ei[2] = eigY_L;
ei[3] = eigY_R;

//return//
return ei;}

int main(){


// Parameters //
int     len_x = 50;
int     len_y = 50;
double  xmin = -2.5;
double  xmax = 2.5;
double  ymin = -2.5;
double  ymax = 2.5;
double  dx = (xmax - xmin) / (len_x);
double  dy = (ymax - ymin) / (len_y);
double  c = 1.;
double  dt = 0.;
double  t  = 0.;
double  nmb_t  = 300;

Vector<double> x(len_x+2);
Vector<double> y(len_y+2);

    //  U   //
Matrix_2D<std::array<double,3>> Ui(len_x+2,len_y+2);
Matrix_2D<std::array<double,3>> Un(len_x+2,len_y+2);
enum var{rho,mx,my};
    //  Fx   //
Matrix_2D<std::array<double,3>> Fx(len_x+2,len_y+2);
    //  Fy   //
Matrix_2D<std::array<double,3>> Fy(len_x+2,len_y+2);


for(int ii=1;ii<=len_x;ii++){
  x[ii] = xmin + ((ii-1)*dx + 0.5*dx);
 }
for(int jj=1;jj<=len_y;jj++){
  y[jj] = ymin + ((jj-1)*dy + 0.5*dy);
 }


std::ofstream file("2D_GAS_Fri_plus1.txt");


file << "#title = 2D advection equation (Fri)" << std::endl;
file << "#xmin = " << xmin << std::endl;
file << "#xmax = " << xmax << std::endl;
file << "#ymin = " << ymin << std::endl;
file << "#ymax = " << ymax << std::endl;
file << "#dim = " << 2 << std::endl;
file << "#len_x = " << len_x << std::endl;
file << "#len_y = " << len_y << std::endl;
file << "#nmb_t = " << nmb_t << std::endl;
file << "#dx = " << dx << std::endl;
file << "#dy = " << dy << std::endl;
file << "#dt =  variable"  << std::endl;

 //  Initial Conditions  //

 //  density + momentum //
 double  factx = 1.;
 double  facty = 1.;
 double  x0 = 0.;
 double  y0 = 0.;


 //  Initials Conditions  //

 // - Surdensitée au milieur

for(int ix = 1; ix <=len_x; ++ix) {
double argx = factx * (x[ix] - x0) * (x[ix] - x0);
   for(int iy = 1; iy <=len_y; ++iy) {
     double argy = facty * (y[iy] - y0) * (y[iy] - y0);
      Ui(ix,iy)[rho] =  exp(-(argx + argy));
      Ui(ix,iy)[mx] = 0;
      Ui(ix,iy)[my] = 0;

    }
  }

// - Kelvin-Hermunttz
//for(int ix = 1; ix <=int(len_x/2); ++ix) {
//   for(int iy = 1; iy <=len_y; ++iy) {
//      Ui(ix,iy)[rho] = 1.;
//      Ui(ix,iy)[mx] = 0.;
//      Ui(ix,iy)[my] = 10;
//    }  }
//for(int ix = int(len_x/2); ix<=len_x; ++ix) {
//   for(int iy = 1; iy <=len_y; ++iy) {
//      Ui(ix,iy)[rho] = 1.;
//      Ui(ix,iy)[mx] = 0.;
//      Ui(ix,iy)[my] = 1;
//
//    }  }
//
//    // perturbation
//    Ui(len_x/2 - 1,len_y/2 + 1)[mx] = 0.1;
//    Ui(len_x/2 + 1,len_y/2 - 1)[mx] = -0.1;
//

 //  Conditions Aux Bords //

  for(int iy=1;iy<=len_y;iy++){
   Ui(0,iy) = Ui(len_x,iy);
   Ui(len_x+1,iy) = Ui(1,iy);
   }

  for(int ix=1;ix<=len_x;ix++){
   Ui(ix,0) = Ui(ix,len_y);
   Ui(ix,len_y+1) = Ui(ix,1);
   }
//  Conditions Aux Coins //
Ui(0,0) = Ui(len_x,len_y);
Ui(0,len_y+1) = Ui(len_x,1);
Ui(len_x+1,0) = Ui(1,len_y);
Ui(len_x+1,len_y+1) = Ui(1,1);


write(file,t,len_x,len_y,x,y,Ui);

double F0X,F0Y,F1X,F1Y,F2X,F2Y;
double ei_x_L,ei_x_R,ei_Y_L,ei_Y_R;

for(int jj=1;jj<nmb_t;jj++){
  dt = Ga_Fr_dt(dt,Ui,dx,dy,c,len_x,len_y);// FONCT
  t += dt;
  for(int ix=1;ix<=len_x;ix++){
  for(int iy=1;iy<=len_y;iy++){
    //ei_x_L,ei_x_R,ei_Y_L,ei_Y_R = Ga_Fr_ei(Ui,ix,iy,c)[0],Ga_Fr_ei(Ui,ix,iy,c)[1],Ga_Fr_ei(Ui,ix,iy,c)[2],Ga_Fr_ei(Ui,ix,iy,c)[3];
    ei_x_L = dx/dt;
    ei_x_R = dx/dt;
    ei_Y_L = dy/dt;
    ei_Y_R = dy/dt;

    F0X =  (0.5*(Ui(ix-1,iy)[mx] + Ui(ix,iy)[mx]) - 0.25*(ei_x_L)*(Ui(ix,iy)[rho] - Ui(ix-1,iy)[rho])) - (0.5*(Ui(ix,iy)[mx] + Ui(ix+1,iy)[mx]) - 0.25*(ei_x_R)*(Ui(ix+1,iy)[rho] - Ui(ix,iy)[rho]));
    F0Y =  (0.5*(Ui(ix,iy-1)[my] + Ui(ix,iy)[my]) - 0.25*(ei_Y_L)*(Ui(ix,iy)[rho] - Ui(ix,iy-1)[rho])) - (0.5*(Ui(ix,iy)[my] + Ui(ix,iy+1)[my]) - 0.25*(ei_Y_R)*(Ui(ix,iy+1)[rho] - Ui(ix,iy)[rho]));

    F1X =  (0.5*(Ui(ix-1,iy)[mx]*Ui(ix-1,iy)[mx]*Ui(ix-1,iy)[rho] + c*c*Ui(ix-1,iy)[rho]  +  Ui(ix,iy)[mx]*Ui(ix,iy)[mx]*Ui(ix,iy)[rho] + c*c*Ui(ix,iy)[rho]) - 0.25*(ei_x_L)*(Ui(ix,iy)[mx] - Ui(ix-1,iy)[mx]))  -
           (0.5*(Ui(ix,iy)[mx]*Ui(ix,iy)[mx]*Ui(ix,iy)[rho] + c*c*Ui(ix,iy)[rho]  +  Ui(ix+1,iy)[mx]*Ui(ix+1,iy)[mx]*Ui(ix+1,iy)[rho] + c*c*Ui(ix+1,iy)[rho]) - 0.25*(ei_x_R)*(Ui(ix+1,iy)[mx] - Ui(ix,iy)[mx]));
    F1Y =  (0.5*(Ui(ix,iy-1)[mx]*Ui(ix,iy-1)[my]/Ui(ix,iy-1)[rho] +  Ui(ix,iy)[mx]*Ui(ix,iy)[my]/Ui(ix,iy)[rho]) - 0.25*(ei_Y_L)*(Ui(ix,iy)[mx] - Ui(ix,iy-1)[mx]))  -
           (0.5*(Ui(ix,iy)[mx]*Ui(ix,iy)[my]/Ui(ix,iy)[rho] +  Ui(ix,iy+1)[mx]*Ui(ix,iy+1)[my]/Ui(ix,iy+1)[rho]) - 0.25*(ei_Y_R)*(Ui(ix,iy+1)[mx] - Ui(ix,iy)[mx])) ;

    F2X =  (0.5*(Ui(ix-1,iy)[mx]*Ui(ix-1,iy)[my]/Ui(ix-1,iy)[rho] +  Ui(ix,iy)[mx]*Ui(ix,iy)[my]/Ui(ix,iy)[rho]) - 0.25*(ei_x_L)*(Ui(ix,iy)[my] - Ui(ix-1,iy)[my]))  -
           (0.5*(Ui(ix,iy)[mx]*Ui(ix,iy)[my]/Ui(ix,iy)[rho] +  Ui(ix+1,iy)[mx]*Ui(ix+1,iy)[my]/Ui(ix+1,iy)[rho]) - 0.25*(ei_x_R)*(Ui(ix+1,iy)[my] - Ui(ix,iy)[my])) ;
    F2Y =  (0.5*(Ui(ix,iy-1)[my]*Ui(ix,iy-1)[my]*Ui(ix,iy-1)[rho] + c*c*Ui(ix,iy-1)[rho]  +  Ui(ix,iy)[my]*Ui(ix,iy)[my]*Ui(ix,iy)[rho] + c*c*Ui(ix,iy)[rho]) - 0.25*(ei_Y_L)*(Ui(ix,iy)[my] - Ui(ix,iy-1)[my]))  -
           (0.5*(Ui(ix,iy)[my]*Ui(ix,iy)[my]*Ui(ix,iy)[rho] + c*c*Ui(ix,iy)[rho]  +  Ui(ix,iy+1)[my]*Ui(ix,iy+1)[my]*Ui(ix,iy+1)[rho] + c*c*Ui(ix,iy+1)[rho]) - 0.25*(ei_Y_R)*(Ui(ix,iy+1)[my] - Ui(ix,iy)[my]));


    Un(ix,iy)[rho]  = Ui(ix,iy)[rho]  + (dt/dx)*(F0X) + (dt/dy)*(F0Y);  // FONCT
    Un(ix,iy)[mx] = Ui(ix,iy)[mx] + (dt/dx)*(F1X) + (dt/dy)*(F1Y);  // FONCT
    Un(ix,iy)[my] = Ui(ix,iy)[my] + (dt/dx)*(F2X) + (dt/dy)*(F2Y);  // FONCT
}}
  for(int iy=1;iy<=len_y;iy++){
   Un(0,iy) = Un(len_x,iy);
   Un(len_x+1,iy) = Un(1,iy);
}
  for(int ix=1;ix<=len_x;ix++){
   Un(ix,0) = Un(ix,len_y);
   Un(ix,len_y+1) = Un(ix,1);
}
Un(0,0) = Un(len_x,len_y);
Un(0,len_y+1) = Un(len_x,1);
Un(len_x+1,0) = Un(1,len_y);
Un(len_x+1,len_y+1) = Un(1,1);

Ui = Un;


write(file,t,len_x,len_y,x,y,Ui);
}


return 0;
}


